Instances are always exported, and reexported in Haskell.
The default assumption (as far as I know) is that this is necessary to ensure instance coherence.
But this is not in fact the case.

## Motivation

Always "leaking" instances is anti-modular.

For example, in https://github.com/haskellfoundation/tech-proposals/pull/47#issuecomment-1557702559 Bodigrim says

> There is a PVP-related issue with re-exports. Namely: you can control re-exported functions and classes, but not instances, which are implicitly re-exported in full.
> If package A defines a type class X, re-exported from package B, then package B must use tight bounds `A >= X.Y.Z && < X.Y.(Z+1)`, because even minor releases of A can introduce new instances, leaking through the API of the very same version of B.
> A new release of B is required to allow `A >= X.Y.(Z+1) && < X.Y.(Z+2)`, and so on.
> Both packages must evolve and be released in lockstep.

Even without reexporting, similar issues can arise from changing what modules are imported.

All of this is very fragile and the types of mitigations, e.g. exact version pins, are so onerous and costly that they often impose worse problems than the ones they solve.
It would be nice to have a better solution.

## The naive solution that doens't work

We cannot *just* not export instances, as is widely known.
Here is an example of why not::

```haskell
module A (Foo, bar) where

  data Foo

  -- Not exported
  instance Ord Foo where ...

  bar :: Map Foo Int
  bar = ...

module B where

  import A

  -- Orphan
  instance Ord Foo where ...

  baz = Map.insert _ _ bar
```

`baz` will be all messed up because the two orderings don't necessarily agree, so we will get keys out of order.

## Separating privileges and responsibilities

The problem with the above example is that since module `B` did not know about the private instance in module `A`, it did not know that its orphan instance was conflicting with it.
This is a fundamental problem, and I don't think there is a solution for it.
But that doesn't mean the overall problem is hopeless!

The trick is to step back a bit, and ask: what do `import`s do?
Of course, they allow downstream modules to use items defined in upstream modules.
But they also impose burdens in they *disallow* writing instances which would conflict with upstream modules' instances.
This is the "privileges and responsibilities" view of imports: Using imported things is the privilege, and not writing conflicting instances is the responsibility.

## A solution

As the various English cliches go, no privileges without responsibilities.
But nothing prohibits responsibilities without responsibilities!
We cannot neglect to inform downstream code of our instances so they do not write ones that conflict.
But we *can* impose the burden of coherence on them without letting them get to use the instances they must be coherent with!

This is a notion of private instances that works:
Instances can be unexported in the sense that downstream modules cannot use them, but still exported in the sense that downstream modules must not conflict with them.

## Explicit imports and exports

We could have the following syntax:

```bnf
<export-import> ::= ...
                 |  instance <type>
                 |  burden <type>
```

- `instance <type>` would indicate an instance is (re-)exported in the usual fully public way.
 (`<type>` would be the constraint-kind type of the instance.)
 On the import side, they would indicate explicitly opting unto using (privilege side) just the written-down instances, and not any other from that module.

- `burden <type>` would indicate the responsibility is exported without the privilege.
  This would be for private instances.

`burden` imports are permitted too, but at first glance they wouldn't do much there, as opting out of burdens is prohibited.
On the other hand, it is perfectly reasonable to assume burden the upstream isn't actually imposing.
(Compare `hiding foo` being fine even if the module doesn't export a `foo`.)
This can be useful for operating under more restricted conditions than upstream modules currently impose.

On the export side, fictitious burdens also make sense for the module to "reserve the right" to, in the future, write instances it hasn't yet.

> It is interesting to note that `-Werror=orphans` is very similar to writing a `burden` entry for every non-orphan instance a module is "allowed" to contain.
> In particular, if all modules do this, then only non-orphan instances can be written in any module.
>
> [Rehabilitating Orphans with Order theory](/Rehabilitating-Orphans-with-Order-theory) builds off the relationship between the ideas in this page and orphans.

## Theory

The PVP defines a compatability partial order on modules.
Subtyping relations are also partial orders.

It is a well-known trick in the subtyping literature that "invariant" properties can often be broken down into a covariant and contravariant component -- it is just the bundling of the two together that crates the invariant.
For example
```haskell
type Foo a = a -> a
```
is invariant (in some hypothetical Haskell with subtyping) because one use of `a` induces covariance, and the other introduces contravariance.

The idea on this page is but the same idea applied to type class instances.
Privilages are covariant width subtyping, in the sense that modules can be safely upcast by *removing* privilages.
Burdens are contravariant width subtyping, in the sense that modules can be safely upcast by *adding* burdens.
