# FAQ for new plugin authors
## How to run a plugin I am developing
A plugin must be in the package database, and then running `ghc -package PluginPackage -fplugin=My.Plugin` ...` will run `ghc` with the plugin active. Practically, if you have a cabal package in the current directory, then the following should work
```
#build your plugin
cabal build
#run ghc with the plugin active
cabal exec -- ghc -fplugin=...
```

## How to run a plugin I am developing with a custom GHC
If there are issues with your plugin triggering core lint errors etc, you may want to run with a custom ghc (perhaps with `-DDEBUG`) with the plugin. This is similar to the above section, but telling giving `cabal` to run a different compiler
```
#build your plugin
cabal build
#run ghc with the plugin active
cabal exec -- ghc -fplugin=...
```

## How to get more information about core lint errors
If running with `-dlint` gives a core lint error, it may be useful to get some more detailed output in the offending program (especially displaying uniques). The [`-dppr-debug`](https://downloads.haskell.org/ghc/latest/docs/users_guide/debugging.html#ghc-flag--dppr-debug) will give (much) more verbose output.

## How to remove irrelevant information in core lint errors
You can suppress printing parts of of programs with the various [`-dsuppress-...` flags](https://downloads.haskell.org/ghc/latest/docs/users_guide/debugging.html#suppressing-unwanted-information)