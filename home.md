This is GHC's Wiki.

You may wish to see the [table of contents](./index) to get a sense for what is available in this Wiki.

Everyone can edit this wiki.  Please do so -- it easily gets out of date.  But it has lots of useful information in it.

# Quick Links

* [GHC Home](https://www.haskell.org/ghc/) GHC's home page
* [GHC User's Guide](/ghc-users-guide#ghc-users-documentation) Official user's guide for current and past GHC releases.
* Joining In
  * [Newcomers info](/contributing#newcomers-to-ghc) / [Contributing](/contributing) Get started as a contributor.
  * [Mailing Lists & IRC](/mailing-lists-and-irc) Stay up to date and chat to the community.
  * [The GHC Team](/team-ghc) Contributors and who is responsible for what.
* Documentation
  * [GHC Status Info](/GHC-status) How is the next release coming along?
  * [Migration Guide](/migration) Migrating code between GHC releases
  * [Working conventions](/contributing#contributing-to-ghc) How should I do X?
  * [Building Guide](/building/#building-and-porting-ghc) Build GHC from source.
  * [Coding style guidelines](/commentary/coding-style) Please follow these guidelines.
  * [Trees that grow (TTG)](/implementing-trees-that-grow) Guidance on how GHC uses the TTG style.
  * [Debugging](/debugging#debugging) How to debug GHC.
  * [Commentary](/commentary/#the-ghc-commentary) Explaining the GHC source code.
