[[_TOC_]]

# GHC 9.8.x Migration Guide

This guide summarises the changes you may need to make to your code to migrate from GHC 9.6 to GHC 9.8. This guide complements the GHC 9.8.x release notes which should be consulted as well.

---

## Compiler changes

### GHC is pickier about orphan instances for type families and data families

GHC 9.8 is now more picky about what constitutes an instance for a type family or a data family.
Consider for example:

```haskell
type instance T A1 A2 = B
```

If none of `T`, `A1` and `A2` are defined in the same module as this instance, GHC will now report it as being an orphan. This differs from the previous (buggy) behaviour which instead checked that either `T` or **`B`** is defined in the current module.

If GHC emits undesirable orphan instances warnings that it didn't use to, the following options are available:

  - move the instance, e.g. to the same module as `T` or `A1` or `A2` (this might require restructuring of your module hierarchy), or
  - if the instance is deliberately an orphan, you can silence the warning with by adding `{-# OPTIONS_GHC -Wno-orphans #-}` to the top of the module.

As usual with orphans, you can also introduce a newtype to hang the instance off of, if that's workable in your use-case.

### Re-exporting clashing record fields now requires -XDuplicateRecordFields

It is no longer sufficient to enable `-XDuplicateRecordFields` in the defining modules; if a module introduces field name clashes in its export list it should itself enable `-XDuplicateRecordFields`. For example:

```haskell
-- M1.hs
{-# LANGUAGE DuplicateRecordFields #-}
module M1 where { data D1 = MkD1 { fld :: Int } }

-- M2.hs
{-# LANGUAGE DuplicateRecordFields #-}
module M2 where { data D2 = MkD2 { fld :: Int } }

-- M.hs
module M ( module M1, module M2 ) where { import M1; import M2 }
```

This used to be accepted, but will now require adding the `DuplicateRecordFields` extension to `M` in order to be accepted.

Note that this does not apply if the re-exporting module does not itself introduce new clashes. For example, the following remains ok:

```haskell
-- N1.hs
{-# LANGUAGE DuplicateRecordFields #-}
module N1 where
  data D1 = MkD1 { fld :: Int }
  data D2 = MkD2 { fld :: Int }

-- N2.hs
module N2 ( module N1 ) where { import N1 }
```

### Small change in behaviour of partial type signatures

Consider these partial type signatures:
```haskell
f3 :: (Eq a) -> a -> a -> _
f3 x y = ...

f2 :: (Eq a, _) => a -> a -> _
f2 x y = ....

f1a :: a -> a -> _
f1a x y = ...

f1b :: () => a -> a -> _
f1b x y = ...
```
* `f3` says that only `(Eq a)` constraints should arise in the RHS.  So the rhs could be `x==y`, say.
   But `f3 x y = x>y` should fail, becuase it needs `(Ord a)` and we only have `(Eq a)`
* `f2` has an "extra constraints" wildcard, saying that `f2` has constraints `Eq a` and perhaps more
  constraints; so the definition could be `f3 x y z = show x`, and GHC would report that the
  extra-constraints wildcard was filled in with `(Show a)`.
* Finally, `f1a` and (even more explicitly) `f1b` have no constraints, a bit like
  `f3`.  So `f1a x y = x==y` should fail, just like `f3 x y = x>y` should fail.

This latter point is a change. Previously, GHC would trigger the Monomorphism Restriction
for non-overloaded partial type signatures (regardless of the form of the definition).
So `f1a` and `f1b` would unexpectedly be monomorphic; and provided they were only used
monomorphically, GHC might have been able to discharge an `(Eq a)` constraint arising from the RHS of `f1a` or `f1b`.

Making the behaviour more consistent may (in obscure circumstances) make some programs fail,
such as this example from test T11192
```
fails = let go :: _
            go 0 a = a
        in go (0 :: Int) undefined
```
The real type of `go` is `go :: forall a b. Num a => a -> b -> b`.
But the signature explicitly says "no constraints" so we now get an
unsolved `Num a` constraint.

The fix is simple: add a full type signature, or add an extra-constraint wildcard, like this
```
           go :: _ => _
```

## Template Haskell changes

### New namespace for record fields

Record fields now belong to separate `NameSpace`s, keyed by the parent of the record field, instead of all belonging to the variable namespace.

The namespace for a record field is given by the name of the first constructor of the parent type, even when this constructor does not have the field in question.

For example:

```haskell
data Foo = MkFoo1 { fld1 :: Int } | MkFoo2 { fld1 :: Int, fld2 :: Bool }
```

the `NameSpace` for **both** `fld1` and `fld2` is `FldName "MkFoo1"` (even though `MkFoo1` does not have the field `fld2`).

In practice, this change means you should use `mkNameG_fld` instead of `mkNameG_v` to refer to a record field; e.g. you would refer to `fld1` by using `mkNameG_fld pkg mod "MkFoo1" "fld1"` instead of `mkNameG_v pkg mod "fld1"`.

This change was made in order to allow Template Haskell to properly support the `DuplicateRecordFields` extension.

### Invisible binders in type-level declarations

GHC now supports invisible binders in type-level declarations, as described in [GHC proposal #425](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0425-decl-invis-binders.rst). These changes also affect Template Haskell. There is now a `BndrVis` data type to represent the difference between visible and invisible type-level binders:

```hs
data BndrVis = BndrReq   -- a
             | BndrInvis -- @a
```

The following Template Haskell ASTs now have `TyVarBndr`s that are parameterized by `BndrVis` instead of `()`:

```diff
 data Dec
   = ...
-  | DataD Cxt Name [TyVarBndr ()] (Maybe Kind) [Con] [DerivClause]
-  | NewtypeD Cxt Name [TyVarBndr ()] (Maybe Kind) Con [DerivClause]
-  | TypeDataD Name [TyVarBndr ()] (Maybe Kind) [Con]
-  | TySynD Name [TyVarBndr ()] Type
-  | ClassD Cxt Name [TyVarBndr ()] [FunDep] [Dec]
-  | DataFamilyD Name [TyVarBndr ()] (Maybe Kind)
+  | DataD Cxt Name [TyVarBndr BndrVis] (Maybe Kind) [Con] [DerivClause]
+  | NewtypeD Cxt Name [TyVarBndr BndrVis] (Maybe Kind) Con [DerivClause]
+  | TypeDataD Name [TyVarBndr BndrVis] (Maybe Kind) [Con]
+  | TySynD Name [TyVarBndr BndrVis] Type
+  | ClassD Cxt Name [TyVarBndr BndrVis] [FunDep] [Dec]
+  | DataFamilyD Name [TyVarBndr BndrVis] (Maybe Kind)
   | ...

 data TypeFamilyHead =
-  TypeFamilyHead Name [TyVarBndr ()]      FamilyResultSig (Maybe InjectivityAnn)
+  TypeFamilyHead Name [TyVarBndr BndrVis] FamilyResultSig (Maybe InjectivityAnn)
```

It is possible that Template Haskell code which compiled prior to GHC 9.8 will no longer compile after these changes. Here are some possible ways to make it compile with 9.8 in a backwards-compatible way, inspired by https://gitlab.haskell.org/ghc/head.hackage/-/merge_requests/302:

* Construct `TyVarBndr`s using `plainTV` or `kindedTV` from `Language.Haskell.TH.Lib`, which is polymorphic over both `TyVarBndr ()` and `TyVarBndr BndrVis`. These functions will default to using `BndrReq` on GHC 9.8.
* The [`th-abstraction`](http://hackage.haskell.org/package/th-abstraction) library provides a compatibility layer in the [`Language.Haskell.TH.Datatype.TyVarBndr`](http://hackage.haskell.org/package/th-abstraction-0.4.0.0/docs/Language-Haskell-TH-Datatype-TyVarBndr.html) module.

## Library changes
### base

#### `(?!)` now defined in `base`

`Data.List` now exports

```haskell
(?!) :: [a] -> Int -> Maybe a
```

This might cause name clashes which will have to be resolved.

#### `unzip` is now defined in `Data.Functor`

`Data.Functor` now exports
```haskell
unzip :: Functor f => f (a, b) -> (f a, f b)
````
which may cause name clashes with the less general version in `Prelude` and `Data.List`:
```haskell
unzip :: [(a, b)] -> ([a], [b])
````

#### Partiality warnings for `head`/`tail`

[CLC proposal #87](https://github.com/haskell/core-libraries-committee/issues/87) has been implemented, adding partiality warnings to `head` and `tail` from `Data.List`. These new warnings belong to the custom warning category `-x-partial` (see [GHC proposal #541](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0541-warning-pragmas-with-categories.rst)), and can be silenced by using `-Wno-x-partial`, or (ideally) by refactoring the code to avoid uses of `head`/`tail`.

The CLC also provides a [specific migration guide](https://github.com/haskell/core-libraries-committee/blob/main/guides/warning-for-head-and-tail.md) for this issue.

### text
#### ```Data.Text.Array.Array``` is now a type synonym

The version of the ``text`` library included changes ``Data.Text.Array.Array`` to be
a type synonym of ``Data.Array.Byte.ByteArray``. While its former  data
constructor, ``ByteArray``, has been replaced with a pattern synonym, it cannot
be imported as bundled with the type constructor.

Consequently, imports like:

    import Data.Text.Array (Array(..))

will need to avoid using a bundled import (e.g. by qualification):

    import Data.Text.Array as A