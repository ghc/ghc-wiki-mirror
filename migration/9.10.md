[[_TOC_]]

# GHC 9.10.x Migration Guide

This guide summarises the changes you may need to make to your code to migrate from GHC 9.8 to GHC 9.10. This guide complements the GHC 9.10.x release notes which should be consulted as well.

---


## Compiler changes

### GHC now checks for `DataKinds` (once again)

Due to an oversight, previous GHC releases (starting from 9.4) allowed the use of promoted data types in kinds, even when `DataKinds` was not enabled. That is, GHC would erroneously accept the following code:

```hs
{-# LANGUAGE NoDataKinds #-}

import Data.Kind (Type)
import GHC.TypeNats (Nat)

-- Nat shouldn't be allowed here without DataKinds
data Vec :: Nat -> Type -> Type
```

This oversight has now been fixed. If you wrote code that took advantage of this oversight, you may need to enable `DataKinds` in your code to allow it to compile. To ease migration, these errors will be downgraded to warnings in GHC 9.10, but they will be promoted to errors in the following GHC release. If you see a warning like this:

```
Vec.hs:7:1: warning: [GHC-68567] [-Wdata-kinds-tc]
    • An occurrence of ‘GHC.Num.Natural.Natural’ in a kind requires DataKinds.
      Future versions of GHC will turn this warning into an error.
    • In the expansion of type synonym ‘Nat’
      In the data type declaration for ‘Vec’
    Suggested fix: Perhaps you intended to use DataKinds
  |
7 | data Vec :: Nat -> Type -> Type
  | ^^^^^^^^
```

Then you can fix the warning by enabling `DataKinds`.


## Template Haskell

### `Code` kind change
The kind of `Code` was changed from `forall r. (Type -> Type) -> TYPE r -> Type`
    to `(Type -> Type) -> forall r. TYPE r -> Type`. This enables higher-kinded usage. This might break your code if you use type applications, in order to fix it..

###### Examples
- [th-compat](https://github.com/haskell-compat/th-compat/commit/db6d5205b7d6d560bf6dcc4e72b74866ddabb671)

### New `ArgPat` type

A new `ArgPat` type has been added to `template-haskell` as part of the implementation of [GHC Proposal 155](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0155-type-lambda.rst). `LamE` and `Clause` now take an `ArgPat` rather than a `Pat`. You can use the `clause`, and `lamE` functions from `Language.Haskell.TH.Lib` for backwards compatibility. 

###### Examples
 - [deriving-compat](https://github.com/haskell-compat/deriving-compat/commit/ecdff91314dd3e8ce46572ab896bf3c8b85a41ca)
 - [bifunctors](https://github.com/ekmett/bifunctors/commit/70c504844e28b170161b75718cba819f0e798506)