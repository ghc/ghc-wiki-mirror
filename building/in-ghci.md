## Loading GHC into GHCi

With one command you can load GHC into GHCi (assuming a configured source tree: `./boot && ./configure`):

```
./hadrian/ghci
```

If you run into unexpected error messages, you may need to remove and rebuild the `.hadrian_ghci` directory.

:warning: If your `~/.ghci` configuration file enables extensions like `TemplateHaskell` or `OverloadedStrings`, they will interfere with the loading, and **will** produce compilation errors. In order to skip `~/.ghci`, start GHCi with

```
./hadrian/ghci -ignore-dot-ghci
```

This works best on systems with a decent amount of RAM.  The GHCi process uses about 3.5GB initially, and the usage sometimes increases during development.  So, if you have less than 8GB of RAM, loading GHC into GHCi probably will not aid development efficiency.

## ghcid

There is a `.ghcid` file which allows use of Neil Mitchell's handy [ghcid](https://github.com/ndmitchell/ghcid) tool directly by just running `ghcid`. This tool drives GHCi to automatically reload when files change.  