# Setting up your system for building GHC

This section describes how to set up your system with all the tools you need to build and develop GHC. Jump to the relevant section for your [platform](platforms):

- [Preparation Linux](building/preparation/linux)
- [Preparation macOS](building/preparation/mac-osx)
- [Preparation Windows](building/preparation/windows) ([Windows support for GHC](platforms/windows))
- [Preparation FreeBSD](building/preparation/free-bsd) ([FreeBSD support for GHC](free-bsd-ghc))
- [Preparation Solaris](building/preparation/solaris) and [Building under Solaris](building/solaris)
- [Illumos](building/preparation/illumos) (SmartOS, OpenIndiana, etc.)
- [Raspberry Pi](building/preparation/raspberry-pi) (cross-compiling from Linux)
- [Preparing and Building OpenBSD](building/preparation/openbsd)

Reference: [list of tools needed to build GHC](building/preparation/tools).

If you wish to use Stack to build GHC, follow [these instructions](building/preparation/stack).

See the [Newcomers](/contributing#newcomers-to-ghc) page for how to proceed after this.