# Setting up a Linux system for building GHC


If you're on a recent Linux system, then you should be able to get a working build environment by installing the following packages using your system's package manager.

## Docker


If you are familiar with docker and don't mind running the build and runghc inside a docker image then this is a great option. A docker image is provided with all the ghc build requirements and some developer related tools.

1. The GHC source code is not part of the docker image. First download the relevant version of the GHC source code, described in [Building/GettingTheSources](building/getting-the-sources).
2. Set the version of the docker image with `export DOCKER_ENV=7f63b34ac87b85470eef9c668e9528e8e2f5b46a`. This is an example image version, you can find the latest [here](https://gitlab.haskell.org/ghc/ghc/-/blob/master/.gitlab-ci.yml#L5).
3. cd into the directory with the source code
4. Now run `docker run --rm -i -t -v $(pwd):/home/ghc registry.gitlab.haskell.org/ghc/ci-images/x86_64-linux-deb10:$DOCKER_ENV /bin/bash`

You can now edit the GHC source code with your own editor. Builds can be done inside the docker container following [Building/Hadrian](building/hadrian).

## Fedora


Install the [required tools](https://gitlab.haskell.org/ghc/ghc/-/wikis/building/preparation/tools) using the following command for Fedora 22 and later (for earlier versions of Fedora, use `yum` instead of `dnf`):

```
   $ sudo dnf install glibc-devel ncurses-devel gmp-devel autoconf automake libtool gcc gcc-c++ make perl python ghc happy alex git
```


For building the documentation: (User's Guide and Cabal guide):
(optional)

for GHC > 7.10:
```
   $ sudo dnf install python3-sphinx
```
for GHC <= 7.10:
```
   $ sudo dnf install docbook-utils docbook-utils-pdf docbook-style-xsl
```


other  packages that are useful for development:
(optional)

```
   $ sudo dnf install strace patch
```


Now the system should be ready to build GHC.


For a quickstart, follow the commands listed under:

[https://gitlab.haskell.org/ghc/ghc/-/tree/master#building-installing](https://gitlab.haskell.org/ghc/ghc/-/tree/master#building-installing)

## Debian, Ubuntu, and other Debian-based systems


You can make sure you have all dependencies by

```
   $ sudo apt-get build-dep ghc
```


But this might install some packages you do not use in your system (e.g. Sphinx).  Alternatively install the following:

```
   $ sudo apt-get install build-essential git autoconf python3 libgmp-dev libnuma-dev libncurses-dev
   $ cabal v2-install alex happy
```


(`ncurses-dev` is needed by the `terminfo` package, and `g++` is needed by a couple of tests, `ghcilink003` and `ghcilink006`).


Optional: install LLVM from \<[http://apt.llvm.org](http://apt.llvm.org)\> (only necessary to make the `-fllvm` flag work). [Commentary/Compiler/Backends/LLVM/Installing](commentary/compiler/backends/llvm/installing#llvm-support) will tell you which version to install.


For building the documentation (User's Guide):

for GHC > 7.10:
```
   $ sudo apt-get install python3-sphinx texlive-xetex texlive-fonts-recommended fonts-lmodern texlive-latex-recommended texlive-latex-extra
```
for GHC <= 7.10:
```
   $ sudo apt-get install dblatex docbook-xsl docbook-utils libxml2-utils texlive-font-utils
```


other packages that are useful for development:

```
   $ sudo apt-get install linux-tools-generic xutils-dev
```


The package `linux-tools` includes `perf`, see [Debugging/LowLevelProfiling/Perf](debugging/low-level-profiling/perf). The package `xutils-dev` provides the `lndir` program, need for running `make sdist` and useful for maintaining a separate build tree, see [Building/Using](building/using).


For [validating patches](testing-patches) :

## Arch


Install the [required tools](https://gitlab.haskell.org/ghc/ghc/-/wikis/building/preparation/tools):

```
   $ sudo pacman -S ghc ghc-static perl gcc make happy alex cabal-install autoconf automake python python-sphinx libedit numactl
```


Note that due to questionable decisions made by Arch's Haskell packaging (namely the decision to package only dynamic objects), it's quite important that you do not attempt to use the `haskell-*` packages to bootstrap your GHC tree. 

## Nix/NixOS


The recommended way to create an environment in which to build GHC is to use Alp's `ghc.nix` repository. 

1. Clone `https://github.com/alpmestan/ghc.nix` into your `ghc` folder.
1. `nix-shell ghc.nix/`

That repo has a flake file, so you can use it in a new-style nix shell, too:

```shell
# You can also point to a local clone of the git repo instead
$ nix develop github:alpmestan/ghc.nix
```

An even more comfortable way is to combine new-style nix shell with the builtin (since version 2.29) support in `direnv`. Simply do

```shell
$ echo "use flake github:alpmestan/ghc.nix" > .envrc
$ direnv allow
```

And after a while, you should be all set. (If you want to speed this process up by downloading cached build artifacts, see https://github.com/alpmestan/ghc.nix#cachix.)

You can then perform a build by running

```shell
# configure_ghc is supplied by ghc.nix, it is an alias for
# ./configure $CONFIGURE_ARGS, where $CONFIGURE_ARGS contains
# several useful arguments, in particular for making sure that
# the resulting GHC can be used outside of the nix shell, still
# knowing exactly where the relevant dependencies reside.
# In ZSH, this doesn't work and you'll need to do 
# ./configure ${=CONFIGURE_ARGS} instead.
./boot && configure_ghc

# GHC has moved away from Make, and exclusively uses Hadrian (based on Shake) as a build system
hadrian/build -j
```
