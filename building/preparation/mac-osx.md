# Preparation (using Homebrew)

This is how to get started using [Homebrew](https://www.brew.sh/):

1. Install Apple's command line tools; there are two to accomplish this:
   - Install the latest command line tools from [http://developer.apple.com/downloads](http://developer.apple.com/downloads) **or**
   - Install XCode, launch XCode, open Preferences, select "Downloads" and install the command line tools
2. Run `brew install autoconf automake python sphinx-doc`.
3. Add `sphinx-build` tool to the PATH: `export PATH="/opt/homebrew/opt/sphinx-doc/bin:$PATH"`.
4. Install a recent GHC version using your method of choice (e.g. [ghcup](https://www.haskell.org/ghcup/))
5. Install GHC's build dependencies: `cabal update; cabal install alex happy`
6. If you want to build the PDF users guide you will need a `xelatex` installation (e.g. from [MacTeX](https://www.tug.org/mactex/)) and the [Dejavu](https://www.fontsquirrel.com/fonts/list/foundry/dejavu-fonts) font family.

At this point you have everything necessary to build GHC. To do so, run the following:
```
$ git clone https://gitlab.haskell.org/ghc/ghc
$ cd ghc
$ ./boot
$ sdk_path="$(xcrun --sdk macosx --show-sdk-path)"
$ ./configure --with-system-libffi --with-intree-gmp --with-ffi-libraries=$sdk_path/usr/lib --with-ffi-includes=$sdk_path/usr/include/ffi
$ hadrian/build
```

# Preparation (using Nix)

This is how to get started using the Nix derivation used by GHC CI:
```
$ NIX_SYSTEM=aarch64-darwin bash .gitlab/ci.sh shell
$ ./boot
$ ./configure --with-system-libffi --with-intree-gmp $CONFIGURE_ARGS
$ hadrian/build -j4 --flavour=Quick
```
