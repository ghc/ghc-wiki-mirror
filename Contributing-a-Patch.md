<!--
To generate images with borders:
```
mkdir -p shadow
rm shadow/*
for i in *.png; do 
        nix run nixpkgs.imagemagick -c \
                convert $i -bordercolor white -border 50 \
                \( +clone -background black -shadow 80x5+2+2 \) \
                +swap -background white -layers merge +repage \
                shadow/$i
done
```
-->

Submitting a patch for incorporation into the tree is done by creating a *merge request*. The merge request serves as a place to conduct code review, collect continuous integration results, and eventually merge your patch.

## 1. Merge Request Workflow

1. If you are fixing a bug then have a look at [fixing bugs](working-conventions/fixing-bugs) else if you are adding a feature see [adding features](/working-conventions/adding-features).
1. **Open an MR:** see [opening a merge request](#2-opening-a-merge-request) below for the basics of opening an MR. Apply the ~"Blocked on Review" label when you believe the work is ready for code review.
1. **Technical review**: reviewers will evaluate the concept and implementation of the patch and work with the contributor (that's you) to iterate as necessary. At this stage, additional changes are best made as separate commits, for ease of reviewing. Reviewers can use the “Approve” button to indicate they are happy with the MR.
1. **Squash and rebase:** squash all your work-in-progress commits into a single commit with an informative commit message, and rebase on master.  (Occasionally your MR deals with more than one issue, in which case multiple commits may be appropriate, but the common case is a single commit.)  Check that you have followed the [review checklist](#5-review-checklist).
1. **Final review:** a member of [the GHC Team](https://gitlab.haskell.org/ghc/ghc-hq#2-the-ghc-team) will have a final look at the MR, and if it is ready, add the MR to the merge queue.
1. **Post-merge cleanup:** the contributor (that's you) should close and/or have a final look over the any related issues.

If you are stuck needing help or feedback at any stage then please apply the ~"Blocked on Review" label and leave a comment mentioning @triagers to draw attention to the MR.

## 2. Tickets, merge requests, and commits

### 2.1 Every MR should have a ticket

* A *ticket* or *issue* (e.g. #21623) describes a *problem*.
* A *merge request* (e.g. !8750) describes a *solution*.

Every MR should point to the tickets that it solves.  If you find yourself writing an MR without a ticket, consider opening a ticket that describes the problem you are solving.

### 2.2 Commit messages

Commit messages should aim to describe the changes a patch makes, e.g. "This patch modifies the Core simplifier to implement a new form of inlining pragma". Include ample information, in particular which commit the ticket fixed (if applicable) and which parts of the compiler are affected. This information is important to help GHC maintainers bisect regressions, compare features across branches, prepare release notes, etc.

The main content of a commit message should *not* be an explanation of the current implementation. This information is harder to find in a commit message, and (much worse) there is no explicit indication in the code that there is carefully-written information available about that particular line of code. Prefer instead writing an explanatory Note in the code, which you refer to in the commit message.

In short, commit messages describe *changes*, whereas comments explain the code *as it now is*.


## 3. Draft Status

You can mark a MR as "draft" or "work in progress" by beginning the title with the string `Draft:` or `WIP:`.  (The two are equivalent.)

### 3.1 What does it mean for an MR to be marked as Draft?

* It signals that your MR is not ready for review; you are working on it.   (Of course, you are free to ask someone for a review.) 
* The automated merge system (marge-bot) will not merge a Draft MR into master.
* Maintainers are unlikely to fix CI issues, review or rebase such patches.

###  3.2 Why has someone changed my MR marked as Draft?

There are a number of reasons why an MR might be marked as draft by someone else:

* The patch has been reviewed and some aspect of it needs to be addressed by the author.
* The patch is causing tests to fail on CI which need to be addressed. Either by fixing the patch or accepting new test output.
* The patch needs outside input to proceed (ghc-proposal approval, clc-proposal approval, upstream changes, community feedback)
* The patch is still being worked on.

###  3.3 When should I remove "Draft" from my MR?

* If you want review or help on design and implementation aspects of your MR.
* If you think there are no remaining issues with your MR and it's ready to be merged into master.
* If CI fails but you suspect the failures are unrelated to the MR.

If you are in doubt it's **always ok to remove "Draft"**. A MR not marked ready might never be seen by a maintainer. So it's better to mark your MR as ready than not.  

Worst case a maintainer will mark it as Draft again if they think it's up to you to make changes to the MR before moving forward.

## 4. Review checklist

See the [merge request description template](https://gitlab.haskell.org/ghc/ghc/-/blob/master/.gitlab/merge_request_templates/Default.md) for a checklist of requirements for a merge request.

When reviewing a merge request here are a few things to check for:

 * Have you followed GHC's [coding style guidelines](https://gitlab.haskell.org/ghc/ghc/-/wikis/commentary/coding-style), and especially have you added [suitable `Notes`s](https://gitlab.haskell.org/ghc/ghc/-/wikis/commentary/coding-style#2-using-notes)?
 * Are the commits logically structure? Are their commit messages descriptive?
 * Are ticket numbers referenced as appropriate?
 * Is a GHC release notes entry included (e.g. `docs/users_guide/*-notes.rst`)?
 * Have changelog entries been added to any changed packages (e.g. `libraries/*/changelog.md`)?
 * Has a test case been added?
 * Milestone and ~"backport needed" label set as appropriate
 * Does the patch change the language accepted by GHC or add a significant new user-facing feature? If so perhaps a [GHC proposal](https://github.com/ghc-proposals/ghc-proposals) is in order.
 * Does the patch change GHC's core libraries (e.g. `base`, `template-haskell`)? If so:
    * Has the [Core Libraries Committee](https://github.com/haskell/core-libraries-committee) consented to any changes to `base` (directly or due to changing a definition that `base` re-exports)?
    * Has the ~"user-facing" label been applied?  After applying that label, has the [head.hackage job](https://gitlab.haskell.org/ghc/head.hackage/) been run to characterise the effect of the change on user code?
    * Changelog and release notes entries are mandatory
    * Have package versions been bumped as appropriate?
    * Has an entry been added to the next release's [migration guide](../migration)?

To ensure that all interested reviewers have an opportunity to comment, please leave at least 24 hours between the time an MR is opened and assigning to @marge-bot.

## 5. Modifying the validation pipeline

By default we run a selected few jobs in each merge request on the most common platforms.

When your patch is tested for merging by marge bot the full pipeline is run, which may show additional unanticipated failures in your patch.

You can use *labels* to modify the configurations which are tested on a merge request pipeline.

* ~"full-ci": Run a complete validation pipeline which tests many more architectures and configurations
* ~"user-facing": Run head.hackage on your MR to test any user-facing changes which might affect libraries.
* ~"test-bootstrap": Test the bootstrapping scripts to make sure we can build with several different boot compilers.
* ~"test-reinstall": Test the cabal-reinstall job, which checks that we can build all the packages using a vanilla cabal.
* ~"test-primops": Run test-primops, a testsuite which generates randomised tests for primops.

You apply a label using the "Labels" button in the right hand column.

## 6. Merging your merge request

We use @marge-bot to automatically rebase and merge branches to `master`. 
After your merge request has been reviewed and approved, you can request that it be added to the merge queue by @-mentioning @triagers. Your MR should be added to the merge queue within a day or two, assuming it passes CI and the commit history is in order (individually buildable commits with informative commit messages).

As long as your MR satisfies the following, GitLab will batch your MR with other MRs and attempt to merge into `master`:

* the MR is assigned to @marge-bot,
* the MR passes CI
* the MR has no merge conflicts with `master` (see [rebasing](#rebasing)).

Each batch is an MR of its own, which must itself pass CI. You can expect GitLab to merge two or three batches per day.

## 7. Rebasing

You generally do *not* need to rebase your MRs unless there are merge conflicts with `master`. GitLab will automatically rebase on top of `master` when batching MRs.

## 8. Backports

After a patch has made it to `master` it might be appropriate to backport it to the stable branch (e.g. `ghc-8.8`). If backporting is desired first ensure that the issue is milestoned for the next minor release and mark the merge request containing the fix with the appropriate "backport needed" label(s) (e.g. ~"backport needed:9.8").

While the release manager can perform the backport on your behalf, it is appreciated if you open a merge request with the backported patches yourself. There are two ways to backport a merge request:

 * Via the web interface using the "Cherry-pick" button on the merged MR. While convenient, this is only possible if there are no merge conflicts with the stable branch. Be sure to **select the correct target branch**.
 * Via the command-line using the `git cherry-pick` command. In this case select the appropriate "backport" template (e.g. `backport-for-ghc-9.8`) when creating your merge request.

After the merge request is created ensure it has the ~backport label applied and that its milestone is set appropriately.

Once the backport MR lands the ~"backport needed" label can be removed from the source MR.

## 9. GitLab mechanics

Here we give a bit more guidance about the mechanics of merge requests

### 9.1. Opening a merge request

To open a merge request:

1. Push your branch. You may push either to your fork or the primary ghc/ghc> project.
2. Starting from the [GHC GitLab project](https://gitlab.haskell.org/ghc/ghc) click on the *Merge Requests* link in the left navigational bar.
3. Click on the green *New Merge Request* button on the top right corner of the Merge Requests page
4. In the left drop-down of the *Source branch* pane select the project to which you pushed your branch.
5. In the right drop-down of the *Source branch* pane select the name of your branch.
6. Click on the green *Compare branches and continue* button.
7. Give your merge request a title. Suffix with the issue number e.g "Fix some bug #12345"
8. Write a description of your change. This should be ideally at very least a few sentences to help reviewers understand what you have done. If your change requires any changes to [submodules](https://gitlab.haskell.org/ghc/ghc/-/wikis/working-conventions/git/submodules), please be sure to include links to the upstream merge requests in your description.
9. Click on the green *Submit merge request* button.

## 9.2. Working with your merge request

Your merge request shows you several panes of information:

Top-most is the request's title and description. These can be edited by pressing the yellow `Edit` button on the top-right corner of the page.

The next pane summarises your merge request, showing the source and target branches.

![request-summary](uploads/a6b259498530ea48964a11c2539447d2/request-summary.png)

The next pane shows the status of the merge request's continuous integration builds. If the build has failed you can click on the red *X* icon to see the build log.

![pipeline](uploads/3e1bf21a870eef5608665f9d6e6a48de/pipeline.png)

At the bottom of the page is the code review interface, consisting of several tabs:

![code-review](uploads/f90d8a2f0a3716669546ae1fd43480c1/code-review.png)

The *Discussion* tab shows a summary of the comments left on the change. This includes comments left in-line in the code, as well as those independent of the code. 

The *Commits* tab lists the commits being proposed for merge. These may be clicked upon to restrict the diff to only changes made by that commit. 

The *Pipelines* tab provides a more detailed overview on the state of the various continuous integration jobs associated with the merge request.

Finally, the *Changes* tab shows the patch itself. This view may be restricted to changes made by a single commit by selecting the commit in the *Commits* tab. Moreover, one may view previous iterations of the merge request using the two drop-down menus at the top of the tab. To leave an inline comment click on a line in the patch.

