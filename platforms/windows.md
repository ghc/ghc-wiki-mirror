GHC supports the following Windows releases:

| GHC Version   | \<= WinNT4   | Win2k   | WinXP   | Vista   | Win7   | Win8   | Win10 Creators   | Win10    | Win11   |
| ------------- | ------------ | ------- | ------- | ------- | ------ | ------ | ---------------- | -------  | ------- |
| 7.8           |              | ✔       | ✔       | ✔       | ✔      |        |                  |          |         |
| 7.10          |              |         | ✔       | ✔       | ✔      | ✔      |                  |          |         |
| 8.0           |              |         |         | ✔       | ✔      | ✔      | ✔ (Note 1)       | ✔        |         |
| 8.2           |              |         |         | ✔       | ✔      | ✔      | ✔                | ✔        |         |
| 8.4           |              |         |         | ✔       | ✔      | ✔      | ✔                | ✔        |         |
| 8.6           |              |         |         |         | ✔      | ✔      | ✔                | ✔        |         |
| 8.8           |              |         |         |         | ✔      | ✔      | ✔                | ✔        |         |
| 8.10          |              |         |         |         | ✔      | ✔      | ✔                | ✔        |         |
| 9.0           |              |         |         |         |        |        | ✔                | ✔        |         |
| 9.2           |              |         |         |         |        |        | ✔                | ✔        |         |
| 9.4           |              |         |         |         |        |        | ✔                | ✔        | ✔       |
| 9.6           |              |         |         |         |        |        | ✔                | ✔        | ✔       |
| 9.8           |              |         |         |         |        |        | ✔                | ✔        | ✔       |

**Note 1**: Only distributions specifically advertising support the Creator's
Update will work with Windows 10 Creator's Update and later. 

Note that Vista must be patched with KB2533623.

Note that as of GHC 9.0 32-bit Windows targets are no longer supported (#18487).

## Building 64-bit Windows programs

Releases of GHC since 7.6.1 also provide a 64-bit Windows version for building
64-bit programs on Windows. It ships with a
[MinGW-w64](http://mingw-w64.sourceforge.net/) system bundled, which provides
tools (such as a C compiler, linker and assembler) that are used by the
compiler.

## Building on Windows

Build instructions for Windows are incorporated in the [Building
Guide](building).  In particular, here is how to [set up your Windows system
for building GHC](building/preparation/windows).


