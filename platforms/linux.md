# Linux distribution version matrix

The table below is produced from data from [repology](https://repology.org) collected using the [`distro-versions`](https://gitlab.haskell.org/bgamari/ghc-utils/-/tree/master/rel-eng%2Fdistro-versions) utility in bgamari/ghc-utils>.

<table>
<tr>
<th>

</th>
<th>gcc</th>
<th>binutils</th>
<th>glibc</th>
<th>gmp</th>
<th>ncurses</th>
<th>tinfo</th>
</tr>
<tr>
<th>Alpine 3_10</th>
<td>8.3</td>
<td>2.32</td>
<td>?</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_11</th>
<td>9.3</td>
<td>2.33</td>
<td>?</td>
<td>6.1</td>
<td>5.9</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_12</th>
<td>9.3</td>
<td>2.34</td>
<td>?</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_13</th>
<td>10.2</td>
<td>2.35</td>
<td>?</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_14</th>
<td>10.3</td>
<td>2.35</td>
<td>?</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_15</th>
<td>10.3</td>
<td>2.37</td>
<td>?</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_16</th>
<td>11.2</td>
<td>2.38</td>
<td>?</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_17</th>
<td>12.2</td>
<td>2.39</td>
<td>?</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_8</th>
<td>6.4</td>
<td>2.30</td>
<td>?</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Alpine 3_9</th>
<td>8.3</td>
<td>2.31</td>
<td>?</td>
<td>6.1</td>
<td>5.9</td>
<td>?</td>
</tr>
<tr>
<th>CentOS 6</th>
<td>4.4</td>
<td>2.20</td>
<td>2.5</td>
<td>4.3</td>
<td>5.7</td>
<td>?</td>
</tr>
<tr>
<th>CentOS 7</th>
<td>4.8</td>
<td>2.27</td>
<td>2.17</td>
<td>6.0</td>
<td>5.9</td>
<td>?</td>
</tr>
<tr>
<th>Debian 10</th>
<td>8.3</td>
<td>2.26</td>
<td>2.28</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Debian 11</th>
<td>9.3</td>
<td>2.35</td>
<td>2.31</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Debian 12</th>
<td>3.3</td>
<td>2.35</td>
<td>2.36</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Debian 8</th>
<td>4.9</td>
<td>0.23</td>
<td>2.19</td>
<td>6.0</td>
<td>5.9</td>
<td>?</td>
</tr>
<tr>
<th>Debian 9</th>
<td>6.3</td>
<td>2.26</td>
<td>2.24</td>
<td>6.1</td>
<td>6.0</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 27</th>
<td>7.2</td>
<td>2.29</td>
<td>2.26</td>
<td>6.1</td>
<td>6.0</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 28</th>
<td>8.0</td>
<td>2.29</td>
<td>2.27</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 29</th>
<td>8.2</td>
<td>2.31</td>
<td>2.28</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 30</th>
<td>8.3</td>
<td>2.30</td>
<td>2.29</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 31</th>
<td>9.2</td>
<td>2.32</td>
<td>2.30</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 32</th>
<td>10.0</td>
<td>2.34</td>
<td>2.31</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 33</th>
<td>9.2</td>
<td>2.34</td>
<td>2.32</td>
<td>6.1</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 34</th>
<td>11.0</td>
<td>2.34</td>
<td>2.33</td>
<td>6.1</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 35</th>
<td>11.2</td>
<td>2.37</td>
<td>2.34</td>
<td>6.1</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 36</th>
<td>11.2</td>
<td>2.37</td>
<td>2.35</td>
<td>6.1</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Fedora 37</th>
<td>12.2</td>
<td>2.38</td>
<td>2.36</td>
<td>6.1</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Rocky 8</th>
<td>8.5</td>
<td>2.30</td>
<td>2.28</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Rocky 9</th>
<td>11.3</td>
<td>2.35</td>
<td>2.34</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Ubuntu 16.04 (xenial)</th>
<td>5.3</td>
<td>2.26</td>
<td>2.19</td>
<td>6.1</td>
<td>6.0</td>
<td>?</td>
</tr>
<tr>
<th>Ubuntu 18.04 (bionic)</th>
<td>8</td>
<td>2.30</td>
<td>2.27</td>
<td>6.1</td>
<td>6.1</td>
<td>?</td>
</tr>
<tr>
<th>Ubuntu 20.04 (focal)</th>
<td>9.3</td>
<td>2.34</td>
<td>2.30</td>
<td>6.2</td>
<td>6.2</td>
<td>?</td>
</tr>
<tr>
<th>Ubuntu 22.04 (jammy)</th>
<td>11.2</td>
<td>2.38</td>
<td>2.35</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
<tr>
<th>Ubuntu 22.10 (kinetic)</th>
<td>12.2</td>
<td>2.39</td>
<td>2.36</td>
<td>6.2</td>
<td>6.3</td>
<td>?</td>
</tr>
</table>

