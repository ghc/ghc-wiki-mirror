This page summarises the state of play of GHC's release engineering efforts.

The table below summarizes recent major GHC releases and their current support status. For information about future releases, including their expected release date, click on the release's milestone in the table below.

| Series | Most recent release | Next planned release | Status |
|--------|---------------------|----------------------|--------|
| Nightlies | N/A | N/A | See [Section 2 below](#2-ghc-nightlies) |
| 9\.10 | None | %9.10.1 (#24374) | :large_blue_circle: Next major series |
| 9\.8 | %9.8.2 | %9.8.3 | :large_blue_circle: Current major release |
| 9\.6 | %9.6.4 | %9.6.5 | 🟢 Stable |
| 9\.4 | %"9.4.8"  | None | 🟢 Stable but no further releases planned |
| 9\.2 | %9.2.8 | None | 🟡 Stable but no further releases planned |
| 9\.0 | %9.0.2 | None | :red_circle: Not recommended for use; no further releases planned |
| 8\.10 | %8.10.7 | None | :red_circle: Not recommended for use; no further releases planned |

The link in the "Most recent release" and "Next planned release" columns are to _milestones_, sometimes with a _tracking ticket_ in parentheses. To understand what a milestone is, and a tracking ticket, see [Section 1 (release practices)](#1-release-practices).

Links to all previous releases are in [Section 2](#2-major-release-series).

Further details on our release policies can be found in on the [Wiki](working-conventions/releases). We release GHC on multiple platforms; the [platforms page](platforms) gives details.

## 1\. Release practices

Each GHC release has an associated Gitlab

* **Milestone**, denoted by the `%` sigil; example %9.8.1. The milestone carries information such as which issues we are expecting to address in the release and the **release schedule**.
* **Tracking ticket**, an issue, denoted by the `#` sigil, bearing the ~"T::release tracking" label; example #23563. The tracking ticket contains internal bookkeeping and a chronological account of the current status of the release.

GHC releases fall into two categories:

* A **major release** carries version number `N.M.1`, and may include major new features and breaking changes.
* A **minor release**, with a version `N.M.2`, `N.M.3` etc, contains (only) bug-fixes to the major release.

**Platforms**: GHC works on a number of platforms, arranged in "tiers" with different levels of support. [This page is the canonical description of those tiers](https://gitlab.haskell.org/ghc/ghc/-/wikis/platforms).

### 1\.1. Major releases

Major releases are currently produced twice a year. The schedule for a major release looks like this:

* **Fork**: a branch in the repository is created, named for the release, e.g. `ghc-9.8`.
  * All changes to `base` package slated for the future release should be approved by CLC and land before forking. Backports are usually reserved to security or packaging matters only.
  * All breaking changes to boot packages (such as `bytestring` and `text`) should be reflected in the GHC source tree by upgrading respective submodules before forking.
* **Alpha releases**: a series of pre-releases, called `alpha 1`, `alpha 2`, etc, allow GHC's developers and users to test the release.
  * This prerelease window typically lasts three months, with an alpha every two-to-three weeks, resulting in four or five alphas.
  * Boot libraries should be finalised, including all minor changes, and released by the penultimate alpha release (usually `alpha 3`).
* **Release candidate** is a release is supposed to be ready to go. Sometimes there is a quick succession of release candidates as minor glitches are fixed called `RC1`, `RC2`, etc.
* **Final release**.

### 1\.2 Minor releases

Minor releases generally occur on an as-needed basis without a pre-determined schedule. But once we have decided to make a minor release, we post the schedule on the milestone. Unlike major releases, minor releases do not have an associated set of pre-releases.

### 1\.3. See also

* [release annoucements](https://www.haskell.org/ghc/blog.html)
* [migration guide](migration)
* [library version history](commentary/libraries/version-history)
* [pragma history](language-pragma-history)
* [milestones](https://gitlab.haskell.org/ghc/ghc/milestones)
* [Haskell Communities and Activities Report submissions](HCAR-Status-Reports)

## 2\. GHC Nightlies

A channel for recent builds of GHC is available. This allows you to install a recent build (a so-called "nightly build") using `ghcup`.

:warning: This feature is **EXPERIMENTAL** and the GHC team can make **NO** promises about its usefulness right now. **It is suitable for experimental use only.**

GHC Nightlies are currently subject to the following qualifications:

1. :warning: They cannot be guaranteed to be updated nightly. That is "recent build" does not necessarily mean "last night".
2. :warning: They may disappear altogether for a while if the GHC Nightly Pipeline experiences a long string of failures.
3. :warning: A "recent build" is not subject to the same quality control as a full release. It has been tested by CI, and is a snapshot of GHC's master branch, but that's all.

An up to date status of Nightlies can be checked on the [Nightly Availability Dashboard](https://grafana.gitlab.haskell.org/d/ab109e66-a8a1-4ae9-b976-40e2dfe281ab/availabilitie-of-ghc-nightlies-via-ghcup?orgId=2&from=1681980144490&to=1697791344490&refresh=1d).

With these qualifications firmly in mind, you can fetch GHC Nightlies via GHCup with the following commands.

* First, [get GHCup](https://www.haskell.org/ghcup/).

  Then, to add the nightly channel, run:

  ```sh
  ghcup config add-release-channel https://ghc.gitlab.haskell.org/ghcup-metadata/ghcup-nightlies-0.0.7.yaml
  ```
* To list all nightlies from 2023, run:

  ```sh
  ghcup list --show-nightly --tool=ghc --since=2023-01-01
  ```
* Ways to install a nightly:

  ```sh
  # by date
  ghcup install ghc 2023-06-20
  # by version
  ghcup install ghc 9.7.20230619
  # by tag
  ghcup install ghc latest-nightly
  ```

## 3\. Major release series

### 3\.1 GHC 9.x releases

| Milestone | Date | Tracking ticket | Release manager | Downloads |
|-----------|------|-----------------|-----------------|-----------|
| %9.8.2 | 23 Feb 2024 | #24079 | @wz1000 | [downloads](https://www.haskell.org/ghc/download_ghc_9_8_2.html) |
| %9.8.1 | 9 Oct 2023 | #23563 | @bgamari | [downloads](https://www.haskell.org/ghc/download_ghc_9_8_1.html) |
| %9.6.4 | 9 Jan 2024 | #24017 | @wz1000 | [downloads](https://www.haskell.org/ghc/download_ghc_9_6_4.html) |
| %9.6.3 | 25 Sept 2023 | #23759 | @wz1000 | [downloads](https://www.haskell.org/ghc/download_ghc_9_6_3.html) |
| %9.6.2 | 23 May 2023 | #23433 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_6_2.html) |
| %9.6.1 | 10 Mar 2023 | #22562 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_6_1.html) |
| %9.4.8 | 20 Aug 2023 | #24128 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_4_8.html) |
| %9.4.7 | 10 November 2023 | #23819 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_4_7.html) |
| %9.4.6 | 7 Aug 2023 | #23710 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_4_6.html) |
| %9.4.5 | 18 Apr 2023 |  | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_4_5.html) |
| %9.4.4 | 24 Dec 2022 | #22597 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_4_4.html) |
| %9.4.3 | 3 Nov 2022 | #22337 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_4_3.html) |
| %9.4.2 | 22 Aug 2022 | #22083 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_4_2.html) |
| %9.4.1 | 7 Aug 2022 | #21127 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_4_1.html) |
| %9.2.8 | 26 May 2023 |  | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_2_8.html) |
| %9.2.7 | 27 Feb 2023 |  | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_2_7.html) |
| %9.2.6 | 10 Feb 2023 | #22598 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_2_6.html) |
| %9.2.5 | 7 Nov 2022 | #22407 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_2_5.html) |
| %9.2.4 | 28 Jul 2022 | #21815 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_9_2_4.html) |
| %9.2.3 | 27 May 2022 | #21326 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_2_3.html) |
| %9.2.2 | 6 Mar 2022 | #21074 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_2_2.html) |
| %9.2.1 | 29 Sep 2021 | #19321 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_2_1.html) |
| %9.0.2 | 25 Dec 2021 | #19632 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_0_2.html) |
| %9.0.1 | 4 Feb 2021 | #18216 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_9_0_1.html) |

### 3\.2 GHC 8.x releases

| Release | Date | Tracking ticket | Release manager | Downloads |
|---------|------|-----------------|-----------------|-----------|
| %8.10.7 | 27 Aug 2021 | #20296 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_8_10_7.html) |
| %8.10.6 | 15 Aug 2021 | #19987 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_8_10_6.html) |
| %8.10.5 | 5 Jun 2021 | #19859 | @wz1000 | [downloads](https://haskell.org/ghc/download_ghc_8_10_5.html) |
| %8.10.4 | 5 Feb 2021 | #19333 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_10_4.html) |
| %8.10.3 | 19 Dec 2020 | #19070 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_10_3.html) |
| %8.10.2 | 8 Aug 2020 | #18518 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_10_2.html) |
| %8.10.1 | 24 Mar 2020 | #17214 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_10_1.html) |
| %8.8.4 | 15 Jul 2020 | #18366 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_8_4.html) |
| %8.8.3 | 24 Feb 2020 | #17860 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_8_3.html) |
| %8.8.2 | 16 Jan 2020 | #17457 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_8_2.html) |
| %8.8.1 | 25 Aug 2019 | #16959 | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_8_1.html) |
| %8.6.5 | 23 Apr 2019 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_6_5.html) |
| %8.6.4 | 5 Mar 2019 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_6_4.html) |
| %8.6.3 | 7 Dec 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_6_3.html) |
| %8.6.2 | 2 Nov 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_6_2.html) |
| %8.6.1 | 21 Sep 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_6_1.html) |
| %8.4.4 | 29 May 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_4_4.html) |
| %8.4.3 | 29 May 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_4_3.html) |
| %8.4.2 | 19 Apr 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_4_2.html) |
| %8.4.1 | 8 Mar 2018 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_4_1.html) |
| %8.2.2 | 22 Jul 2017 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_2_2.html) |
| %8.2.1 | 22 Jul 2017 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_2_1.html) |
| %8.0.2 | 11 Jan 2017 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_0_2.html) |
| %8.0.1 | 21 May 2016 | N/A | @bgamari | [downloads](https://haskell.org/ghc/download_ghc_8_0_1.html) |

### 3\.3 Older releases

- [GHC 7.10.3](status/ghc-7.10.3)
- [GHC 7.10.2](status/ghc-7.10.2)
- [GHC 7.10.1](status/ghc-7.10.1)
- [GHC 7.8.4](status/ghc-7.8.4)
- [GHC 7.8.3](status/ghc-7.8.3)
- [GHC 7.8.1](status/ghc-7.8)
- [GHC 6.12](status/ghc-6.12)
- [GHC 6.10](status/ghc-6.10)