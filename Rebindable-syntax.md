This wiki page tracks the progress on ticket #17582, in which we re-engineer rebindable syntax away from the custom "syntax operator" logic (and `tcSyntaxOp`).

Instead, we should use `HsExpansion`, as detailed in `Note [Rebindable syntax and HsExpansion]` in `GHC.Hs.Expr`. Note however, that where possible we do the expansion in the Renamer, but sometimes we need typechecker information, so we do it "on the fly" in the typechecker.

The main note in GHC which keeps track of progress is `Note [Handling overloaded and rebindable constructs]` in `GHC.Rename.Expr`; any MR that moves us forward should be sure to update it.

Implementation status:

* [x] If-then-else.
* [x] Overloaded labels.
* [x] Overloaded lists (in expressions and in patterns).
* [x] Left and right sections.
* [x] Record updates.
* [ ] Overloaded strings (Related #24390).
* [ ] Rebindable overloaded literals (`fromInteger`, `fromRational`), in expressions and in patterns.
* [ ] Negation (in expressions and in patterns).
* [ ] `(n+k)` patterns.
* [ ] Do notation
    * [x] Vanilla Do Notation (!10140). Tickets are #21206 #18324 #20020 #23147 #22788 #15598 #22086 
    * [x] Recursive Do (`mdo`) (#24411)
    * [ ] Applicative Do (#24406)
    * [ ] GHCi Stmts (#24419)
* [ ] Arrow notation (should probably wait until do notation is handled). See #23061

(The following table was taken from https://gitlab.haskell.org/ghc/ghc/-/issues/17582#note_257552, it might be slightly out of date.)

| Source code | Transformed (RS off) | Transformed (RS on) | Rebindable name |
|-------------|----------------------|---------------------|-----------------|
| `if cond then t else f` | `if cond then t else f` | `ifThenElse cond t f` | `ifThenElse` |
| `365` | `Prelude.fromInteger (365 :: Integer)` | `fromInteger (365 :: Integer)` | `fromInteger` |
| `3.8` | `Prelude.fromRational (3.8 :: Rational)` | `fromRational (3.8 :: Rational)` | `fromRational` |
| `"foo"` | `"foo"`/`Data.String.fromString ("foo" :: String)` (OS) | `fromString ("foo" :: String)` (OS) | `fromString` |
| `[True, False]` | `True : False : []`/`GHC.Exts.fromListN 2 (True : False : [])` (OLi) | `fromListN 2 (True : False : [])` (OLi) | `fromListN` |
| `[1..5]` | `enumFromTo 1 5`/`GHC.Exts.fromList (enumFromTo 1 5)` (OLi) | `fromList (enumFromTo 1 5)` (OLi) | `fromList` |
| `#field1` | `GHC.OverloadedLabels.fromLabel @"field1"` (OLa) | `fromLabel @"field1"` (OLa) | `fromLabel` |
| `arr id` | `Control.Arrow.arr id` | `arr id` | `arr` (ditto for `>>>`, `first`, `app`, \` |
| `do { a <- m; f a }` | `m Prelude.>>= \a -> f a` | `m >>= \a -> f a` | `(>>=)`, `(>>)` and `fail` |
| `case n of { 0 -> True; _ -> False }` | Uses `Eq`'s `(==)` | Uses local `(==)` | `(==)` |
| `- (f x)` | `Prelude.negate (f x)` | `negate (f x)` | `negate` (ditto for `-` and `>=` in n+k patterns) |

(OLi = when `OverloadedLists` is enabled, OLa = when `OverloadedLabels` is enabled, OS = when `OverloadedStrings` is enabled)