_This is a more conceptual-level than implementation-level companion to _[_design/type-refactor_](design/type-refactor)_._

For a number of reasons, one might want types in Core to be more minimal than types in Haskell. Assuming that is a goal worth pursuing, how can we make it work? A big challenge is type families.

## Desugaring in the presence of type families

Suppose we have a type family `Foo`, and a function `bar` like

```haskell
bar :: forall a. a -> Foo a
bar x = ...
```

In general, we will have to compile this to Core without specializing it. In other words, we will have to desugar `Foo a` without first being able to simplify the type family application. This is, in general, a problem for any type-level open term which causes type family applications to so become "stuck".

If `a` is to become a core `type`, and `Foo` a Core "axiom", then `Foo` must not discriminate between two Haskell types that become the same Core type. We can implement this restriction by coarsening the "apartness relation" that type families must abide by, but this isn't the most satisfactory solution: it is tantamount to a decision that whatever distinctions Haskell types are making beyond what Core types make, those distinctions are unimportant. For something like visibility (see https://github.com/ghc-proposals/ghc-proposals/issues/558) this is probably fine, but for more exciting ways we might want to desugar types, it is not.

There is however another solution: don't do

```plaintext
⟦Foo a⟧_t = ⟦Foo⟧_t ⟦a⟧_t
```

i.e. don't desugar `Foo` and `a` separately into core types. How might that be done? Quite simply, there is no need to get rid of Haskell types in Core! Let's take a step back, what do we mean by "Haskell Types" and "Core Types"? We mean that which goes on the RHS of a typing judgement (`::` in Haskell, sometimes or `:` in Core). `Type` is indeed the kind of both these today (when Haskell and Core types are the same). But `Type` is not the only kind, thanks to `-XDataKinds` we have an open world of other kinds. But inhabitants of those never go on a RHS, e.g. there is no valid program like this:

```haskell
undefined :: 5 :: Int
```

`-XDataKinds` effects Core and Haskell alike. That means already, there are plenty of Core type-level elements that are not types. We can simply "keep around" Haskell types in this way too: Haskell's `Type` --- as is, not desugared --- can just be another "data kind" in Core without any special privileges; not part of the language but just a use of a language?

This gives us another way to compile `Foo a`: we just keep `Foo` as a type family on Haskell types, and `a` a variable with kind `Type`. If we need to actually type a Core term, however, with one of these open Haskell types (thanks to `a`) which we keep as a Haskell type in Core, we _defer_ the desugaring by reifying it with another type family. Let's call that type family `HsToCoreType :: Type -> CoreType`.

Let's work this out with our example.

First, let's be more explicitly typed at the Haskell level so all our desugaring "obligations" are clearer. `bar` is now:

```haskell
bar :: forall (a :: Type). a -> Foo a
bar @a (x :: a) = ... :: Foo a
```

The two new signatures `x :: a` and `... :: Foo a` are just the sorts of situations we described above, where a haskell term/pattern we need to desugar to Core is typed with a stuck open type.

The desugaring proposed looks like this:

```plaintext
bar :: forall (a : Type). HsToCoreType a ->_Core HsToCoreType (Foo a)
bar = Λ (a : Type) -> λ (x : HsToCoreType a) -> ... : HsToCoreType (Foo a)
```

The `forall` / Λ still quantifies over Haskell types, but on the inside we have a regular core function from core types to core types, thanks to the prolific use of `HsToCoreType`.

This might appear as a sort of do-nothing desugaring, but that is OK! Fundamentally, for a term that relies on distinctions we'd like to get rid of to be well typed, it will have to "drag with it" remnants of its previous representation / the previous language. That seems like a pretty unavoidable penalty to me, and one I am fine paying, because we are encoding them only at the "library level" not the "language" level of Core.

The good thing is that in cases like `id a = a` where the fine distinction of Haskell types do not matter, we can do a worker-wrapper trnsformation to desugar futher:

```plaintext
id :: forall (a : Type). HsToCoreType a ->_Core HsToCoreType a
id = Λ (a : Type) -> id' @(HsToCore a)

id' :: forall (a : CoreType). a ->_Core a
id' = Λ (a : CoreType) -> λ (x : a) -> ... : HsToCoreType a
```

And most code will just be able to use `id'` directly, inlining the wrapper.

> **JB TODO**: Add example for an open type family, which might need to be deferred even when applied to concrete types
>
> **JB TODO**: Explicitly state that the reason we need to use `HsToCoreType` is that `Foo`'s _codomain_ is `Type`, whereas the reason we need to quantify over `Type` is that `Foo`s _domain_ is `Type` - If I understand correctly\
> → This implies that these properties can occur independently, e.g. for `Bar :: Nat -> Type` we have to use `HsCoreToType`, and for `Baz :: Type -> Nat` we have to quantify over `Type`.
>
> **JE** Good point!
>
> **JB**: I've also been wondering but am not 100% sure yet what should happen if the domain/codomain is `Type -> Type` or similar, instead of `Type`
>
> **JE**: For classes / datatypes (things that make matchable `->`) that don't use type families, I think all the `Type` can become `CoreType`. Those Haskell data definitions become Core data definitions so we don't need a `CoreTypeToHsType` right away. We should indeed _never_ need that but I need to think about how to demonstrate that.
>
> **JB**: What I'm wondering is if you have something like
>
> ```haskell
> newtype NewList a = MkNewList (List a)
> type F :: (Type -> Type) -> Nat
> type family F ctr where
>   F NewList = 1
>   F List = 2
> f :: forall (ctr :: Type -> Type) . Something (F ctr)
> ```
>
> What does the kind of `ctr` become in Core? I think it has to stay `Type -> Type`?
>
> **JE**: In the version of this section, yes it does.
> In general, any polymorphism has to keep its original kind.
> Being able to change it per the `id` / `id'` worker-wrapper example only happens in simpler type-family-free (but common cases).
>
> (In the version below, however, yes we get rid of `Type -> Type` turning it into `CoreType -> CoreType` since the `F` usage requires a dictionary.)

## Constrained type families

> **JB**: Should this be "_associated_ type families"?
>
> **JE**: It applies to both open and closed, actually. Richard's paper introduced _Closed Type Classes_ for closed type families. The end result is all type families are associated. Now, Dependent Haskell would say we can promote functions too, but I don't consider that a risk to for two reasons:
>
> 1. OK to case on things besides `Type` (and `CoreType`).
> 2. "Promoted" partial functions would fail outside the domain, which IMO is better then how type families today get stuck outside their domain. The latter makes for harder to debug type errors / obfuscate the root problem.

An astute reader might have noticed that the above desugaring imposes a certain "viral" cost. For example, suppose we have another function `baz` which calls `bar`:

```haskell
baz :: forall a. a -> [a]
baz x = ... bar @a ...
```

`baz` has no type families in its type / external interface. But since `bar` is desugared to a polymorphic function that still quantifies over Haskell types, and `baz` instantiates it with its own `a`, `baz` must _also_ quantify over Haskell types. This obligation bubbles all the way out until some transitive caller instantiates with a closed type.

This sounds quite bad: it could mean that almost the entire core program is quantifying over Haskell types because of type families deep within it. However, there are reasons to think it is less bad in practice. For one, the `a -> Foo a` type I've been using in my example cannot be written without partiality, unless `Foo` only has one case. It's not the sort of thing Haskellers intentionally write. What is more likely is something like

```haskell
bar :: FooClass a => a -> Foo a
```

where `Foo` is _associated_ with `FooClass`.

Today, this association is very informal, GHC and Core do not take advantage of it. But suppose it was formal: suppose `Foo a` induced a wanted constraint with the idea that rather than "casing" on the type, we just "look up the result type in the dictionary".

For example, suppose `A` and `B` are Haskell types which, for whatever reason, desugar to the same Core type `C`.

In haskell, we have

```haskell
class FooClass a where
  type Foo a

instance FooClass A where
  type Foo A = Int

instance FooClass B where
  type Foo B = Bool

bar :: forall a. FooClass a => a -> Foo a
bar x = ...
```

In Core, with some strawman features thrown in, this could look like this:

```plaintext
data FooClassD (a : CoreType) = FooClassD
  { typeFamilyFoo : CoreType {-# MagicErased #-}
  }

dictAFooClass : FooClassD C
dictAFooClass = FooClassD
  { typeFamilyFoo = Int
  }

dictBFooClass : FooClassD C
dictBFooClass = FooClassD
  { typeFamilyFoo = Bool
  }

bar :: forall (a : CoreType) (d : FooClassD a) -> a -> typeFamilyFoo d
bar x = ...
```

There's a lot going on here, so let's break it down.

1. `Foo` is a field in the dictionary. It is not a function, but just the "result" of the type family application `Foo A`, _desugared to Core_. The last part is key: because we are not casing on anything, we have no more need to keep around Haskell types.

   `{-# MagicErased #-}` indicates we don't need this field at runtime. This is not important to the topic at hand, but I wanted to be clear I am not proposing we incur any runtime costs. We might be able to reuse GADT record notation for this, but I didn't want to bother as straying off topic further.
2. `dictAFooClass` and `dictBFooClass` have the same (Core) type, `FooClassD C`. Because we are using Core types, we no longer have evidence of canonicity, but this is fine because Core never tracked canonicity prior either.
3. `bar` does a dependent binding of the dictionary. It presumably already needed the dictionary `d`, at the term level. Now, it also needs it at the type level in order to project the `typeFamilyFoo` field.

> **JB**: It could be worth considering having separate dictionaries for the types and the methods - although perhaps that flies in the face of the goal unifying type and terms
>
> **JE**: Yeah I would support that. It lessons the need for `{-# MagicErased #-}`, perhaps. But note that if the term-level methods use the associated type, then we will want an erased superclass constraint for the type-level class in the term-level class, so the problem doesn't go away entirely.

...

## Getting rid of coercions with Representational Core

All of the above might seem a lot of work for little value. Sure, we can redesign type families to make this thing work well, but for what purpose? No one is begging for type families that e.g. discriminate between `forall .` and `forall ->`.

A more tantalizing goal is to use our newfound ability to throw away distinctions in Core types to throw away a lot more distinctions. A big win here would be the ability to throw away the distinction between representationally-equivalent types. Why is that a big win? It hopefully means we can get rid of representational coercions entirely! Any two Haskell types which are representationally equivalent we want to compile to Core types which are nominally equivalent, so there is nothing which needs to be coerced.

The first challenge of this is that it will create occurs-check violating things. Newtypes would be just gotten rid of, but that will defeat newtypes being used to "break cycles". So we'll need mu types or something like it. But IIRC the problems with mu types are with type inference, so we should be OK.

The second challenge is that merely unwrapping newtypes doesn't do anything for coercions based on phantom roles don't go away, e.g. `Proxy Int ~#R Proxy Bool`. I don't yet know how to do deal with that.

> **JB**: So my understanding of coercions is far from perfect, but is there a plan for getting around type family axioms being used as coercions in Core, since non-`Type` type families still need to be reduced? It seems like this would make this goal tricky to accomplish (unless it just means "Getting rid of newtype-coercions")
>
> **JE**: The goal would be to _have no coercions at all_. See what I aded above.

...
