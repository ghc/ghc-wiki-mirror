GHC uses mutable references to represent unification variables (also
called metavariables); see the `MetaTv` constructor of `TcTyVarDetails` in `GHC.Tc.Utils.TcType`. During typechecking, these mutable references
are filled in via unification. Zonking traverses a type
and replaces all mutable references with the type that they
dereference to; thus, the resulting structure is immutable and
requires no dereferencing to interpret.

Note that these type variables are meta-variables, i.e. they don't
correspond to the type variables introduced by polymorphism; rather,
they are unification variables to be replaced by real types. In error
messages, these type variables are printed with a trailing number, like
`a0`, `a1`, etc.

This notion of zonking extends naturally to other intermediate
representations of the typechecker that contain types; see the modules
under the `GHC.Tc.Zonk` hierarchy.
