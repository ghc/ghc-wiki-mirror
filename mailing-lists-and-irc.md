GHC developers hang out on email, Matrix, IRC, and GitLab. You're already on GitLab. :) Here's more information about the others.

## Mailing Lists

The [ghc-devs mailing list](http://www.haskell.org/mailman/listinfo/ghc-devs) is suitable for contributors and users of GHC. Topics are usually advanced, but any discussion of GHC is welcome.

More general-purpose Haskell mailing list are [haskell-cafe](https://www.haskell.org/mailman/listinfo/haskell-cafe) and [libraries](https://mail.haskell.org/mailman/listinfo/libraries).

## Matrix

Join the [#GHC channel on Matrix](https://matrix.to/#/#GHC:matrix.org).

[Matrix](https://matrix.org/) is now the default chat network for GHC. There are many different clients, but to get started, we recommend the standard app called "Element".

This is a good place to ask questions related to working on GHC itself.

There is a large [Haskell Space on Matrix](https://matrix.to/#/#haskell-space:matrix.org) with many different channels for different topics besides GHC.

## IRC

GHC developers also hang out on `#ghc` channel on [libera.chat](https://libera.chat/). This channel is bridged to the Matrix channel, meaning you can join via IRC or Matrix and end up in the same channel.

Similar to the mailing lists, there is also a more general-purpose IRC channel, `#haskell`.

If you are unfamiliar with IRC, start with Matrix.