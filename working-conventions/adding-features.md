# How to contribute a new feature to GHC

We welcome your involvement in making GHC able to do more.

* Proposals for a change to the libraries should be sent to the relevant library maintainer (or the [Core Libraries Committee](https://github.com/haskell/core-libraries-committee) for changes to `base`).

* Proposals for language extensions and significant new user-facing features should be sent to the [GHC Steering Committee](https://github.com/ghc-proposals/ghc-proposals/) and follow the proposals process. (It is fine to open a [feature request](https://gitlab.haskell.org/ghc/ghc/issues) **issue** on the GitLab issue tracker as well, especially if you are unsure whether a ghc-proposal is required for a minor feature request.)

Here's how to contribute a feature:

1. Write down the **specification** of the feature! Specifying before implementing is obviously a good idea; and it makes it possible for others to comment on your design before you invest heavily in building the feature.

1. Get **feedback** by raising a GHC proposal or opening a feature request issue. Often you'll get useful ideas. Update the specification as needed. 

1. You are welcome to **implement** your proposed feature at any stage, but bear in mind that until the proposal has been accepted by the GHC Steering Committee, there is no guarantee that a patch will be accepted.

1. Once your proposal has been accepted and the implementation is ready, follow the instructions for **[contributing a patch](/Contributing-a-Patch#merge-request-workflow)**.

1. Include updates to the **user manual** that documents your feature. For features that introduce a new flag, at least two sections need to be updated:

  - The [flag reference](https://downloads.haskell.org/~ghc/master/users-guide/flags.html) section generated from the modules in `utils/mkUserGuidePart/Options/`.
  - The flag descriptions in `docs/users_guide/using.rst` provide a detailed explanation of each flags' usage. 
1. Add a section to the latest **release notes** (`docs/users_guide/<version>-notes.rst` or `libraries/base/changelog.md`).

## Things to bear in mind


While we love new features, we think twice before incorporating them into GHC's main code base. Here are some things to bear in mind:
 

- It may seem that running 'git push' is practically free for us; you have done all the hard work.  But it isn't:

  - It effectively commits us to maintaining it indefinitely, and worrying about its interactions with existing features
  - We try hard to keep GHC's language design somewhat coherent.  GHC deliberately tries to host a variety of ideas, not all of which may be good, but we try to keep it under control.
  - We have to do quality-control on your proposal; we don't want to push un-maintainable code... or even well-written code that we can't understand.
  - If your proposal breaks something else, we'll get blamed regardless.  
  - Subsequent new features have to take account of interactions with your feature.

- New features should be switchable with their own flag, by default off.  We used to have just one flag `-fglasgow-exts` but nowadays we try to be much more selective.

- We are much happier about features whose implementation is:   

  - **Localised**: only a handful of places in the code are changed.
  - **Orthogonal**: no new invariants or constraints are added that subsequent changes must bear in mind. This is really important; any change that imposes costs on later, apparently unrelated, changes is much more costly. 


 


- New features should work in a way that is consistent, as far as possible, with the way that other
  existing GHC features work.  Adopting a variety of different styles leads to a
  system that is hard to learn, and complaints of the form "why doesn't it work like X?
  I'm familiar with X!".

- Remember that GHC HQ is not heavily staffed!  It may take us a while to give your proposal the attention it deserves. However, if you hear nothing for a couple of weeks then please feel free to ping us, in case your proposal has slipped through the cracks.


If you are working on a feature that you think is a candidate for including in GHC's main repository, you may want to talk to us while you are developing it.  We may well, for example, have advice about how to structure the change in a way that we're happy to incorporate in our code base.
