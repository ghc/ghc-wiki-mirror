This page explains how to build GHC with the JS backend enabled.

**Note:** GHC isn't multi-target yet hence a given GHC installation/bindist targets a single platform, i.e. you can't expect a GHC built for Linux/x86-64 to produce JavaScript code. Thus in order to use the JavaScript backend we must build a GHC targeted for JavaScript.

## Build Requirements

- emscripten version 3.14 or later: used to support CPP, hsc2hs, and GHC's configuration (constant deriving for the platform)
- NodeJS: for Template-Haskell and to easily run produced JS code.

### If You're using Nix

Ghc does not provide a nix derivation to build the compiler or any cross-compilers. However, we do use [ghc.nix](https://github.com/alpmestan/ghc.nix) to create a development environment which is suitable for building GHC. If you're using nix or are on NixOS we recommend using ghc.nix to enter a development environment as we regularly update the project to align dependency versions with GHC's CI runners. Please see `ghc.nix` for the exact setup depending on if you use flakes or not.

## Build Instructions

Boot the build process with:

```bash
> ./boot
```

Building GHC with the JS backend only requires some changes to the `configure` step. Run the following commands:

```bash
> emconfigure ./configure --target=javascript-unknown-ghcjs
```

**Note:** before https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9814, the target was `js-unknown-ghcjs`. Should your version of `ghcHEAD` be outdated then you'll still need to use the old target to build.

`emconfigure` is a script provided by Emscripten that sets some environment variables. In particular it sets `LD` and `CC` to the `emcc` compiler. Both variables need to be correctly set for the configure step to succeed. The compiler set by `CC` is then used for CPP support and by `hsc2hs`.

The result of the configure script should look like:

```
----------------------------------------------------------------------
Configure completed successfully.

   Building GHC version  : 9.7.20230207
          Git commit id  : 2ab26b44b2e04e4c33c26ede8d7c8cf8ae1064ce

   Build platform        : x86_64-unknown-linux
   Host platform         : x86_64-unknown-linux
   Target platform       : javascript-unknown-ghcjs

   Bootstrapping using   : /nix/store/kz0c0mva5lpc5anp5lz79h2i3jm31860-ghc-9.2.4-with-packages/bin/ghc
      which is version   : 9.2.4
      with threaded RTS? : YES

   Using (for bootstrapping) : /nix/store/yzs8390walgk2rwl6i5li2g672hdn0kv-gcc-wrapper-11.3.0/bin/cc
   Using clang               : /nix/store/vmy819gfyk30ln1vmzsnldzhj111hn13-emscripten-3.1.17/bin/emcc
      which is version       : 15.0.0
      linker options         : 
   Building a cross compiler : YES
   Unregisterised            : NO
   TablesNextToCode          : YES
   Build GMP in tree         : NO
   hs-cpp       : /nix/store/vmy819gfyk30ln1vmzsnldzhj111hn13-emscripten-3.1.17/bin/emcc
   hs-cpp-flags : -E -undef -traditional -Wno-invalid-pp-token -Wno-unicode -Wno-trigraphs
   ar           : /nix/store/vmy819gfyk30ln1vmzsnldzhj111hn13-emscripten-3.1.17/bin/emar
   ld           : /nix/store/vmy819gfyk30ln1vmzsnldzhj111hn13-emscripten-3.1.17/bin/emcc
   nm           : /nix/store/n772hzjm6hvnjw29h4hrx54f0jm8fhx4-emscripten-llvm-3.1.17/bin/llvm-nm
   objdump      : /nix/store/zgvxnf9047rdd8g8kq2zxxm9k6kfqf8b-binutils-2.38/bin/objdump
   ranlib       : /nix/store/vmy819gfyk30ln1vmzsnldzhj111hn13-emscripten-3.1.17/bin/emranlib
   otool        : otool
   install_name_tool : install_name_tool
   windres      : 
   dllwrap      : 
   genlib       : 
   Happy        : /nix/store/lwbkqjcx0a4mpsphypzphbrr2w6idlwb-happy-1.20.0/bin/happy (1.20.0)
   Alex         : /nix/store/4d5h3sbcqyv4islda4y4b3c2wwvhki62-alex-3.2.6/bin/alex (3.2.6)
   sphinx-build : /nix/store/827482fziiyg0sdmd51qvb1gqrpvmi21-python3.9-sphinx-4.5.0/bin/sphinx-build
   xelatex      : /nix/store/jhbbjy68i1mcn135hkwbrzp57jg2b802-texlive-combined-2021/bin/xelatex
   makeinfo     : /run/current-system/sw/bin/makeinfo
   git          : /nix/store/6ycia1xk500pxssx5nk1hppxh6c0rl99-git-2.36.2/bin/git
   cabal-install : /nix/store/9amnrjfck2yf9w5h38d18r2i4dcbifjb-cabal-install-3.6.2.0/bin/cabal

   Using LLVM tools
      clang : clang
      llc   : llc
      opt   : opt

   HsColour was not found; documentation will not contain source links

   Tools to build Sphinx HTML documentation available: YES
   Tools to build Sphinx PDF documentation available: YES
   Tools to build Sphinx INFO documentation available: YES
----------------------------------------------------------------------
```

You can also double check this output in `ghc/hadrian/cfg/system.config`:

```
# This file is processed by the configure script.
# See hadrian/src/UserSettings.hs for user-defined settings.
#===========================================================

# Paths to builders:
#===================

alex           = /nix/store/qzgm2m7p7xc0fnyj4vy3jcmz8pvbg9p7-alex-3.2.6/bin/alex
ar             = /nix/store/p894nlicv53firllwgrfxfi51jzckh5l-emscripten-3.1.15/bin/emar
autoreconf     = /nix/store/yfnhm2b6plv48i8sgl64sd148b48hcly-autoconf-2.71/bin/autoreconf
cc             = /nix/store/p894nlicv53firllwgrfxfi51jzckh5l-emscripten-3.1.15/bin/emcc
...
```

Notice that each of the binaries very obviously point to the `emscripten` wrapped versions. If they aren't then it means `emconfigure` isn't setting the environment variables correctly for some reason (cf #22814). You can try setting them explicitly as follows:

```bash
> ./configure CC=$(which emcc) LD=$(which emcc) --target=javascript-unknown-ghcjs
```

**Note:** the JS backend doesn't use or care about other tools (`ar`, `nm`, `objdump`, etc.) so we can let `configure` use the default ones for the host platform.

We're now ready to start building.

### Build the compiler

Build using Hadrian, no surprises here. Most of the existing flavours have been modified to be JS aware:

```bash
> hadrian/build -j12 --flavour=quick --bignum=native --docs=none
```

That should take around 30 minutes to complete. `hsc2hs` should be very slow (we're sorry) because its running `emcc` several times. Similarly, expect the `CCS.hs` module itself to take minutes (again, we're sorry! I (Jeff) promise we have a ticket for it).

Now you should have a working Haskell to JavaScript compiler.

```bash
> _build/stage1/bin/javascript-unknown-ghcjs-ghc --info | grep Target
 ,("Target platform","javascript-unknown-ghcjs")
 ,("Target default backend","compiling to JavaScript")
```

**Note:** after https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9814, the path of the compiler became: `\\\_build/stage1/bin/javascript-unknown-ghcjs-ghc`, if you are on an outdated `ghcHEAD` then it will be `\\\_build/stage1/bin/js-unknown-ghcjs-ghc`.

Good luck!

## Compiling Hello World

Here is a small hello world example to inspect the build artifacts. Here's our program:

```haskell
-- HelloJS.hs
module Main where

main :: IO ()
main = putStrLn "Hello, JavaScript!"
```

and now we compile:

```bash
> <path-to-ghc-root>/_build/stage1/bin/javascript-unknown-ghcjs-ghc -fforce-recomp HelloJS.hs
[1 of 2] Compiling Main             ( HelloJS.hs, HelloJS.o )
[2 of 2] Linking HelloJS.jsexe
```

### Compiler Output and Build Artifacts

The JS backend will produce two build artifacts: (1) a binary called `HelloJS` and (2) a directory of build artifacts `HelloJS.jsexe`:

```bash
> ls
HelloJS  HelloJS.hi  HelloJS.hs  HelloJS.jsexe  HelloJS.o
```

`HelloJS` is just a wrapper that calls `nodejs` on the program payload that resides in `Hello.jsexe/all.js`:

```bash
> head HelloJS
#!/usr/bin/env node
var h$currentThread = null;
var h$stack = null;
var h$sp = 0;
var h$initStatic = [];
var h$staticThunks = {};
var h$staticThunksArr = [];
var h$CAFs = [];
var h$CAFsReset = [];
var h$regs = [];
...
```

notice the shebang points to `node`. The rest of the lines in the output of `head` is the initialization of the JS RTS. We'll find the same information by inspecting `HelloJS.jsexe/all.js`:

```bash
> head HelloJS.jsexe/all.js 
var h$currentThread = null;
var h$stack = null;
var h$sp = 0;
var h$initStatic = [];
var h$staticThunks = {};
var h$staticThunksArr = [];
var h$CAFs = [];
var h$CAFsReset = [];
var h$regs = [];
var h$r1 = 0;
...
```

`HelloJS.jsexe` contains a series of JavaScript programs that all get "linked" (that is merged together in a safe way) into the payload, i.e., `HelloJS.jsexe/all.js`:

```bash
> cd HelloJS.jsexe
> ls -l
total 4792
-rw-r--r-- 1 doyougnu users 2175097 Feb  8 08:20 all.js
-rw-r--r-- 1 doyougnu users  513080 Feb  8 08:20 all.js.externs
-rw-r--r-- 1 doyougnu users     129 Feb  8 08:20 index.html
-rw-r--r-- 1 doyougnu users  217546 Feb  8 08:20 lib.js
-rw-r--r-- 1 doyougnu users      21 Feb  8 08:20 out.frefs.js
-rw-r--r-- 1 doyougnu users       0 Feb  8 08:20 out.frefs.json
-rw-r--r-- 1 doyougnu users 1669160 Feb  8 08:20 out.js
-rw-r--r-- 1 doyougnu users   13175 Feb  8 08:20 out.stats
-rw-r--r-- 1 doyougnu users  288361 Feb  8 08:20 rts.js
-rw-r--r-- 1 doyougnu users      30 Feb  8 08:20 runmain.js
```

- `out.js` is a JavaScript file produced by code generation from Haskell sources.
- `lib.js` is a JavaScript file produced by concatenation of `.js` source files (e.g. via Cabal's `js-sources`).
- `rts.js` is a JavaScript file produced by GHC. It contains the _generated part_ of the JavaScript backend RTS (see `GHC.StgToJS.Rts.Rts` module). The rest of the RTS comes from `js-sources` of the `rts` package, hence is included in `lib.js`
- `runmain.js` is a file with a single invocation to call the programs `main` function.
- `all.js` is a JavaScript file produced by concatenation of `lib.js`, `out.js`, `rts.js`, and `runmain.js`.
- `index.html` is a small html doc that wraps `all.js` in a `<script>` tag

Other files:

- `all.js.externs` are exported references from the JS backend RTS.
- `out.frefs.js` and `out.frefs.json` are used for foreign references. `out.frefs.js` contains only a single unused function `h$checkForeignRefs` and `out.frefs.json` is empty. These files will be used once the FFI is implemented.
- `out.stats` provides some metadata on the number of modules used in the program and their size.


## Running the test suite

You can run the test suite like so:

```bash
> hadrian/build --bignum=native --docs=none test
```

## Building Issues and how to fix them

### Configure fails because the "js-unknown-ghcjs" target isn't recognised

The error looks like:

```
> emconfigure ./configure --target=js-unknown-ghcjs
...
checking target system type... Invalid configuration `js-unknown-ghcjs': machine `js-unknown' not recognized
configure: error: /bin/sh ./config.sub js-unknown-ghcjs failed
emconfigure: error: './configure --target=js-unknown-ghcjs' failed (returned 1)
```

The issue is that you're using the old target triple `js-unknown-ghcjs` while you should now use `javascript-unknown-ghcjs`:

```bash
> emconfigure ./configure --target=javascript-unknown-ghcjs
```

See #22740 for the reason of the change.


### Configure fails with: sub-word sized atomic operations not available

The error looks like this:

```bash
> ./boot && ./configure --target=javascript-unknown-ghcjs
...
checking version of gcc... checking version of gcc... 15.0.0
15.0.0
checking whether CC supports -no-pie... no
checking whether C compiler supports __atomic_ builtins... yes
checking whether -latomic is needed for sub-word-sized atomic operations... failed
configure: error: sub-word-sized atomic operations are not available.
```

If you got this error you are most likely using `nix` and `ghc.nix`. The error is caused by `emscripten` trying to write to the `emscripten_cache`, but it cannot write because the `nix store` is read-only.

The fix is hacky, we need to provide the `emscripten_cache` to emscripten so that it is mutable. Similarly, setting `EM_CACHE` to the cache in the nix store does not work. The workaround, in nix, is to copy the cache, make it writable and export `EM_CACHE` to point to the cache. This is done in the following lines in `ghc.nix`:

```
# we have to copy from the nixpkgs because we cannot unlink the symlink and copy
# similarly exporting the cache to the nix store fails during configuration
# for some reason
cp -Lr ${emscripten}/share/emscripten/cache .emscripten_cache
chmod u+rwX -R .emscripten_cache
export EM_CACHE=.emscripten_cache
```

See [this](https://discourse.nixos.org/t/emscripten-tries-to-write-to-nix/15263) post for more.


### JS Backend fails during stage1 with error: symbol memset missing .functype

The error looks like:

```
/run/user/1729/ghc3035129_0/ghc_1.s:528:2: error:
     error: symbol memset missing .functype
            call    memset
            ^
    |
528 |         call    ...
```

This error is a bug in `llvm` which `emscripten` uses to generate JS through wasm (via `wasm2js`). The fix is to use a more recent version of `llvm` or a patched version, and a more recent version of `emscripten` (at least `3.1.x`). Whichever you choose the llvm you use must include [this](https://github.com/llvm/llvm-project/commit/2368f18eb305ae9d5a4f2110c048e5daf5007992) patch, and that llvm must be the llvm that `emscripten` uses. See also [this](https://github.com/llvm/llvm-project/issues/53712) issue. Thus far this has only been problematic for nix users. If that is you please see [this](https://github.com/NixOS/nixpkgs/pull/182840) PR on nixpkgs. Other distros, such as Ubuntu have worked out of the box due to a patched llvm included in their package manager.

### JS Backend fails with RangeError: Offset is outside the bounds of the DataView

The complete error looks like:

```
uncaught exception in Haskell main thread: RangeError: Offset is outside the bounds of the DataView
RangeError: Offset is outside the bounds of the DataView
    at DataView.getInt16 (<anonymous>)
    at h$$fa752042 (/tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:501742:92)
    at h$runThreadSlice (/tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:14948:11)
    at h$runThreadSliceCatch (/tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:14922:12)
    at h$mainLoop (/tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:14917:9)
    at /tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:15378:13
    at h$handleErrnoC (/tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:8704:9)
    at /tmp/ghctest-1_b3dywb/test   spaces/testsuite/tests/parser/should_compile/T23315/T23315.run/Setup:8427:13
    at FSReqCallback.oncomplete (node:fs:200:23)
```

This error is actually a bug in alex. See [this GHC issue](https://gitlab.haskell.org/ghc/ghc/-/issues/22356), and [this cabal pull request](https://github.com/haskell/cabal/pull/8896/). 

The failure mode is this with alex < 3.2.7 the lexer will generate incorrect static tables and we'll have a broken parser as a result. This happens on every backend but the value is unused and so its not a problem. However, because of how the JS backend implements `Addr#` we end up catching this bug.

The fix is to upgrade to alex >= 3.2.7. This bug should be relatively rare because it requires that you're building the JS backend with your own manually built cabal.
