Current failures in the testsuite for the JS backend.

2023-09-04 - Running the testsuite with `js_broken` predicate set to `normal` and a `perf` build returns:

1 unexpected pass:
- length001

10 framework failures:
- ImpSafeOnly01-10

178 unexpected failures


---


| Test                                  | Ticket  | Notes                                                                                     |
|---------------------------------------|---------|-------------------------------------------------------------------------------------------|
| rdynamic                              | #22374   | -rdynamic flag                                                                           |
| T12059                                | skipped | ByteArrays are always pinned with the JS backend                                          |
| T13894                                | skipped | ByteArrays are always pinned with the JS backend                                          |
| cloneMyStack2                         | #22261   | cloneMyStack#                                                                             |
| decodeMyStack                         | #22261   | cloneMyStack#                                                                             |
| decodeMyStack_underflowFrames         | #22261   | cloneMyStack#                                                                             |
| decodeMyStack_emptyListForMissingFlag | #22261   | cloneMyStack#                                                                             |
| compact_*                             | skipped | compact                                                                                   |
| T16916                                | #22261   | event manager, rtsopts                                                                    |
| T4850                                 | #22261   | FFI dynamic convention                                                                    |
| ffi007                                | skipped | foreign "dynamic" call                                                                    |
| conc021                               | skipped | foreign exports                                                                           |
| conc039                               | skipped | foreign exports                                                                           |
| conc040                               | skipped | foreign exports                                                                           |
| ffi001                                | skipped | foreign exports                                                                           |
| ffi002                                | skipped | foreign exports                                                                           |
| ffi016                                | skipped | foreign exports                                                                           |
| tests in ghc-heao                     | skipped | ghc-heap                                                                                  |
| T3333                                 | #22359   | ghci                                                                                      |
| caf_crash                             | #22359   | ghci script                                                                               |
| T10955dyn                             | #22351   | ghci, dynamic linking                                                                     |
| T15729                                | #22359   | ghci, require C                                                                           |
| T1407                                 | #22359   | ghci, require C                                                                           |
| T12023                                | #22359   | ghci?                                                                                     |
| fat005                                | #2261    | grep for Proto-BCOs                                                                       |
| T13604a                               | #22261   | HPC                                                                                       |
| tests in hpc                          | skipped | hpc                                                                                       |
| traceEvent                            | skipped | JS backend has no payload size limit for trace events                                     |
| conc071                               | skipped | JS RTS doesn't report the same cap/locked status                                          |
| T2615                                 | skipped | linker scripts                                                                            |
| conc012                               | skipped | no stack overflow detection, rts opts                                                     |
| T3924                                 | #22261   | perf results                                                                              |
| T16473                                | #22261   | perf results                                                                              |
| InfiniteListFusion                    | #22576   | perf results                                                                              |
| T11462                                | #22261   | plugin                                                                                    |
| T11525                                | #22261   | plugin                                                                                    |
| TcPlugin_*                            | #22261   | plugins                                                                                   |
| T4201                                 | skipped | prints NCG info (LFI, etc.). Perhaps reenablable now that we generate some of these infos |
| recomp015                             | skipped | require asm support                                                                       |
| process11                             | #22349   | require C                                                                                 |
| T14900                                | skipped | require compact                                                                           |
| linker_error*                         | skipped | require dynamic_linking                                                                   |
| T1791                                 | skipped | require heap overflow detection                                                           |
| outofheap_*                           | skipped | require heap overflow detection                                                           |
| outofmem                              | skipped | require heap overflow detection                                                           |
| outofmem2                             | skipped | require heap overflow detection                                                           |
| overflow1, 2, 3                       | skipped | require heap overflow detection                                                           |
| InternalCounters                      | skipped | require internal counters                                                                 |
| perf tests                            | skipped | require perf results                                                                      |
| T8766                                 | #22261   | require perf results                                                                      |
| T9848                                 | #22261   | require perf results                                                                      |
| T13191                                | #22261   | require perf results                                                                      |
| T3474                                 | #22374   | require perf results                                                                      |
| T17499                                | #22261   | require perf results                                                                      |
| T10590                                | skipped | require pipe support                                                                      |
| profiling tests                       | skipped | require profiling support                                                                 |
| allocLimit*                           | skipped | require setThreadAllocationCounter                                                        |
| stackoverflow_*                       | skipped | require stack overflow detection                                                          |
| T15261a                               | #22370   | rts                                                                                       |
| T15261b                               | #22370   | rts                                                                                       |
| T9839_01                              | #22261   | rts opts                                                                                  |
| T19481                                | #22374   | rts opts                                                                                  |
| T19381                                | #22374   | rts opts                                                                                  |
| T17720a                               | #22261   | rts opts                                                                                  |
| T17720b                               | #22261   | rts opts                                                                                  |
| T17720c                               | #22261   | rts opts                                                                                  |
| rtsopts001                            | skipped | rts opts                                                                                  |
| T17574                                | skipped | rts opts                                                                                  |
| rts/flags tests                       | skipped | rts opts                                                                                  |
| T18642                                | #22374   | rts opts                                                                                  |
| rtsopts001                            | skipped | rtsopts                                                                                   |
| rtsopts002                            | #22370   | rtsopts                                                                                   |
| WithRtsOpts                           | #22370   | rtsopts                                                                                   |
| rtsflags002                           | #22261   | rtsopts                                                                                   |
| alloccounter1                         | #22261   | rtsopts, counters                                                                         |
| js-callback04 and 05                  | skipped | sync + io bug. Should be "expect broken"                                                  |
| rtsflags001                           | skipped | test out of range heap size, rts opts                                                     |
| T22795a, b, c                         | skipped | test threaded RTS (perhaps req_target_smp?)                                               |
| DocsInHiFileTH                        | #22261   | TH?                                                                                       |
| TD_TH_splice                          | #22576   | TH?                                                                                       |
| T16135                                | #22576   |                                                                                           |
| T17648                                | #22370   |                                                                                           |
| T13825-unit                           | #22362   |                                                                                           |
| mask002                               | #22261   |                                                                                           |
| T14452                                | #22261   |                                                                                           |
| T17481                                | #22261   |                                                                                           |
| T21869                                | #22261   |                                                                                           |
| T22405                                | #22576   |                                                                                           |
| T22405b                               | #22576   |                                                                                           |
| recomp011                             | #22261   |                                                                                           |
| ffi008                                | #22363   |                                                                                           |
| ffi020                                | #22363   |                                                                                           |
| rn.prog006                            | #22261   |                                                                                           |
| RnPatternSynonymFail                  | #22261   |                                                                                           |
| T8308                                 | #22261   |                                                                                           |
| stack004                              | #22374   |                                                                                           |
| exec_signals                          | #22355   |                                                                                           |
| GcStaticPointers                      | #22261   |                                                                                           |
| T9405                                 | #22261   |                                                                                           |
| T14695                                | #22359   |                                                                                           |
| typecheck.testeq1                     | #22355   |                                                                                           |
| unpack_sums_6                         | #22374   |                                                                                           |
| conc016                               | skipped |                                                                                           |
| conc015                               | fragile |                                                                                           |
| conc034                               | skipped |                                                                                           |
| T22669                                | skipped |                                                                                           |
| T11579                                | skipped |                                                                                           |
| haddock_parser_perf                   | skipped |                                                                                           |
| haddock_renamer_perf                  | skipped |                                                                                           |
| T7014                                 | skipped |                                                                                           |
| T8832                                 | skipped |                                                                                           |
| T13340                                | skipped |                                                                                           |
| T15953                                | skipped |                                                                                           |
| T21336a                               | #22261   |                                                                                           |
| T21336c                               | #22370   |                                                                                           |
| hClose                                | #22261   |                                                                                           |
| hSetBuffering003                      | #22261   |                                                                                           |
| openFile005                           | #22261   |                                                                                           |
| openFile007                           | #22261   |                                                                                           |
| readFile001                           | #22261   |                                                                                           |
| countReaders001                       | #22261   |                                                                                           |
| Timeout001                            | #22261   |                                                                                           |

## Print issue (newline?)

- T13914:

```diff
--- "/tmp/ghctest-oeqsldey/test   spaces/testsuite/tests/driver/T13914/T13914.run/T13914.stdout.normalised"	2023-09-04 12:12:15.922599412 +0200
+++ "/tmp/ghctest-oeqsldey/test   spaces/testsuite/tests/driver/T13914/T13914.run/T13914.run.stdout.normalised"	2023-09-04 12:12:15.922599412 +0200
@@ -3,8 +3,7 @@
 [2 of 2] Linking main
 main: Assertion failed
 CallStack (from HasCallStack):
-  assert, called at main.hs:<line>:<column> in <package-id>:Main
-With -fignore-asserts
+  assert, called at main.hs:<line>:<column> in <package-id>:MainWith -fignore-asserts
```

## Process' T4198

Replace `js_broken` with `req_c, extra_files['exitminus1.c']` (and remove `extra_src_files` variable in `testsuite/driver/testlib.py`..).

## Foreign import wrapper/dynamic

Error:
- TypeError: f.apply is not a function

Affected tests:
- ffi006
- ffi011
- ffi019
- ffi021
- T1679
- T2469
- T4038

## ReferenceError: h$baseZCGHCziConcziSynczireportError is not defined

- T1959
- MergeObjsMode
- recomp008

## ReferenceError: h$getNumberOfProcessors is not defined

- jspace (and performs explicit heap census...)

## ReferenceError: h$printf is not defined

- T4012

## ReferenceError: h$qsort is not defined

- fed001
- ffi013

## foo.o: file not recognized: file format not recognized

- T12733
- cabal03
- cabal04
- cabal05
- cabal06
- cabal08
- cabal10
- T18567
- t19518
- T22333
- T1372
- different-db
- mhu-closure
- package-imports-20779
- recomp007
- recompChangedPackage
- recompTHpackage
- safePkg01
- ImpSafeOnly01
- ImpSafeOnly02
- ImpSafeOnly03
- ImpSafeOnly04
- ImpSafeOnly05
- ImpSafeOnly06
- ImpSafeOnly07
- ImpSafeOnly08
- ImpSafeOnly09
- ImpSafeOnly10
- T13168
- bug1465
- T3007
- T16219
- bkpcabal01
- bkpcabal06
- bkpcabal07
- T13350

- T11155: explicitly call `nm` on the object file! Need new `req_nm` predicate maybe.


## Require C support

Should have `req_c` instead of `js_broken`

- process011

## ReferenceError: h$o is not defined

```
--- /dev/null	2023-09-04 10:12:41.382401300 +0200
+++ "/tmp/ghctest-vwlirkez/test   spaces/libraries/process/tests/process007.run/process007.run.stderr.normalised"	2023-09-04 14:31:47.008555549 +0200
@@ -0,0 +1,14 @@
+/tmp/ghctest-vwlirkez/test   spaces/libraries/process/tests/process007.run/process007_fd:3374
+h$o(h$blackhole,5,0,2,0,null);
+^
+
+ReferenceError: h$o is not defined
+    at Object.<anonymous> (/tmp/ghctest-vwlirkez/test   spaces/libraries/process/tests/process007.run/process007_fd:3374:1)
+    at Module._compile (node:internal/modules/cjs/loader:1233:14)
+    at Module._extensions..js (node:internal/modules/cjs/loader:1287:10)
+    at Module.load (node:internal/modules/cjs/loader:1091:32)
+    at Module._load (node:internal/modules/cjs/loader:938:12)
+    at FunctioncuteUserEntryPoint [as runMain] (node:internal/modules/run_main:83:12)
+    at node:internal/main/run_main_module:23:47
```

- process007

## Eventlog support

- traceBinaryEvent
- EventlogOutput1
- EventlogOutput2

## runtimeRepPrimRep / kindPrimRep

Error:
```
javascript-unknown-ghcjs-ghc: panic! (the 'impossible' happened)
  GHC version 9.9.20230901:
	runtimeRepPrimRep
  typePrimRep (Unboxed Int :: TYPE (Rep Int))
  Rep Int
  Call stack:
      CallStack (from HasCallStack):
        callStackDoc, called at compiler/GHC/Utils/Panic.hs:191:37 in ghc:GHC.Utils.Panic
        pprPanic, called at compiler/GHC/Types/RepType.hs:620:5 in ghc:GHC.Types.RepType
  CallStack (from HasCallStack):
    panic, called at compiler/GHC/Utils/Error.hs:512:29 in ghc:GHC.Utils.Error
```

Affected tests:
- T13244
- T13105

Error:
```
javascript-unknown-ghcjs-ghc: panic! (the 'impossible' happened)
  GHC version 9.9.20230901:
	kindPrimRep
  Star2 Constraint
  typePrimRep (a_ayk :: Star2 Constraint)
  Call stack:
      CallStack (from HasCallStack):
        callStackDoc, called at compiler/GHC/Utils/Panic.hs:191:37 in ghc:GHC.Utils.Panic
        pprPanic, called at compiler/GHC/Types/RepType.hs:593:5 in ghc:GHC.Types.RepType
  CallStack (from HasCallStack):
    panic, called at compiler/GHC/Utils/Error.hs:512:29 in ghc:GHC.Utils.Error
```

Affected tests:
- Dep3

## STM

- stm055: issue with detection or reporting of nested transactions

## Exception during weak pointer finalization (ignored): <stdout>: hFlush: resource exhausted (unknown error)

- T21336b


## ReferenceError: h$pipe is not defined

- apirecomp001
- TargetContents
- T12903
- T12852
- hGetBuf001


## ByteString C sources

Errors:
- ReferenceError: h$_hs_bytestring_uint_hex is not defined
- ReferenceError: h$fps_reverse is not defined
- ReferenceError: h$_hs_bytestring_long_long_uint_hex is not defined

Affected tests:

- literals
- parsed
- T6145
- cabal01
- annrun01

## ReferenceError: h$primop_unpackClosurezh is not defined

- T19156
- T4891

## JS RTS doesn't generate .stats file

- T10678

## hsc2hs returns suboptimal alignment

- T10272: returns alignment 8 while 4 should be the max on 32-bit arch even with 64-bit fields

## getDeps: Couldn't find home-module

- RepPolyBackpack5: `getDeps: Couldn't find home-module: SomeNumber`

## createProcess: posix_spawnp: permission denied

- hsc2hs001
- hsc2hs002
- hsc2hs003
- T3837
- T4340
- T11004
- T12504
- T15758

## executablePath config.os

- executablePath: probably wrong config.os settings. Grep queryHostTarget in Hadrian's RunTest module...

## Require hp2ps

- T15904

## Require IOPort support

- UnliftedIOPort

## ReferenceError: h$hs_try_putmvar is not defined

- T15427

## ReferenceError: h$waidRead is not defined

- T7773

## ReferenceError: h$kill is not defined

- topHandler01

## EBADF vs invalid argument

Error:
```
-Left hClose002.tmp: hClose: invalid argument (Bad file descriptor)
+Left hClose002.tmp: hClose: invalid argument (Invalid argument)
```

Affected test:
- hClose002

## ReferenceError: h$fdReady is not defined

- hWaitForInput-accurate-stdin
- hReady001
- hReady002
- readWrite002

## T13525: JavaScript exception: h$base_mkfifo

- T13525

## ReferenceError: h$EINVAL is not defined

- encoding004

## ReferenceError: h$socket is not defined

- T12010

## Finalizers

- T7160: finalizers not run

## Delimited continuations

- cont_simple_shift
- cont_exn_masking
- cont_missing_prompt_err
- cont_nondet_handler
- cont_stack_overflow

## Wrong signal exit code

- topHandler02: expects 130 for UserInterrupt/sigINT+128 but gets 252
- topHandler03: expects 143 for sigTERM+128 but gets 241