This page combines with the [jsbits tracking ticket](https://gitlab.haskell.org/ghc/ghc/-/issues/24015) to track the quality of support for the JavaScript backend in various libraries, including:
- The status of all boot libraries
- Libraries from the GHCJS ecosystem (`ghcjs-base`, `ghcjs-dom`, `jsaddle`)
- Popular libraries that use the C FFI and need alternate implementations
- Bugs when using libraries with the JS backend
- Workarounds to these bugs
- Usage advice for certain libraries (including JS backend-specific performance warnings)

# Build Tools and Installation

## GHCup

Installation of the JavaScript backend is available in newer versions of GHCup. The install command will require the use of `emconfigure`, and therefore the installation of the Emscripten toolchain.

See: https://github.com/haskell/ghcup-hs/pull/844

# Boot libraries

Many boot libraries are pure-Haskell by default:
* Cabal
* containers
* deepseq
* exceptions
* parsec
* pretty
* semaphore-compat
* sum
* transformers
* xhtml

## `text`

There are many C FFI functions that need to be matched in JavaScript to fully support `text`. One such function, `_hs_text_measure_off`, is used by a dependency of the `ghcjs-base` testsuite.

A pure-Haskell implementation is in progress at https://github.com/haskell/text/pull/536.

# GHCJS Libraries

## `ghcjs-base`

Work is being done to ensure that `ghcjs-base` is updated for the JavaScript backend and that its testsuite is able to complete successfully. 

## `ghcjs-dom`

Blocked by `ghcjs-base` and `jsaddle`.

## `jsaddle`

Requires minor changes to the cabal file to update GHCJS conditions to also include the JavaScript backend.

# Other Libraries

## `splitmix`

This library has a GHCJS FFI import to replace one C function used. 

An MR exists to update this to include the JavaScript backend, which is blocked on CI tooling.

See: https://github.com/haskellari/splitmix/pull/75