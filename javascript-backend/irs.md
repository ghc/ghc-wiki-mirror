Currently the JS backend uses a single IR inherited from [JMacro](https://hackage.haskell.org/package/jmacro) and stored in [GHC.JS.Syntax](https://gitlab.haskell.org/ghc/ghc/-/blob/3e09cf82ad111e0a6feed81b726849ceaaf3c805/compiler/GHC/JS/Syntax.hs).

GHCJS used to provide an optimizer for this IR, but it was slow especially because it had to reconstruct some information from the IR. E.g. look at the name of a function in a function call to check if it's one of the known stack operations: we can only do some good variable propagation if we have this kind of information about the STG machine.

We propose to fix this by introducing more IRs. From the bottom up:

- JS Link IR: stored in object files so that we can perform link-time optimizations
  - currently we store the JS IR in object files and pretty-print it at link time after LTOs (global renaming)
  - we could pretty-print most of the JS IR before storing it into object files, only keeping some names to pretty-print at link time.
- JS IR: close to JavaScript grammar so that:
  - we can generate any JS code we want into it
  - we can parse any JS code into it (for inlined foreign imports)
  - we can provide an optimizer for it and a minimizer ("uglifier")
- Imperative STG IR: intermediate between STG and JS
  - has STG operations as primitives (heap management, stack management, scheduling...)
  - basic imperative constucts: functions, variables, constants, static data...
  - ideally most optimizations should be done here (constant and variable propagation, CSE, dead code removal, loopification...)
- EDSL(s): typed EDSL(s) to make the creation of IR ASTs safer.

