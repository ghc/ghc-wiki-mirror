GHC is a changing system so there are sure to be bugs in it.

To report a bug:

 - Register an account on this GitLab instance.
 - **Ask for your account to be approved**. This is an unfortunate workaround for all the spam that the service attracts.
     - Contact admins through [the mailing list or IRC](https://gitlab.haskell.org/ghc/ghc/-/wikis/mailing-lists-and-irc).
 - Create a [new bug](https://gitlab.haskell.org/ghc/ghc/issues), and enter your bug report. Make a new ticket for your bug unless you are 100% sure there is a ticket for your exact problem.

If you believe that your bug may have security implications please refer to GHC's [Security Disclosure Policy](https://www.haskell.org/ghc/disclosures.html).

For more information about how GHC's developers handle bugs, see [Issue conventions](https://gitlab.haskell.org/ghc/ghc/wikis/gitlab/issues).


## 1. How do I tell if I should report my bug?

- **Is it a bug at all?**  Take a look at ["What to do when something goes wrong"](http://www.haskell.org/ghc/docs/latest/html/users_guide/gone_wrong.html) from the GHC User's Guide, which will give you some guidance as to whether the behaviour you're seeing is really a bug or not.

- **Duplicate bug reports**.  Please search for existing tickets on the [bug tracker](https://gitlab.haskell.org/ghc/ghc/issues).  If your problem has already been reported, it saves time to have all the manifestations of the same bug gathered together.  If you get an error message from GHC, a good search key is usually the non-program-specific part of the error message.

  If you do find an existing ticket that seems to describe the same problem, then

  - Add a comment that explains how it manifests for you, and add your description of how to reproduce it (see below)
  - Subscribe to notifications for the ticket. We will try to prioritise bugs that affect a lot of people. 

- **RTS bugs**.  However, if you encounter a crash from the runtime system, then don't bother searching for existing tickets - just create a new ticket.  These indicate a general RTS failure of some kind, and can arise due to a wide range of causes, so it is easier for us to track each failure in a separate ticket.  

  >
  >
  > Runtime system errors usually manifest as one of the following error messages:   
  >
  >
  > ```wiki
  > internal error: evacuate: strange closure type ...
  > internal error: scavenge: unimplemented/strange closure type ...
  > internal error: update_fwd: unknown/strange object ...
  > internal error: stg_ap_v_ret
  > ```

- **If in doubt, just report your bug**.


## 2. What to put in a bug report
 
The name of the bug-reporting game is: facts, facts, facts. Don't omit them because "Oh, they won't be interested…".

Here is a check-list of things to cover in your description:

1. The source code of the program that shows the bug.  You can give the code inline, or attach a file, or attach a tarball.
1. What is the program behaviour that is wrong, in your opinion?
1. (Only if you think the bug is platform dependent): What kind of machine are you running on, and exactly what version of the operating system are you using? On a Unix-like system, `uname -a` and `cat /etc/motd` will show the desired information. In the bug tracker, this information can be given in the "Architecture" and "Operating system" fields.
1. What version of your C compiler are you using? `cc -v` will tell you.
1. Run the sequence of compiles/runs that caused the offending behaviour, cut-and-paste the whole session into the bug report. We'd prefer to see the whole thing.
1. Add the `-v` flag when running GHC, so we can see exactly what was run, what versions of things you have, etc. When using `cabal-install` this can be done using the `--ghc-options=-v` flag or by adding a similar field to your `cabal.project` file.
1. Add the `-dcore-lint` flag when running GHC.  This adds some significant internal consistency-checking, which often nails bugs early.


## 3. The reproduction case

The absolutely key thing is that we must be able to reproduce the bug, the **repro case**.  Without this, we are largely helpless; we know there's a problem but often we can make no progress with fixing it.  

Largely helpless is not the same as completely helpless.  Sometimes the bug manifests in some big code base, and/or in a code base that you can't share.  *It is still better to report the bug*, even if you can't offer a way to reproduce it.  It may form part of a pattern with other bug reports, or someone may come up with good ideas for how you could narrow it down.  

But we *really appreciate* effort to produce a repro case if you can:

- **As small as possible**.  It costs you real work to "boil down" the bug from a big program to a small one, but the plain truth is that the easier the bug is to reproduce, and the smaller the test program (= smaller debug output), the less time it costs us to fix it. Also, as you are familiar with the code, it is generally easier for you to boil it down than for us to. Please note that instances of certain classes, whether hand-written or derived, can produce a substantial amount of code. In particular, if you can demonstrate the bug without writing or deriving instances of `Show`, `Read`, `Generic`, or `Data`, that is generally preferable. To demonstrate evaluation, prefer `deepseq` to `show`, and write any necessary `NFData` instances by hand.

- **Minimise dependencies**.  If we have to run `cabal install`, we can't put it in the testsuite *as is*. Dependencies also make it substantially harder to determine exactly where a bug was introduced, as it may be difficult or impossible to compile those dependencies at an arbitrary GHC commit. 

  One way to cut down programs and minimise dependencies is to replace library functions with definitions like
  ```haskell
  displayWidget :: This -> IO That
  displayWidget = undefined
  ```
  and thereby avoid the necessity for the supporting library.

- **Make your setup as reproducible as possible**.  Notably:
  * For reproducers which are larger than a few modules in size, it is appreciated if you provide the source in the form of a git repository (or make a branch if the issue is observed in an existing project's repository). This makes it easy for GHC's developers to be certain they have faithfully reproduced your issue.
  * If you don't have time to eliminate all dependencies, try to make them reproducible.  For example
    * Include version numbers or git commit hashes for each remaining dependency (e.g. via `cabal freeze`)
    * Use a tool like `nix` or `docker` to provide a frozen environment in which the bug can be investigated


  
Lacking this precision makes it difficult to reproduce the problem; we will have to try to guess likely version numbers or commits based on ticket entry dates, which is both painful and unreliable, especially since library maintainers will often add workarounds for the very issues reported here.

If you are a hero and manage to track down the problem, please [send us patches](working-conventions/fixing-bugs).