This is a description of some of the coding practices and style that we use for Haskell code inside `compiler`.  For run-time system code see the [Coding Style Guidelines for RTS C code](commentary/rts/conventions).  Also see the wiki page on [Contributing](contributing) for issues related to version control, workflow, testing, bug tracking and other miscellany.

**Table of contents**

[TOC]

## 1. Programming Style

The general rule is to stick to the same coding style as is already used in the file you're editing. If you must make stylistic changes, commit them separately from functional changes, so that someone looking back through the change logs can easily distinguish them.

### 1.1 Naming things

* Lexical conventions
  * For exported functions, always use CamelCase e.g. `exprType`
  * For local bindings in let or where clauses, lambdas, case expression, use snake_case  e.g. `expr_type`.
  * For top-level binding that are not exported, use your judgement.  The more localised the definition is, the more you should use snake_case.

* Naming conventions for functions.  If a function has type `This -> That`, call it `thisThat`.  Not `thisToThat` nor `thatOfThis`.  Example `exprType :: Expr -> Type`.

* For naming modules in the basic libraries `base`, `ghc-internal`, `ghc-experimental`, `ghc-prim`, and `ghc-bignum`, see [here](https://github.com/haskellfoundation/tech-proposals/blob/main/proposals/0000-ghc-module-naming.rst). In particular:
  * Modules in `base`, `ghc-experimental`, `ghc-prim`, `ghc-internal` etc should all have distinct names.
  * New modules in GHC's internal libraries (`ghc-prim`, `ghc-internal` etc) should be of form `GHC.Internal*`.
  * Modules in `ghc-experimental` should be of form `*.Experimental`.
  * Modules in `base` should not have either of these forms.


### 1.2 Prefer lucidity over brevity

**It's much better to write code that is transparent than to write code that is short.**

For example, compare
```haskell
f1, f2 :: TcM a -> TcM (a, [b])
f1 thing_inside = flip (,) [] <$> thing_inside

f2 thing_inside = do { res <- thing_inside; return (res, []) }
```
`f1` is terse, but `f2` is readable. Prefer `f2` every time.

### 1.3 Type classes

As a rule of thumb, when writing monomorphic code, prefer using a monomorphic function over a typeclass-overloaded one.
  - A monomorphic function communicates information to the programmer, e.g. when reading `map f wibbles` one learns that `wibbles` is a list, as opposed to reading `fmap f wibbles` where one needs to run a mental typechecker to figure out the type of `wibbles` and then infer that `fmap` uses the instance for lists.
  - Monomorphic functions lead to easier-to-understand type errors.
  - It is easier to refactor code using monomorphic functions, as one can grep/search for a certain function name.  Searching for `mapBag` is much, much more helpful than searching for `fmap`.
  - Overloaded functions rely on typeclass specialisation to be performant, which isn't always reliable and might require additional complexity, in the form of manual `SPECIALISE` annotations and `RULES` pragmas.
  - Using an overloaded function may allow the program to continue to compile after a refactor that changes the type of the argument -- but the new behaviour may be flat-out wrong.  As [Kerckhove puts it: type-class polymorphism erodes type safety](https://cs-syd.eu/posts/2023-08-25-ad-hoc-polymorphism-erodes-type-safety). 

Even when one has genuinely polymorphic code, it can be better to write out the code longhand than to reuse a generic abstraction (not always, of course). Sometimes it's better to duplicate some similar code than to try to construct an elaborate generalisation with only two instances.  Remember: other people have to be able to quickly understand what you've done, and overuse of abstractions just serves to obscure the *really* tricky stuff, and there's no shortage of that in GHC.

Here's a real-life example (!9037).  Here are two functions that do the same thing:
```haskell
f1, f2, f3 :: [a] -> [a] -> [[a]]
f1 as    = sequenceA $ (:) <$> as
f2 as bs = [a:bs | a <- as]
f3 as bs = map (\a -> a:bs) as
```
For GHC we much prefer `f2` or `f3`.  The former uses a list comprehension; the latter uses (list) map. Both are easy to grok.

Understanding `f1` is quite a bit more demanding. To be clear, **there is nothing wrong with `f1`** for people who have `sequenceA` and friends paged into their mental cache.  But for those who don't, here's the journey of at least one reader.

Let's start with type checking.  For `f1` we must look up the type of `sequenceA`:
```haskell
sequenceA :: forall t f b. (Traversable t, Applicative f) => t (f b) -> f (t b)
```
Now we must figure out how `t` and `f` are instantiated.  Let's figure out the type of `(:) <$> as`.  Well, `as :: [a]`,  so `<$> :: Functor g => (p->q) -> g p -> g q` is being used with `g=[]` and `q = [a] -> [a]`.  So we have
```haskell
  (:) <$> as  ::  [ [a] -> [a] ]
```
Now that type must match the argument of `sequenceA`, which has type `t (f b)`,
so we must have `t = []` and `f = (->) [a]` and `b = [a]`.

Right, so the result of `sequenceA` has type `f (t b)` which is `[a] -> [[a]]`.  That matches the type signature for `f1`, so the program is type correct.

Now we need to work out what `sequenceA @[] @((->) a)` actually *does*.  It's a method of the `Traversable` class, so we need the `Traversable []` instance.  Instances aren't all that easy to find (you can't grep for them the way you can grep for a function name), but it's in `Data.Traversable`:
```haskell
instance Traversable [] where
    traverse f = List.foldr cons_f (pure [])
      where cons_f x ys = liftA2 (:) (f x) ys
```
Oh. No actual definition of `sequenceA`.  Let's look at the default method:
```haskell
class (Functor t, Foldable t) => Traversable t where
    sequenceA :: Applicative f => t (f a) -> f (t a)
    sequenceA = traverse id
```
OK so in our call of `sequenceA` it really means
```haskell
   sequenceA @[] @((->) a)
   = traverse @[] @((->) a) id
   = List.foldr cons_f (pure [])
      where cons_f x ys = liftA2 (:) (f x) ys
```
Now `liftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c`, so again we have to
work out what `f` is, and thus what code is executed by that `liftA2`.

None of this is easy.  We prefer `f2` or `f3`.  **General rule: use monomorphic functions
if you can**.

Another simpler example.  Suppse `f :: t2 -> t3` and `xs :: [(t1,t2)]` Instead of
```haskell
fmap f <$> xs   -- Two uses of fmap, in different notation, one 
                -- of which is over a partial application of (,)
```
write
```
mapSnd f xs
```
where `mapSnd :: (b->c) -> [(a,b)] -> [(a,c)]` is defined in `GHC.Utils.Misc`.   Actually, even there `mapSnd` generalises over the list part:
```haskell
mapSnd :: Functor f => (b->c) -> f (a,b) -> f (a,c)
```

### 1.4 INLINE and SPECIALISE pragmas

If inlining or specialisation is important for performance, use pragmas to say so.  Consider
```haskell
f = ...big....

g :: Num a => Int -> a
g x = if (f x) then 1 else 2
```
It is tempting to think that "`g` is small, so GHC's inlining heuristics will inline `g` at every call site, so there will be no type-class overhead".  But this reasoning is fragile.  For example, if this is the only call to `f`, GHC will inline it to give
```haskell
g :: Num a => Int -> a
g x = if (...big...) then 1 else 2
```
and now `g` won't be inlined.  Conclusion:
* if you are relying on a function to be inlined, use an INLINE pragma
* if you are relying on a function to be specialised, use an SPECIALISE pragma

Do not rely on heuristics.  And add a comment to explain the importance of the pragma.

### 1.5 Tabs vs Spaces

The Haskell source code in GHC is indented using spaces only. The usual indentation level is 2 spaces, but some modules follow a different convention, which you should try to stick to.

Other, non-Haskell-code is indented with a mixture of tabs and spaces, and is standardised on a tab-stop of 8.


## 2. Using Notes

One of the most serious difficulties in a large, long-lived project is keeping technical documentation up to date. We have no silver bullet, but we offer one low-tech mechanism that has served us particularly well: **Notes**.

### 2.1 Notes: the basic idea

When writing code, there is often a moment when a careful programmer will mentally say something like "This data type has an important invariant". She is faced with two choices, both unsatisfactory. She can add the invariant as a comment, but that can make the data type declaration too long, so that it is hard to see what the constructors are. Alternatively, she can document the invariant elsewhere, and risk it going out of date. Over twenty years, everything goes out of date!

Thus motivated, we use the following very simple convention:

* Comments of any significant size are not interleaved with code, but instead set off by themselves, with a heading in standard form, thus:
  ```
      Note [Equality-constrained types]
      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      The type   forall ab. (a ~ [b]) => blah
      is encoded like this:

         ForAllTy (a:*) $ ForAllTy (b:*) $
         FunTy (TyConApp (~) [a, [b]]) $
         blah
  ```

* At the point where the comment is relevant, we add a short comment referring to the Note:
  ```haskell
     data Type
       = FunTy Type Type -- See Note [Equality-constrained types]

       | ...
  ```
  The comment highlights that something interesting is going on, and gives a precise reference to the comment that explains. It sounds trivial, but the precision is vastly better than our previous habit of saying "see the comment above", because it often was not clear which of the many comments above was intended, and after a few years the comment was not even above (it was below, or gone altogether).

* Notes should be copiously illustrated with concrete examples, and should point specifically to tickets that give relevant background detail.  The extra space afforted by a Note (rather than an inline comment) makes it easy to to give such examples without breaking up the code it describes.

Not only is it possible to go from the code that refers to the Note to the Note itself, but the reverse is also possible, and that is often useful. Moreover, the same Note may be referred to from multiple points in the code.

This simple, ASCII-only technique, with no automated support, has transformed our lives: GHC's source code has thousands of Notes, and the number grows daily.
We have a lint check in CI to identify references to Notes that don't exist.

### 2.2 Overview Notes

Suppose you wonder "how is `unsafeCoerce` implemented in GHC?", or "how are
floating-point literals represented at compile time?" or "what does it mean
to say that two types are equal in GHC?".  There is often no one bit of code that will answer such questions. *But there can be one Note!"

We strongly encourage **overview Notes** that summarise how the compiler deals with a particular topic.

An overview Note should

* **Topic**.  Identify its topic/problem.

* **Design**.  Explain the overall design -- give the big picture.

* **Moving parts**.  Point to the "moving parts"; that is, the key bits of code that implement
  the feature.  These pointers should be quite specific, referring to
  functions and data types by name.  (Those functions and data types should
  in turn refer back to the overview Note.)

* **Wrinkles**.  List "wrinkles"; that is, any non-obvious corners that need special treatment.
  This overlaps with the previous bullet a bit; again we would typically point
  to the specific code that deals with the non-obvious corner.

  It is often helpful to give a name to each wrinkle, such as (W7); then you can say (elsewhere) `-- See wrinkle (W7) of Note [Wombat]`.  Precision is so helpful!

A good example of an overview Note is `Note [Implementing unsafeCoerce]` in the library `base:Unsafe.Coerce`.
Another is `Note [Quasi-quote overview]` in `Language.Haskell.Syntax.Expr`.

Many (not all) MRs would be strengthened by adding a suitable overview Note, that
summarises the overall design.  An overview makes the MR much easier to review.

### 2.3 Being specific, using examples

* Be specific.  Don't just say "we reject overlapping instances in situation X".  Instead point directly to the code that implements that decision.
* Offer concrete examples to illustrate the point.  It can be fantastically illuminating to see how some general point plays out in a particular example.
* Mention specific tickets and/or testsuite cases that further illustrate the issue.

Here's an example 
```
Note [Put touchable variables on the left]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ticket #10009, a very nasty example:

    f :: (UnF (F b) ~ b) => F b -> ()

    g :: forall a. (UnF (F a) ~ a) => a -> ()
    g _ = f (undefined :: F a)

For g we get [G]  g1 : UnF (F a) ~ a
             [W] w1 : UnF (F beta) ~ beta
             [W] w2 : F a ~ F beta

g1 is canonical (CEqCan). It is oriented as above because a is not touchable.
See canEqTyVarFunEq.
```
Notice the concrete example, the pointer to #10009, and the pointer to `canEqTyVarFunEq`.

Caution: it is easy for these code references to get out of date. But that's a lesser evil.

### 2.4 Notes describe the present state, not the journey

**A commit message describes a *change*; a comment or a Note describes a *state*.**.  That is, a Note or a comment should always describe the current state of affairs, not the journey that led to that state of affairs.
At the moment you write it, the journey is high in your consciousness; but in ten years time, it is at best a distraction, and at worst the Note becomes simply incomprehensible without knowledge of the
previous state of affairs.

* **Design choices**.  A Note can be a good place to document the reasoning that led to a particular decision.  Lacking the reasoning, someone might change the decision later, without realising the consequences.
* **Historical notes**.  Sometimes it can be helpful to have a clearly-signposted section called "Historical note: blah blah" that describes some previous state of affairs. But the "Historical note" signpost signals that you don't need to read it to understand the current compiler.


## 3. Warnings and assertions

### 3.1 Warning flags

We are aiming to make the GHC code warning-free, for all warnings turned on by

```wiki
-Wall
```

The build automatically sets these flags for all source files (see `mk/warnings.mk`).


The [validate script](testing-patches), which is used to test the build before commiting, additionally sets the `-Werror` flag, so that the code **must** be warning-free to pass validation. The `-Werror` flag is not set during normal builds, so warnings will be printed but won't halt the build.

Currently we are some way from our goal, so some modules have a

```haskell
{-# OPTIONS_GHC -fno-warn-... #-}
```

pragma; you are encouraged to remove this pragma and fix any warnings when working on a module.

### 3.2 Assertions

GHC is littered with assertion checks like
```
isCTupleTyConName n
 = assertPpr (isExternalName n) (ppr n) $
   getUnique n `elementOfUniqSet` cTupleTyConKeys
```
Whenever something should be the case (here that `n` is an `External` name), but is not enforced by the type system, use an `assert` to check it.
Not only does this add a runtime test (in a debug build of GHC), but it is also fantastic (and machine checked, albeit at runtime)
documentation about invariants.

These `assert` functions all test `GHC.Utils.Constants.debugIsOn :: Bool`, which is set True in debug builds, and False otherwise.  So if it is False the assert is a no-op and is removed at compile time.  You can also use use `if debugIsOn then ... else ...` to add code that will only run in a debug build.

Here `assertPpr` is defined in GHC.Utils.Panic, along wih a bunch of other such functions:
```
GHC.Utils.Panic:
    assertPpr  :: HasCallStack => Bool -> SDoc -> a -> a
    assertPprM :: (HasCallStack, Monad m) => m Bool -> SDoc -> m ()

GHC.Utils.Panic.Plain:
    assert  :: HasCallStack => Bool -> a -> a
    assertM :: (HasCallStack, Monad m) => m Bool -> m ()
```
The `Ppr` versions take a `SDoc`; the plain versions reply on the `HasCallStack` to report the site of the failure.


## 4. Exports and Imports

### Exports

```haskell
module Foo (
   T(..),
   foo,	     -- :: T -> T
 ) where
```

We usually (99% of the time) include an export list. The only exceptions are perhaps where the export list would list absolutely everything in the module, and even then sometimes we do it anyway.

It's helpful to give type signatures inside comments in the export list, but hard to keep them consistent, so we don't always do that.


### Imports

List imports in the following order:

- Prelude: Use GHC.Prelude instead of Prelude when possible
- "friends" local to this subsystem (or directory)
- Compiler imports, generally ordered from specific to generic (ie. modules from utils/ and basicTypes/ usually come last)
- Library imports
- Standard Haskell 98 imports last

  ```haskell
  -- Prelude
  import GHC.Prelude

  -- friends
  import SimplMonad

  -- GHC
  import CoreSyn
  import Id
  import BasicTypes

  -- libraries
  import Data.IORef

  -- std
  import Data.List
  import Data.Maybe
  ```

Import library modules from the [boot packages](commentary/libraries) only (boot packages are those packages in the file [packages](https://gitlab.haskell.org/ghc/ghc/blob/master/packages) that have a '-' in the "tag" column). Use `#defines `in `HsVersions.h` when the modules names differ between versions of GHC.  For code inside `#ifdef GHCI`, don't worry about GHC versioning issues, because this code is only ever compiled by the this very version of GHC.

In general, we recommend using explicit import lists only when they convey useful information without imposing excessive an maintenance burden. That is, when you are using only a few declarations from an imported module an explicit import list can be useful for future readers. However, if you use a significant fraction of a module's exports then the export list is likely to incur more of a maintenance and readability burden than benefit.

In general we tend not to use `qualified` imports very often since we try choose names to avoid clashes (e.g. the map function for `OrdList` is named `mapOL` instead of merely `map`).

One area where we do generally require export and import lists is in `hs-boot` files in order to clarify the reason for the boot file. This can help identify refactorings that would eliminate `hs-boot` files.

## 5. Compiler versions and language extensions

GHC must be compilable and validate by the previous two major GHC releases, and itself. It isn't necessary for it to be compileable by every intermediate development version.


### The C Preprocessor (CPP)

Whenever possible we try to avoid using CPP, as it can hide code from the compiler (which means changes that work on one platform can break the build on another) and code using CPP can be harder to understand.

The following CPP symbols are used throughout the compiler:

* `DEBUG`

  Used to enables extra checks and debugging output in the compiler. The ASSERT macro (see `HsVersions.h`) provides assertions which disappear when DEBUG is not defined.

  However, whenever possible, it is better to use `debugIsOn` from the `Util` module, which is defined to be `True` when `DEBUG` is defined and `False` otherwise.  The ideal way to provide debugging output is to use a Haskell expression "`when debugIsOn $ ...`" to arrange that the compiler will be silent when `DEBUG` is off (unless of course something goes wrong or the verbosity level is nonzero). When option `-O` is used, GHC will easily sweep away the unreachable code.

  As a last resort, debugging code can be placed inside `#ifdef DEBUG`, but since this strategy guarantees that only a fraction of the code is seen be the compiler on any one compilation, it is to be avoided when possible.

  Regarding performance, a good rule of thumb is that `DEBUG` shouldn't add more than about 10-20% to the compilation time. This is the case at the moment. If it gets too expensive, we won't use it. For more expensive runtime checks, consider adding a flag - see for example `-dcore-lint`.

* `GHCI`

  Enables GHCi support, including the byte code generator and interactive user interface. This isn't the default, because the compiler needs to be bootstrapped with itself in order for GHCi to work properly. The reason is that the byte-code compiler and linker are quite closely tied to the runtime system, so it is essential that GHCi is linked with the most up-to-date RTS. Another reason is that the representation of certain datatypes must be consistent between GHCi and its libraries, and if these were inconsistent then disaster could follow.


### Platform tests

Please refer to [Platforms and Conventions](commentary/platform-naming) wiki page for an overview of how to handle target specific code in GHC.
```
Note [Special classes]
~~~~~~~~~~~~~~~~~~~~~~
GHC has built-in behaviour for certain type classes, particularly in two places:
* In GHC.Tc.Instance.Class.matchGlobalInst, we have built-in behaviour for solving these special classes.
* In GHC.Tc.Validity.check_special_inst_head, we check for user-written instances of these classes.

We enumerate these classes with the data type SpecialClass,
to help ensure we cover all cases if we later all another.
See #20441.
```