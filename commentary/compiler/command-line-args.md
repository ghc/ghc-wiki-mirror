# Parsing of command line arguments


GHC's many flavours of command line flags make the code interpreting them rather involved. The following provides a brief overview of the processing of these options. All flags can be changed in GHCi sessions using `:set` command or `OPTIONS_GHC` pragma in the source code. There used to be a notion of "static flags" that could not have been changed after a GHC session was started, but those have been since removed in the compiler version 8.2.

Command line-side flags are described by Flag data type defined in [compiler/GHC/Driver/CmdLine.hs](https://gitlab.haskell.org/ghc/ghc/blob/master/compiler/GHC/Driver/CmdLine.hs):

```haskell
data Flag m = Flag
    {   flagName    :: String,     -- Flag, without the leading "-"
        flagOptKind :: OptKind m,  -- What to do if we see it
        flagGhcMode :: GhcFlagMode    -- Which modes this flag affects
    }
```


    
This file contains functions that actually parse the command line parameters. 

Compiler-side flags are managed by functions in [compiler/GHC/Driver/Session.hs](https://gitlab.haskell.org/ghc/ghc/blob/master/compiler/GHC/Driver/Session.hs) file. Looking from the top you will find data types used to describe flags: `DumpFlag`, `GeneralFlag`, `WarningFlag`, `Language`, `SafeHaskellMode`, `ExtensionFlag`, and finally `DynFlags`. The function `defaultDynFlags :: Settings -> DynFlags` initializes some of the flags to default values. Available flags and their respective actions are defined by `dynamic_flags :: [Flag (CmdLineP DynFlags)]`. Also, `fWarningFlags :: [FlagSpec WarningFlag]`, `fFlags :: [FlagSpec GeneralFlag]`, `xFlags :: [FlagSpec ExtensionFlag]` and a few other smaller functions define more flags needed for example for language extensions, warnings and other things. These flags are descibred by the data type `FlagSpec f`:

```haskell
type FlagSpec flag
   = ( String   -- Flag in string form
     , flag     -- Flag in internal form
     , TurnOnFlag -> DynP ())    -- Extra action to run when the flag is found
                                 -- Typically, emit a warning or error
```


Flags described by `FlagSpec` can be reversed, e.g. flags that start with `-f` prefix are reversed by using `-fno-` prefix instead.