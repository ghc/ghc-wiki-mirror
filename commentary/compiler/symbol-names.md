# Symbol Names

Since Haskell allows many symbols in constructor and variable names that C compilers or assembly might not allow (e.g. `:`, `%`, `#`) these have to be encoded using z-encoding.  The encoding is as follows.  See [compiler/GHC/Utils/Encoding.hs](https://gitlab.haskell.org/ghc/ghc/blob/master/compiler/GHC/Utils/Encoding.hs) and https://gitlab.haskell.org/ghc/ghc/snippets/1535 for encoding/decoding implementations.

## Tuples

<table>
<tr><th>Decoded</th><th>Encoded</th><th>Comment</th></tr>       
<tr><td><tt>()</tt></td><td>Z0T</td><td>Unit / 0-tuple</td></tr>
<tr><td></td><td></td><td>There is no Z1T</td></tr>             
<tr><td><tt>(,)</tt></td><td>Z2T</td><td>2-tuple</td></tr>      
<tr><td><tt>(,,)</tt></td><td>Z3T</td><td>3-tuple</td></tr>     
<tr><td>...</td><td></td><td>And so on</td></tr>                
</table>

## Unboxed Tuples

<table>
<tr><th>Decoded</th><th>Encoded</th><th>Comment</th></tr>
<tr><td></td><td></td><td>There is no Z0H</td></tr>
<tr><td><tt>(# #)</tt></td><td>Z1H</td><td>unboxed 1-tuple (note tde space)</td></tr>
<tr><td><tt>(#,#)</tt></td><td>Z2H</td><td>unboxed 2-tuple</td></tr>
<tr><td><tt>(#,,#)</tt></td><td>Z3H</td><td>unboxed 3-tuple</td></tr>
<tr><td>...</td><td></td><td>And so on</td></tr>
</table>

## Alphanumeric Characters

<table>
<tr><th>Decoded</th><th>Encoded</th><th>Comment</th></tr>
<tr><td>a-y, A-Y, 0-9</td><td>a-y, A-Y, 0-9</td><td>Regular letters don&apos;t need escape sequences</td></tr>
<tr><td>z, Z</td><td>zz, ZZ</td><td>&apos;Z&apos; and &apos;z&apos; must be escaped</td></tr>
</table>


## Constructor Characters


<table>
<tr><th>Decoded</th><th>Encoded</th><th>Comment</th></tr>
<tr><td><tt>(</tt></td><td>ZL</td><td>Left</td></tr>
<tr><td><tt>)</tt></td><td>ZR</td><td>Right</td></tr>
<tr><td><tt>[</tt></td><td>ZM</td><td>&apos;M&apos; before &apos;N&apos; in []</td></tr>
<tr><td><tt>]</tt></td><td>ZN</td><td></td></tr>
<tr><td><tt>:</tt></td><td>ZC</td><td>Colon</td></tr>
</table>

## Variable Characters
<table>
<tr><th>Decoded</th><th>Encoded</th><th>Mnemonic</th></tr>
<tr><td><tt>&</tt></td><td>za</td><td>Ampersand</td></tr>
<tr><td><tt>|</tt></td><td>zb</td><td>Bar</td></tr>
<tr><td><tt>^</tt></td><td>zc</td><td>Caret</td></tr>
<tr><td><tt>$</tt></td><td>zd</td><td>Dollar</td></tr>
<tr><td><tt>=</tt></td><td>ze</td><td>Equals</td></tr>
<tr><td><tt>></tt></td><td>zg</td><td>Greater than</td></tr>
<tr><td><tt>#</tt></td><td>zh</td><td>Hash</td></tr>
<tr><td><tt>.</tt></td><td>zi</td><td>The dot of the &apos;i&apos;</td></tr>
<tr><td><tt><</tt></td><td>zl</td><td>Less than</td></tr>
<tr><td><tt>-</tt></td><td>zm</td><td>Minus</td></tr>
<tr><td><tt>!</tt></td><td>zn</td><td>Not</td></tr>
<tr><td><tt>+</tt></td><td>zp</td><td>Plus</td></tr>
<tr><td><tt>'</tt></td><td>zq</td><td>Quote</td></tr>
<tr><td><tt>\</tt></td><td>zr</td><td>Reverse slash</td></tr>
<tr><td><tt>/</tt></td><td>zs</td><td>Slash</td></tr>
<tr><td><tt>*</tt></td><td>zt</td><td>Times sign</td></tr>
<tr><td><tt>_</tt></td><td>zu</td><td>Underscore</td></tr>
<tr><td><tt>%</tt></td><td>zv</td><td>(TODO I don't know what the mnemonic for this one is. Perhaps relatiVe or diVide?)</td></tr>
</table>



## Other


Any other character is encoded as a `z` followed by its hex code (lower case, variable length) followed by `U`.  If the hex code starts with `a`, `b`, `c`, `d`, `e` or `f`, then an extra `0` is placed before the hex code to avoid conflicts with the other escape characters.

## Examples

<table>
<tr><th>Before</th><th>After</th></tr>
<tr><td><tt>Trak</tt></td><td><tt>Trak</tt></td></tr>
<tr><td><tt>foo_wib</tt></td><td><tt>foozuwib</tt></td></tr>
<tr><td><tt>></tt></td><td><tt>zg</tt></td></tr>
<tr><td><tt>>1</tt></td><td><tt>zg1</tt></td></tr>
<tr><td><tt>foo#</tt></td><td><tt>foozh</tt></td></tr>
<tr><td><tt>foo##</tt></td><td><tt>foozhzh</tt></td></tr>
<tr><td><tt>foo##h</tt></td><td><tt>foozhzhh</tt></td></tr>
<tr><td><tt>fooZ</tt></td><td><tt>fooZZ</tt></td></tr>
<tr><td><tt>:+</tt></td><td><tt>ZCzp</tt></td></tr>
<tr><td><tt>()</tt></td><td><tt>Z0T</tt></td></tr>
<tr><td><tt>(,,,,)</tt></td><td><tt>Z5T</tt></td></tr>
<tr><td><tt>(# #)</tt></td><td><tt>Z1H</tt></td></tr>
<tr><td><tt>(#,,,,#)</tt></td><td><tt>Z5H</tt></th></tr>
</table>
