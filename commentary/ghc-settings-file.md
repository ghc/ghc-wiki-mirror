# GHC Settings File

Running `ghc --info` shows various compiler info and settings. A collection of these settings can be modified when necessary. This can be useful for changing things like compiler and linker flags in case there are special needs for a specific installation.

The settings file is located in the `libdir` which can be found by running `ghc --print-libdir`, and it's named `settings`.

If you are on Linux you can find it like so:

```bash
$ echo $(ghc --print-libdir)/settings
/home/johndoe/.ghcup/ghc/9.2.5/lib/ghc-9.2.5/settings
```

## Useful Settings

Here's an incomplete list of settings that might be useful:

* `C compiler link flags`: This sets additional linker flags that will be passed to the C compiler. This can be used, for example, if you have a required library such as `gmp` installed to a non-standard location on your system, and you need to add it's directory to the library search path. Or if you want to override the linker used.

  ```haskell
  [("C compiler link flags", "-fuse-ll=lld -L/home/johndoe/.local/lib")
  ]
  ```
  * **Note:** There is also a separate setting for `ld flags`. ( I'm unsure _exactly_ when ld or the C compiler link flags are each used. )