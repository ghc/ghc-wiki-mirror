> For a quick cheatsheet of how to start documenting GHC or base, see [https://ghc.dev/](ghc.dev).

## Style guides

- [`base` library](commentary/Documentation-Style-Guide/base)
- [GHC compiler](commentary/Documentation-Style-Guide/GHC)
- [GHC User's Guide](commentary/Documentation-Style-Guide/GHC-Users-Guide)