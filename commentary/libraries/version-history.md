# GHC Boot Library Version History

This table lists the versions of GHC against those of its boot libraries, including most notably the `base` library. This may be useful if you ever want to find out which version of the `base` package was bundled with which version of GHC or vice versa.

See also: [LanguagePragmaHistory](language-pragma-history), which lists the language extensions added and/or removed in each GHC version.

See also [index of the libraries](https://downloads.haskell.org/\~ghc/latest/docs/html/libraries/index.html) and changelogs on Hackage (e.g. [base's changelog](http://hackage.haskell.org/package/base/changelog)).

<table>
<tr>
<th>

</th>
<th>

**9\.8.1**
</th>
<th>

**9\.6.3**
</th>
<th>

**9\.6.2**
</th>
<th>

**9\.6.1**
</th>
<th>

**9\.4.8**
</th>
<th>

9\.4.7
</th>
<th>

9\.4.6
</th>
<th>

9\.4.5
</th>
<th>

9\.4.4
</th>
<th>

**9\.4.3**
</th>
<th>

**9\.4.2**
</th>
<th>

**9\.4.1**
</th>
<th>

**9\.2.2**
</th>
<th>

**9\.2.1**
</th>
<th>

**9\.0.2**
</th>
<th>

**9\.0.1**
</th>
<th>

**8\.10.7**
</th>
<th>

**8\.10.6**
</th>
<th>

**8\.10.5**
</th>
<th>

**8\.10.4**
</th>
<th>

**8\.10.3**
</th>
<th>

**8\.10.2**
</th>
<th>

**8\.10.1**
</th>
<th>

**8\.8.4**
</th>
<th>

**8\.8.3**
</th>
<th>

**8\.8.2**
</th>
<th>

**8\.8.1**
</th>
<th>

**8\.6.5**
</th>
<th>

**8\.6.4**
</th>
<th>

**8\.6.3**
</th>
<th>

**8\.6.2**
</th>
<th>

**8\.6.1**
</th>
<th>

**8\.4.4**
</th>
<th>

**8\.4.3**
</th>
<th>

**8\.4.2**
</th>
<th>

**8\.4.1**
</th>
<th>

**8\.2.2**
</th>
<th>

**8\.2.1**
</th>
<th>

**8\.0.2**
</th>
<th>

**8\.0.1**
</th>
<th>

**7\.10.3**
</th>
<th>

**7\.10.2**
</th>
<th>

**7\.10.1**
</th>
<th>

**7\.8.4**
</th>
<th>

**7\.8.3**
</th>
<th>

**7\.8.2**
</th>
<th>

**7\.8.1**
</th>
<th>

**7\.6.3**
</th>
<th>

**7\.6.2**
</th>
<th>

**7\.6.1**
</th>
<th>

**7\.4.2**
</th>
<th>

**7\.4.1**
</th>
<th>

**7\.2.2**
</th>
<th>

**7\.2.1**
</th>
<th>

**7\.0.4**
</th>
<th>

**7\.0.3**
</th>
<th>

**7\.0.2**
</th>
<th>

**7\.0.1**
</th>
</tr>
<tr>
<td>

**Release date**
</td>
<td>2023-10-09</td>
<td>2023-09-26</td>
<td>2023-05-23</td>
<td>2023-03-10</td>
<td>2022-11-10</td>
<td>2023-08-25</td>
<td>2023-08-07</td>
<td>2023-04-18</td>
<td>2023-12-24</td>
<td>2022-11-03</td>
<td>2022-08-22</td>
<td>2022-08-07</td>
<td>2022-03-06</td>
<td>2021-09-29</td>
<td>2021-12-25</td>
<td>2021-02-04</td>
<td>2021-08-27</td>
<td>2021-08-15</td>
<td>2021-06-05</td>
<td>2021-02-05</td>
<td>2020-12-19</td>
<td>2020-08-08</td>
<td>2020-03-24</td>
<td>2020-07-15</td>
<td>2020-02-24</td>
<td>2020-01-16</td>
<td>2019-08-25</td>
<td>2019-04-23</td>
<td>2019-03-05</td>
<td>2018-12-07</td>
<td>2018-11-02</td>
<td>2018-09-21</td>
<td>2018-05-29</td>
<td>2018-05-29</td>
<td>2018-04-19</td>
<td>2018-03-08</td>
<td>2017-07-22</td>
<td>2017-07-22</td>
<td>2017-01-11</td>
<td>2016-05-21</td>
<td>2015-12-08</td>
<td>2015-07-29</td>
<td>2015-03-27</td>
<td>2014-12-23</td>
<td>2014-07-11</td>
<td>2014-04-12</td>
<td>2014-04-09</td>
<td>2013-04-21</td>
<td>2013-01-29</td>
<td>2012-09-06</td>
<td>2012-06-10</td>
<td>2012-02-02</td>
<td>2011-11-11</td>
<td>2011-08-09</td>
<td>2011-06-15</td>
<td>2011-03-27</td>
<td>2011-03-03</td>
<td>2010-11-16</td>
</tr>
<tr>
<td>Cabal</td>
<td>

3\.10.2.0
</td>
<td colspan="3">

3\.10.1.0
</td>
<td colspan="8">

3\.8.1.0
</td>
<td>

3\.6.3.0
</td>
<td>

3\.6.0.0
</td>
<td>

3\.4.1.0
</td>
<td>

3\.4.0.0
</td>
<td colspan="5">

3\.2.1.0
</td>
<td colspan="2">

3\.2.0.0
</td>
<td colspan="3">

3\.0.1.0
</td>
<td>

3\.0.0.0
</td>
<td colspan="5">

2\.4.0.1
</td>
<td colspan="3">

2\.2.0.1
</td>
<td>

2\.2.0.0
</td>
<td>

2\.0.1.0
</td>
<td>

2\.0.0.2
</td>
<td>

1\.24.2.0
</td>
<td>

1\.24.0.0
</td>
<td>

1\.22.5.0
</td>
<td>

1\.22.4.0
</td>
<td>

1\.22.2.0
</td>
<td>

1\.18.1.5
</td>
<td colspan="3">

1\.18.1.3
</td>
<td colspan="3">

1\.16.0
</td>
<td colspan="2">

1\.14.0
</td>
<td colspan="2">

1\.12.0
</td>
<td>

1\.10.2.0
</td>
<td colspan="2">

1\.10.1.0
</td>
<td>

1\.10.0.0
</td>
</tr>
<tr>
<td>Cabal-syntax</td>
<td>

3\.10.2.0
</td>
<td colspan="3">

3\.10.1.0
</td>
<td colspan="8">

3\.8.1.0
</td>
<td colspan="46">

_none_
</td>
</tr>
<tr>
<td>Win32</td>
<td>

2\.13.4.0
</td>
<td colspan="3">

2\.13.3.0
</td>
<td colspan="5">

2\.12.0.1
</td>
<td colspan="3">

2\.12.0.0
</td>
<td colspan="3">

_none_
</td>
<td>

2\.10.0.0
</td>
<td>

_none_
</td>
<td>

2\.6.2.1
</td>
<td colspan="18">

2\.6.1.0
</td>
<td colspan="2">

2\.5.4.1
</td>
<td colspan="2">

2\.3.1.1
</td>
<td colspan="3">

2\.3.1.0
</td>
<td colspan="4">

2\.3.0.2
</td>
<td colspan="3">

2\.3.0.0
</td>
<td colspan="2">

2\.2.2.0
</td>
<td colspan="2">

2\.2.1.0
</td>
<td colspan="4">

2\.2.0.2
</td>
</tr>
<tr>
<td>array</td>
<td>

0\.5.6.0
</td>
<td colspan="3">

0\.5.5.0
</td>
<td colspan="23">

0\.5.4.0
</td>
<td colspan="4">

0\.5.3.0
</td>
<td colspan="7">

0\.5.2.0
</td>
<td colspan="2">

0\.5.1.1
</td>
<td colspan="3">

0\.5.1.0
</td>
<td colspan="4">

0\.5.0.0
</td>
<td colspan="3">

0\.4.0.1
</td>
<td colspan="2">

0\.4.0.0
</td>
<td colspan="2">

0\.3.0.3
</td>
<td colspan="4">

0\.3.0.2
</td>
</tr>
<tr>
<td>base</td>
<td>

4\.19.0.0
</td>
<td>

4\.18.1.0
</td>
<td colspan="2">

4\.18.0.0
</td>
<td>

4\.17.2.1
</td>
<td>

4\.17.2.0
</td>
<td>

4\.17.2.0
</td>
<td>

4\.17.1.0
</td>
<td colspan="4">

4\.17.0.0
</td>
<td>

4\.16.1.0
</td>
<td>

4\.16.0.0
</td>
<td>

4\.15.1.0
</td>
<td>

4\.15.0.0
</td>
<td colspan="2">

4\.14.3.0
</td>
<td>

4\.14.2.0
</td>
<td colspan="3">

4\.14.1.0
</td>
<td>

4\.14.0.0
</td>
<td colspan="4">

4\.13.0.0
</td>
<td colspan="5">

4\.12.0.0
</td>
<td colspan="3">

4\.11.1.0
</td>
<td>

4\.11.0.0
</td>
<td>

4\.10.1.0
</td>
<td>

4\.10.0.0
</td>
<td>

4\.9.1.0
</td>
<td>

4\.9.0.0
</td>
<td>

4\.8.2.0
</td>
<td>

4\.8.1.0
</td>
<td>

4\.8.0.0
</td>
<td>

4\.7.0.2
</td>
<td>

4\.7.0.1
</td>
<td colspan="2">

4\.7.0.0
</td>
<td colspan="2">

4\.6.0.1
</td>
<td>

4\.6.0.0
</td>
<td>

4\.5.1.0
</td>
<td>

4\.5.0.0
</td>
<td>

4\.4.1.0
</td>
<td>

4\.4.0.0
</td>
<td colspan="3">

4\.3.1.0
</td>
<td>

4\.3.0.0
</td>
</tr>
<tr>
<td>bin-package-db</td>
<td colspan="40">

_none_
</td>
<td colspan="18">

0\.0.0.0
</td>
</tr>
<tr>
<td>binary</td>
<td colspan="12">

0\.8.9.1
</td>
<td colspan="2">

0\.8.9.0
</td>
<td colspan="9">

0\.8.8.0
</td>
<td colspan="4">

0\.8.7.0
</td>
<td colspan="5">

0\.8.6.0
</td>
<td colspan="6">

0\.8.5.1
</td>
<td colspan="2">

0\.8.3.0
</td>
<td colspan="2">

0\.7.5.0
</td>
<td>

0\.7.3.0
</td>
<td colspan="4">

0\.7.1.0
</td>
<td colspan="3">

0\.5.1.1
</td>
<td colspan="2">

0\.5.1.0
</td>
<td colspan="2">

0\.5.0.2\*
</td>
<td colspan="4">

_none_
</td>
</tr>
<tr>
<td>bytestring</td>
<td>

0\.12.0.2
</td>
<td>

0\.11.5.2
</td>
<td colspan="2">

0\.11.4.0
</td>
<td>

0\.11.5.3
</td>
<td>

0\.11.5.2
</td>
<td>

0\.11.5.1
</td>
<td>

0\.11.4.0
</td>
<td colspan="4">

0\.11.3.1
</td>
<td>

0\.11.3.0
</td>
<td>

0\.11.1.0
</td>
<td colspan="2">

0\.10.12.1
</td>
<td colspan="5">

0\.10.12.0
</td>
<td colspan="2">

0\.10.10.0
</td>
<td>

0\.10.10.1
</td>
<td colspan="2">

0\.10.10.0
</td>
<td>

0\.10.9.0
</td>
<td colspan="11">

0\.10.8.2
</td>
<td colspan="2">

0\.10.8.1
</td>
<td colspan="3">

0\.10.6.0
</td>
<td colspan="4">

0\.10.4.0
</td>
<td colspan="2">

0\.10.0.2
</td>
<td>

0\.10.0.0
</td>
<td colspan="2">

0\.9.2.1
</td>
<td colspan="2">

0\.9.2.0
</td>
<td colspan="3">

0\.9.1.10
</td>
<td>

0\.9.1.8
</td>
</tr>
<tr>
<td>containers</td>
<td>

0\.6.8
</td>
<td colspan="3">

0\.6.7
</td>
<td colspan="4">

0\.6.7
</td>
<td colspan="4">

0\.6.6
</td>
<td colspan="2">

0\.6.5.1
</td>
<td colspan="2">

0\.6.4.1
</td>
<td colspan="2">

0\.6.5.1
</td>
<td>

0\.6.4.1
</td>
<td colspan="8">

0\.6.2.1
</td>
<td colspan="5">

0\.6.0.1
</td>
<td colspan="4">

0\.5.11.0
</td>
<td colspan="2">

0\.5.10.2
</td>
<td colspan="2">

0\.5.7.1
</td>
<td colspan="3">

0\.5.6.2
</td>
<td colspan="4">

0\.5.5.1
</td>
<td colspan="3">

0\.5.0.0
</td>
<td colspan="2">

0\.4.2.1
</td>
<td colspan="2">

0\.4.1.0
</td>
<td colspan="4">

0\.4.0.0
</td>
</tr>
<tr>
<td>deepseq</td>
<td>

1\.5.0.0
</td>
<td colspan="3">

1\.4.8.1
</td>
<td colspan="8">

1\.4.8.0
</td>
<td>

1\.4.6.1
</td>
<td>

1\.4.6.0
</td>
<td colspan="2">

1\.4.5.0
</td>
<td colspan="16">

1\.4.4.0
</td>
<td colspan="6">

1\.4.3.0
</td>
<td colspan="2">

1\.4.2.0
</td>
<td colspan="3">

1\.4.1.1
</td>
<td colspan="4">

1\.3.0.2
</td>
<td colspan="3">

1\.3.0.1
</td>
<td colspan="2">

1\.3.0.0
</td>
<td colspan="6">

_none_
</td>
</tr>
<tr>
<td>directory</td>
<td colspan="4">

1\.3.8.1
</td>
<td colspan="8">

1\.3.7.1
</td>
<td colspan="3">

1\.3.6.2
</td>
<td>

1\.3.6.1
</td>
<td colspan="9">

1\.3.6.0
</td>
<td>

1\.3.4.0
</td>
<td>

1\.3.3.2
</td>
<td colspan="5">

1\.3.3.0
</td>
<td colspan="4">

1\.3.1.5
</td>
<td colspan="2">

1\.3.0.2
</td>
<td>

1\.3.0.0
</td>
<td>

1\.2.6.2
</td>
<td colspan="3">

1\.2.2.0
</td>
<td colspan="4">

1\.2.1.0
</td>
<td colspan="2">

1\.2.0.1
</td>
<td>

1\.2.0.0
</td>
<td colspan="2">

1\.1.0.2
</td>
<td colspan="2">

1\.1.0.1
</td>
<td colspan="4">

1\.1.0.0
</td>
</tr>
<tr>
<td>exceptions</td>
<td colspan="4">

0\.10.7
</td>
<td colspan="8">

0\.10.5
</td>
<td colspan="11">

0\.10.4
</td>
<td colspan="35">

_none_
</td>
</tr>
<tr>
<td>extensible-exceptions</td>
<td colspan="50">

_none_
</td>
<td colspan="2">

0\.1.1.4
</td>
<td colspan="2">

0\.1.1.3
</td>
<td colspan="4">

0\.1.1.2
</td>
</tr>
<tr>
<td>ffi</td>
<td colspan="52">

_none_
</td>
<td colspan="6">

1\.0
</td>
</tr>
<tr>
<td>filepath</td>
<td colspan="2">

1\.4.100.4
</td>
<td colspan="2">

1\.4.100.1
</td>
<td colspan="9">

1\.4.2.2
</td>
<td colspan="19">

1\.4.2.1
</td>
<td colspan="4">

1\.4.2
</td>
<td colspan="2">

1\.4.1.2
</td>
<td>

1\.4.1.1
</td>
<td>

1\.4.1.0
</td>
<td colspan="3">

1\.4.0.0
</td>
<td colspan="4">

1\.3.0.2
</td>
<td colspan="3">

1\.3.0.1
</td>
<td colspan="2">

1\.3.0.0
</td>
<td colspan="2">

1\.2.0.1
</td>
<td colspan="4">

1\.2.0.0
</td>
</tr>
<tr>
<td>ghc</td>
<td>

9\.8.1
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2\*
</td>
<td>

9\.2.1\*
</td>
<td>

9\.0.2\*
</td>
<td>

9\.0.1\*
</td>
<td>

8\.10.7\*
</td>
<td>

8\.10.6\*
</td>
<td>

8\.10.5\*
</td>
<td>

8\.10.4\*
</td>
<td>

8\.10.3\*
</td>
<td>

8\.10.2\*
</td>
<td>

8\.10.1\*
</td>
<td>

8\.8.4\*
</td>
<td>

8\.8.3\*
</td>
<td>

8\.8.2\*
</td>
<td>

8\.8.1\*
</td>
<td>

8\.6.5\*
</td>
<td>

8\.6.4\*
</td>
<td>

8\.6.3
</td>
<td>

8\.6.2\*
</td>
<td>

8\.6.1\*
</td>
<td>

8\.4.4\*
</td>
<td>

8\.4.3\*
</td>
<td>

8\.4.2\*
</td>
<td>

8\.4.1\*
</td>
<td>

8\.2.2\*
</td>
<td>

8\.2.1\*
</td>
<td>

8\.0.2
</td>
<td>

8\.0.1\*
</td>
<td>

7\.10.3\*
</td>
<td>

7\.10.2\*
</td>
<td>

7\.10.1\*
</td>
<td>

7\.8.4\*
</td>
<td>

7\.8.3\*
</td>
<td>

7\.8.2\*
</td>
<td>

7\.8.1\*
</td>
<td>

7\.6.3\*
</td>
<td>

7\.6.2\*
</td>
<td>

7\.6.1\*
</td>
<td>

7\.4.2\*
</td>
<td>

7\.4.1\*
</td>
<td>

7\.2.2\*
</td>
<td>

7\.2.1\*
</td>
<td>

7\.0.4\*
</td>
<td>

7\.0.3\*
</td>
<td>

7\.0.2\*
</td>
<td>

7\.0.1\*
</td>
</tr>
<tr>
<td>ghc-bignum</td>
<td colspan="12">

1\.3
</td>
<td colspan="2">

1\.2
</td>
<td>

1\.1
</td>
<td>

1\.0
</td>
<td colspan="42">

_none_
</td>
</tr>
<tr>
<td>ghc-binary</td>
<td colspan="54">

_none_
</td>
<td colspan="4">

0\.5.0.2\*
</td>
</tr>
<tr>
<td>ghc-boot</td>
<td>

9\.8.1
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2
</td>
<td>

9\.2.1
</td>
<td>

9\.0.2
</td>
<td>

9\.0.1
</td>
<td>

8\.10.7
</td>
<td>

8\.10.6
</td>
<td>

8\.10.5
</td>
<td>

8\.10.4
</td>
<td>

8\.10.3
</td>
<td>

8\.10.2
</td>
<td>

8\.10.1
</td>
<td>

8\.8.4
</td>
<td>

8\.8.3
</td>
<td>

8\.8.2
</td>
<td>

8\.8.1
</td>
<td>

8\.6.5
</td>
<td>

8\.6.4
</td>
<td>

8\.6.3
</td>
<td>

8\.6.2
</td>
<td>

8\.6.1
</td>
<td>

8\.4.4
</td>
<td>

8\.4.3
</td>
<td>

8\.4.2
</td>
<td>

8\.4.1
</td>
<td>

8\.2.2
</td>
<td>

8\.2.1
</td>
<td>

8\.0.2
</td>
<td>

8\.0.1
</td>
<td colspan="18">

_none_
</td>
</tr>
<tr>
<td>ghc-boot-th</td>
<td>

9\.8.1
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2
</td>
<td>

9\.2.1
</td>
<td>

9\.0.2
</td>
<td>

9\.0.1
</td>
<td>

8\.10.7
</td>
<td>

8\.10.6
</td>
<td>

8\.10.5
</td>
<td>

8\.10.4
</td>
<td>

8\.10.3
</td>
<td>

8\.10.2
</td>
<td>

8\.10.1
</td>
<td>

8\.8.4
</td>
<td>

8\.8.3
</td>
<td>

8\.8.2
</td>
<td>

8\.8.1
</td>
<td>

8\.6.5
</td>
<td>

8\.6.4
</td>
<td>

8\.6.3
</td>
<td>

8\.6.2
</td>
<td>

8\.6.1
</td>
<td>

8\.4.4
</td>
<td>

8\.4.3
</td>
<td>

8\.4.2
</td>
<td>

8\.4.1
</td>
<td>

8\.2.2
</td>
<td>

8\.2.1
</td>
<td>

8\.0.2
</td>
<td>

8\.0.1
</td>
<td colspan="18">

_none_
</td>
</tr>
<tr>
<td>ghc-compact</td>
<td colspan="38">

0\.1.0.0
</td>
<td colspan="20">

_none_
</td>
</tr>
<tr>
<td>ghc-heap</td>
<td>

9\.8.1
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2
</td>
<td>

9\.2.1
</td>
<td>

9\.0.2
</td>
<td>

9\.0.1
</td>
<td>

8\.10.7
</td>
<td>

8\.10.6
</td>
<td>

8\.10.5
</td>
<td>

8\.10.4
</td>
<td>

8\.10.3
</td>
<td>

8\.10.2
</td>
<td>

8\.10.1
</td>
<td>

8\.8.4
</td>
<td>

8\.8.3
</td>
<td>

8\.8.2
</td>
<td>

8\.8.1
</td>
<td>

8\.6.5
</td>
<td>

8\.6.4
</td>
<td>

8\.6.3
</td>
<td>

8\.6.2
</td>
<td>

8\.6.1
</td>
<td colspan="26">

_none_
</td>
</tr>
<tr>
<td>ghc-prim</td>
<td>

0\.11.0
</td>
<td colspan="3">

0\.10.0
</td>
<td colspan="8">

0\.9.1
</td>
<td colspan="2">

0\.8.0
</td>
<td colspan="2">

0\.7.0
</td>
<td colspan="7">

0\.6.1
</td>
<td colspan="9">

0\.5.3
</td>
<td colspan="4">

0\.5.2.0
</td>
<td colspan="2">

0\.5.1.0
</td>
<td colspan="2">

0\.5.0.0
</td>
<td colspan="3">

0\.4.0.0
</td>
<td colspan="4">

0\.3.1.0
</td>
<td colspan="3">

0\.3.0.0
</td>
<td colspan="8">

0\.2.0.0
</td>
</tr>
<tr>
<td>ghci</td>
<td>

9\.8.1
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2
</td>
<td>

9\.2.1
</td>
<td>

9\.0.2
</td>
<td>

9\.0.1
</td>
<td>

8\.10.7
</td>
<td>

8\.10.6
</td>
<td>

8\.10.5
</td>
<td>

8\.10.4
</td>
<td>

8\.10.3
</td>
<td>

8\.10.2
</td>
<td>

8\.10.1
</td>
<td>

8\.8.4
</td>
<td>

8\.8.3
</td>
<td>

8\.8.2
</td>
<td>

8\.8.1
</td>
<td>

8\.6.5
</td>
<td>

8\.6.4
</td>
<td>

8\.6.3
</td>
<td>

8\.6.2
</td>
<td>

8\.6.1
</td>
<td>

8\.4.4
</td>
<td>

8\.4.3
</td>
<td>

8\.4.2
</td>
<td>

8\.4.1
</td>
<td>

8\.2.2
</td>
<td>

8\.2.1
</td>
<td>

8\.0.2
</td>
<td>

8\.0.1
</td>
<td colspan="18">

_none_
</td>
</tr>
<tr>
<td>haskeline</td>
<td colspan="4">

0\.8.2.1
</td>
<td colspan="11">

0\.8.2
</td>
<td>

0\.8.1.0
</td>
<td colspan="2">

0\.8.2
</td>
<td colspan="4">

0\.8.0.1
</td>
<td>

0\.8.0.0
</td>
<td colspan="4">

0\.7.5.0
</td>
<td colspan="5">

0\.7.4.3
</td>
<td colspan="4">

0\.7.4.2
</td>
<td colspan="2">

0\.7.4.0
</td>
<td>

0\.7.3.0
</td>
<td>

0\.7.2.3
</td>
<td colspan="3">

0\.7.2.1
</td>
<td colspan="2">

0\.7.1.2
</td>
<td colspan="13">

_none_
</td>
</tr>
<tr>
<td>haskell2010</td>
<td colspan="43">

_none_
</td>
<td colspan="4">

1\.1.2.0\*
</td>
<td colspan="3">

1\.1.1.0\*
</td>
<td colspan="2">

1\.1.0.1\*
</td>
<td colspan="2">

1\.1.0.0\*
</td>
<td colspan="4">

1\.0.0.0\*
</td>
</tr>
<tr>
<td>haskell98</td>
<td colspan="43">

_none_
</td>
<td colspan="4">

2\.0.0.3\*
</td>
<td colspan="3">

2\.0.0.2\*
</td>
<td colspan="2">

2\.0.0.1\*
</td>
<td colspan="2">

2\.0.0.0\*
</td>
<td colspan="3">

1\.1.0.1
</td>
<td>

1\.1.0.0
</td>
</tr>
<tr>
<td>hoopl</td>
<td colspan="36">

_none_
</td>
<td colspan="2">

3\.10.2.2
</td>
<td colspan="2">

3\.10.2.1
</td>
<td colspan="3">

3\.10.0.2
</td>
<td colspan="4">

3\.10.0.1
</td>
<td colspan="3">

3\.9.0.0
</td>
<td colspan="2">

3\.8.7.3
</td>
<td colspan="2">

3\.8.7.1
</td>
<td colspan="4">

_none_
</td>
</tr>
<tr>
<td>hpc</td>
<td>

0\.7.0.0
</td>
<td colspan="3">

0\.6.2.0
</td>
<td colspan="19">

0\.6.1.0
</td>
<td colspan="17">

0\.6.0.3
</td>
<td colspan="3">

0\.6.0.2
</td>
<td colspan="4">

0\.6.0.1
</td>
<td colspan="3">

0\.6.0.0
</td>
<td colspan="2">

0\.5.1.1
</td>
<td colspan="2">

0\.5.1.0
</td>
<td colspan="4">

0\.5.0.6
</td>
</tr>
<tr>
<td>integer-gmp</td>
<td colspan="16">

1\.1
</td>
<td colspan="7">

1\.0.3.0
</td>
<td colspan="12">

1\.0.2.0
</td>
<td colspan="3">

1\.0.1.0
</td>
<td colspan="2">

1\.0.0.1
</td>
<td colspan="3">

1\.0.0.0
</td>
<td colspan="4">

0\.5.1.0
</td>
<td colspan="3">

0\.5.0.0
</td>
<td colspan="2">

0\.4.0.0
</td>
<td colspan="2">

0\.3.0.0
</td>
<td colspan="3">

0\.2.0.3
</td>
<td>

0\.2.0.2
</td>
</tr>
<tr>
<td>libiserv</td>
<td>

_none_
</td>
<td>

9\.6.3
</td>
<td>

9\.6.2
</td>
<td>

9\.6.1
</td>
<td>

9\.4.8
</td>
<td>

9\.4.7
</td>
<td>

9\.4.6
</td>
<td>

9\.4.5
</td>
<td>

9\.4.4
</td>
<td>

9\.4.3
</td>
<td>

9\.4.2
</td>
<td>

9\.4.1
</td>
<td>

9\.2.2
</td>
<td>

9\.2.1
</td>
<td>

9\.0.2
</td>
<td>

9\.0.1
</td>
<td>

8\.10.7
</td>
<td>

8\.10.6
</td>
<td>

8\.10.5
</td>
<td>

8\.10.4
</td>
<td>

8\.10.3
</td>
<td>

8\.10.2
</td>
<td>

8\.10.1
</td>
<td>

8\.8.4
</td>
<td>

8\.8.3
</td>
<td>

8\.8.2
</td>
<td>

8\.8.1
</td>
<td colspan="3">

8\.6.3
</td>
<td colspan="2">

8\.6.1
</td>
<td colspan="26">

_none_
</td>
</tr>
<tr>
<td>mtl</td>
<td colspan="4">

2\.3.1
</td>
<td colspan="32">

2\.2.2
</td>
<td colspan="22">

_none_
</td>
</tr>
<tr>
<td>old-locale</td>
<td colspan="43">

_none_
</td>
<td colspan="4">

1\.0.0.6
</td>
<td colspan="3">

1\.0.0.5
</td>
<td colspan="2">

1\.0.0.4
</td>
<td colspan="2">

1\.0.0.3
</td>
<td colspan="4">

1\.0.0.2
</td>
</tr>
<tr>
<td>old-time</td>
<td colspan="43">

_none_
</td>
<td colspan="4">

1\.1.0.2
</td>
<td colspan="3">

1\.1.0.1
</td>
<td colspan="2">

1\.1.0.0
</td>
<td colspan="2">

1\.0.0.7
</td>
<td colspan="4">

1\.0.0.6
</td>
</tr>
<tr>
<td>parsec</td>
<td>

3\.1.17.0
</td>
<td colspan="3">

3\.1.16.1
</td>
<td colspan="9">

3\.1.16.1
</td>
<td colspan="14">

3\.1.14.0
</td>
<td colspan="9">

3\.1.13.0
</td>
<td colspan="22">

_none_
</td>
</tr>
<tr>
<td>pretty</td>
<td colspan="36">

1\.1.3.6
</td>
<td colspan="4">

1\.1.3.3
</td>
<td colspan="3">

1\.1.2.0
</td>
<td colspan="4">

1\.1.1.1
</td>
<td colspan="5">

1\.1.1.0
</td>
<td colspan="2">

1\.1.0.0
</td>
<td colspan="4">

1\.0.1.2
</td>
</tr>
<tr>
<td>process</td>
<td>

1\.6.18.0
</td>
<td colspan="3">

1\.6.17.0
</td>
<td>

1\.6.18.0
</td>
<td colspan="2">

1\.6.17.0
</td>
<td>

1\.6.16.0
</td>
<td>

1\.6.16.0
</td>
<td>

</td>
<td colspan="2">

1\.6.15.0
</td>
<td colspan="3">

1\.6.13.2
</td>
<td>

1\.6.11.0
</td>
<td colspan="2">

1\.6.13.2
</td>
<td colspan="4">

1\.6.9.0
</td>
<td>

1\.6.8.2
</td>
<td>

1\.6.9.0
</td>
<td>

1\.6.8.0
</td>
<td>

1\.6.7.0
</td>
<td>

1\.6.5.1
</td>
<td colspan="2">

1\.6.5.0
</td>
<td colspan="7">

1\.6.3.0
</td>
<td colspan="2">

1\.6.1.0
</td>
<td>

1\.4.3.0
</td>
<td>

1\.4.2.0
</td>
<td colspan="3">

1\.2.3.0
</td>
<td colspan="4">

1\.2.0.0
</td>
<td colspan="3">

1\.1.0.2
</td>
<td colspan="2">

1\.1.0.1
</td>
<td colspan="2">

1\.1.0.0
</td>
<td colspan="3">

1\.0.1.5
</td>
<td>

1\.0.1.4
</td>
</tr>
<tr>
<td>random</td>
<td colspan="34">

_none_
</td>
<td>

1\.1
</td>
<td>

_none_
</td>
<td>

1\.1
</td>
<td colspan="17">

_none_
</td>
<td colspan="4">

1\.0.0.3
</td>
</tr>
<tr>
<td>rts</td>
<td colspan="15">

1\.0.2
</td>
<td>

1\.0
</td>
<td colspan="3">

1\.0.1
</td>
<td colspan="39">

1\.0
</td>
</tr>
<tr>
<td>semaphore-compat</td>
<td>

1\.0.0
</td>
<td colspan="57">

_none_
</td>
</tr>
<tr>
<td>stm</td>
<td>

2\.5.2.1
</td>
<td colspan="11">

2\.5.1.0
</td>
<td>

2\.5.0.2
</td>
<td colspan="3">

2\.5.0.0
</td>
<td colspan="3">

2\.5.0.1
</td>
<td colspan="13">

2\.5.0.0
</td>
<td>

2\.4.5.1
</td>
<td colspan="3">

2\.4.5.0
</td>
<td colspan="22">

_none_
</td>
</tr>
<tr>
<td>template-haskell</td>
<td>

2\.21.0.0
</td>
<td colspan="3">

2\.20.0.0
</td>
<td colspan="8">

2\.19.0.0
</td>
<td colspan="2">

2\.18.0.0
</td>
<td colspan="2">

2\.17.0.0
</td>
<td colspan="7">

2\.16.0.0
</td>
<td colspan="4">

2\.15.0.0
</td>
<td colspan="5">

2\.14.0.0
</td>
<td colspan="4">

2\.13.0.0
</td>
<td colspan="2">

2\.12.0.0
</td>
<td>

2\.11.1.0
</td>
<td>

2\.11.0.0
</td>
<td colspan="3">

2\.10.0.0
</td>
<td colspan="4">

2\.9.0.0
</td>
<td colspan="3">

2\.8.0.0
</td>
<td colspan="2">

2\.7.0.0
</td>
<td colspan="2">

2\.6.0.0
</td>
<td colspan="4">

2\.5.0.0
</td>
</tr>
<tr>
<td>terminfo</td>
<td colspan="4">

0\.4.1.6
</td>
<td colspan="11">

0\.4.1.5
</td>
<td colspan="12">

0\.4.1.4
</td>
<td colspan="5">

0\.4.1.2
</td>
<td colspan="4">

0\.4.1.1
</td>
<td colspan="2">

0\.4.1.0
</td>
<td colspan="2">

0\.4.0.2
</td>
<td colspan="3">

0\.4.0.1
</td>
<td colspan="2">

0\.4.0.0
</td>
<td colspan="13">

_none_
</td>
</tr>
<tr>
<td>text</td>
<td>

2\.1
</td>
<td colspan="3">

2\.0.2
</td>
<td colspan="4">

2\.0.2
</td>
<td colspan="4">

2\.0.1
</td>
<td colspan="3">

1\.2.5.0
</td>
<td colspan="6">

1\.2.4.1
</td>
<td colspan="2">

1\.2.3.2
</td>
<td colspan="4">

1\.2.4.0
</td>
<td colspan="6">

1\.2.3.1
</td>
<td colspan="3">

1\.2.3.0
</td>
<td colspan="22">

_none_
</td>
</tr>
<tr>
<td>time</td>
<td colspan="12">

1\.12.2
</td>
<td colspan="2">

1\.11.1.1
</td>
<td colspan="13">

1\.9.3
</td>
<td colspan="11">

1\.8.0.2
</td>
<td colspan="2">

1\.6.0.1
</td>
<td colspan="3">

1\.5.0.1
</td>
<td colspan="4">

1\.4.2
</td>
<td colspan="3">

1\.4.0.1
</td>
<td colspan="2">

1\.4
</td>
<td colspan="2">

1\.2.0.5
</td>
<td colspan="4">

1\.2.0.3
</td>
</tr>
<tr>
<td>transformers</td>
<td colspan="4">

0\.6.1.0
</td>
<td colspan="25">

0\.5.6.2
</td>
<td colspan="7">

0\.5.5.0
</td>
<td colspan="4">

0\.5.2.0
</td>
<td colspan="3">

0\.4.2.0
</td>
<td colspan="4">

0\.3.0.0
</td>
<td colspan="11">

_none_
</td>
</tr>
<tr>
<td>unix</td>
<td>

2\.8.3.0
</td>
<td colspan="3">

2\.8.1.0
</td>
<td colspan="8">

2\.7.3
</td>
<td colspan="26">

2\.7.2.2
</td>
<td>

2\.7.2.1
</td>
<td>

2\.7.2.0
</td>
<td colspan="3">

2\.7.1.0
</td>
<td colspan="4">

2\.7.0.1
</td>
<td colspan="2">

2\.6.0.1
</td>
<td>

2\.6.0.0
</td>
<td>

2\.5.1.1
</td>
<td>

2\.5.1.0
</td>
<td colspan="2">

2\.5.0.0
</td>
<td colspan="3">

2\.4.2.0
</td>
<td>

2\.4.1.0
</td>
</tr>
<tr>
<td>xhtml</td>
<td colspan="35">

3000\.2.2.1
</td>
<td colspan="3">

3000\.2.2
</td>
<td colspan="7">

3000\.2.1
</td>
<td colspan="13">

_none_
</td>
</tr>
<tr>
<th>

</th>
<th>

**9\.8.1**
</th>
<th>

**9\.6.3**
</th>
<th>

**9\.6.2**
</th>
<th>

**9\.6.1**
</th>
<th>

**9\.4.8**
</th>
<th>

9\.4.7
</th>
<th>

9\.4.6
</th>
<th>

9\.4.5
</th>
<th>

9\.4.4
</th>
<th>

**9\.4.3**
</th>
<th>

**9\.4.2**
</th>
<th>

**9\.4.1**
</th>
<th>

**9\.2.2**
</th>
<th>

**9\.2.1**
</th>
<th>

**9\.0.2**
</th>
<th>

**9\.0.1**
</th>
<th>

**8\.10.7**
</th>
<th>

**8\.10.6**
</th>
<th>

**8\.10.5**
</th>
<th>

**8\.10.4**
</th>
<th>

**8\.10.3**
</th>
<th>

**8\.10.2**
</th>
<th>

**8\.10.1**
</th>
<th>

**8\.8.4**
</th>
<th>

**8\.8.3**
</th>
<th>

**8\.8.2**
</th>
<th>

**8\.8.1**
</th>
<th>

**8\.6.5**
</th>
<th>

**8\.6.4**
</th>
<th>

**8\.6.3**
</th>
<th>

**8\.6.2**
</th>
<th>

**8\.6.1**
</th>
<th>

**8\.4.4**
</th>
<th>

**8\.4.3**
</th>
<th>

**8\.4.2**
</th>
<th>

**8\.4.1**
</th>
<th>

**8\.2.2**
</th>
<th>

**8\.2.1**
</th>
<th>

**8\.0.2**
</th>
<th>

**8\.0.1**
</th>
<th>

**7\.10.3**
</th>
<th>

**7\.10.2**
</th>
<th>

**7\.10.1**
</th>
<th>

**7\.8.4**
</th>
<th>

**7\.8.3**
</th>
<th>

**7\.8.2**
</th>
<th>

**7\.8.1**
</th>
<th>

**7\.6.3**
</th>
<th>

**7\.6.2**
</th>
<th>

**7\.6.1**
</th>
<th>

**7\.4.2**
</th>
<th>

**7\.4.1**
</th>
<th>

**7\.2.2**
</th>
<th>

**7\.2.1**
</th>
<th>

**7\.0.4**
</th>
<th>

**7\.0.3**
</th>
<th>

**7\.0.2**
</th>
<th>

**7\.0.1**
</th>
</tr>
</table>

Note: A `*` after the version number denotes the package being hidden by default.

(The table above is generated by a script located at https://gitlab.haskell.org/bgamari/ghc-utils/tree/master/library-versions)

A table covering some GHC 6.\* releases can be found at https://wiki.haskell.org/Libraries_released_with_GHC