Object Linking is the process of combining object files, binary files generated from compiled sources. In Haskell, each module is compiled to an object file, and these object files are then put together with the use of a dedicated program: the linker.

## Static, Dynamic

We often confer an adjective to object linking, like "dynamic" or "static". The truth is that they represent two ends of a spectrum, because you have some freedom to decide how your dependencies are linked. However for safety it is better to always be consistent when linking your Haskell dependencies.

<dl>
<dt>Dynamic linking<dt>
<dd>This is a flexible way of assembling object files, where instead of mashing all the object files together, their paths are put in the executable and loaded in memory upon execution. The executable is much smaller since most dependencies live elsewhere on disk, and recompiling an individual portion of the program is faster. But the path to these dependencies is hardcoded and thus the program is not suitable for distribution.
</dd>

<dt>Static linking<dt>
<dd>With static linking, all object files from compiling Haskell modules and C dependencies are included in the final binary. This produces a self-contained executable that has no runtime dependencies in terms of external libraries, can be used by users of operating systems that have slight differences in terms of library versions installed, because the static executable will not require them. One downside is that changing a component of the executable (that used to be an object file) leads to the recompilation of the whole thing. 
</dd>
</dl>

## "Haskell" static, "Native" static

Haskell modules are compiled to object files that the traditional C toolchains understand, and this allows the programmer to specify the modalities of linking during development and distribution. As such, Haskell dependencies can be linked in static or dynamic fashion.

However the story is not the same for C libraries, and especially for one of them: the standard C library that we all need in our programs. 

### Linux systems

If you are running a GNU/Linux system, the GNU lib C (_glibc_) should not be statically linked, and in fact ultimately cannot be statically linked:

> The most important reason why glibc should not be statically linked, is that it makes extensive internal use of dlopen, to load NSS (Name Service Switch) modules and iconv conversions. The modules themselves refer to C library functions. If the main program is dynamically linked with the C library, that's no problem. But if the main program is statically linked with the C library, dlopen has to go load a second copy of the C library to satisfy the modules' load requirements.

-- https://stackoverflow.com/a/57478728

However, some non-GNU Linux operating systems like Alpine Linux ship a different standard C library called `musl`. From musl's official website:

> Binaries statically linked with musl have no external dependencies, even for features like DNS lookups or character set conversions that are implemented with dynamic loading on glibc. An application can really be deployed as a single binary file and run on any machine with the appropriate instruction set architecture and Linux kernel or Linux syscall ABI emulation layer.

-- https://www.musl-libc.org/intro.html

This is why distributing statically-linked (and thus ultra-compatible) binaries on the Linux platform is commonly done by compiling the project as static on an Alpine system.

### macOS & BSDs

These operating systems do not provide guarantees of stability for their syscalls between major releases,
which makes it unreliable. The maintainers do try to keep stability, more as a courtesy than as a real constraint on themselves.

While it is possible to statically link against the BSD standard C libraries, macOS does not allow for static linking against its standard C library, `libSystem`. 

Since these ecosystems are much more unified however, this causes less trouble because binary distribution is done through a unified channel for each OS, with clear-cut releases and surrounding packages, and especially the kernel.

## Choice of linker

The choice of the linker program is not without heavy compromises. Speed, reliability and support of features like linker scripts all come into play when choosing which linker to use, or even whether or not a custom linker should be written. For this reason, GHC has logic in its `configure` script to choose a reasonable default linker for the platform (this can be disabled using the `--disable-ld-override` flag).

<dl>

<dt>ld.bfd</dt>
<dd>GNU `binutils`' original linker implementation; supports a variety of object file formats (ELF, MachO, PE); still used by default on many platforms.
<dd>

<dt>ld.gold</dt>
<dd>A newer implementation provided by GNU `binutils`. GHC generally tries to use `gold` as it is considerably faster than `ld.bfd`. However it cannot be used reliably on Alpine Linux systems, and so ghcup will use ld.bfd
</dd>

<dt>lld</dt>
<dd>Considerably faster than `ld.gold` and `ld.bfd` although not as widely installed; may break Linux at least.²
</dd>

<dt>mold</dt>
<dd>still generally considered too new/experimental. Specifically, `mold` does not support the linker scripts that GHC uses.³ </dd>

</dl>

## See also

* https://gitlab.haskell.org/ghc/ghc/-/wikis/dynamic-linking-debate
* https://gitlab.haskell.org/ghc/ghc/-/wikis/shared-libraries/platform-support
* https://gitlab.haskell.org/ghc/ghc/-/wikis/windows-dynamic-linking

## References

* ¹ #17508
* ² https://bugzilla.redhat.com/show_bug.cgi?id=2116508
* ³ #20871

## Addenda
 
### What dynamic linking looks like

When enabling `executable-dynamic: True` in your cabal file and querying the shared object dependencies of a binary with `ldd(1)`, you will see a result like this:

```
ldd /home/hecate/Projects/get-tested/dist-newstyle/build/x86_64-linux/ghc-9.6.3/get-tested-0.1.6.0/x/get-tested/build/get-tested/get-tested
	linux-vdso.so.1 (0x00007fffad7cc000)
	libm.so.6 => /lib64/libm.so.6 (0x00007f0efe7ad000)
	libHStext-display-0.0.5.1-7b9c82f291f730b85bf45eaa0e84426f301d6c83353d578226ec29a21480fbd4-ghc9.6.3.so => /home/hecate/.cabal/store/ghc-9.6.3/text-display-0.0.5.1-7b9c82f291f730b85bf45eaa0e84426f301d6c83353d578226ec29a21480fbd4/lib/libHStext-display-0.0.5.1-7b9c82f291f730b85bf45eaa0e84426f301d6c83353d578226ec29a21480fbd4-ghc9.6.3.so (0x00007f0efe71a000)
	libHSoptparse-applicative-0.18.1.0-8910615a9d0123f7f5f1f95d4bec372f129d4f120087a6f894d58fd939e77fdb-ghc9.6.3.so => /home/hecate/.cabal/store/ghc-9.6.3/optparse-applicative-0.18.1.0-8910615a9d0123f7f5f1f95d4bec372f129d4f120087a6f894d58fd939e77fdb/lib/libHSoptparse-applicative-0.18.1.0-8910615a9d0123f7f5f1f95d4bec372f129d4f120087a6f894d58fd939e77fdb-ghc9.6.3.so (0x00007f0efe624000)
	libHSprocess-1.6.17.0-ghc9.6.3.so => /home/hecate/.ghcup/ghc/9.6.3/lib/ghc-9.6.3/lib/x86_64-linux-ghc-9.6.3/libHSprocess-1.6.17.0-ghc9.6.3.so (0x00007f0efe5e3000)
	libHSprettyprinter-ansi-terminal-1.1.3-b7ca9982ad2c8f333f5fba115cc1326fa69084972ad238175f27ae8489ff9208-ghc9.6.3.so => /home/hecate/.cabal/store/ghc-9.6.3/prettyprinter-ansi-terminal-1.1.3-b7ca9982ad2c8f333f5fba115cc1326fa69084972ad238175f27ae8489ff9208/lib/libHSprettyprinter-ansi-terminal-1.1.3-b7ca9982ad2c8f333f5fba115cc1326fa69084972ad238175f27ae8489ff9208-ghc9.6.3.so (0x00007f0efe5ab000)
``` 