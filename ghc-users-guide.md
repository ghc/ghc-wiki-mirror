## HEAD documentation 

- [User's Guide](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/) 
- [Boot Libraries](https://ghc.gitlab.haskell.org/ghc/doc/libraries/)

The content here is deployed by the latest CI build, so it may temporarily disappear if the build was unsuccessful for some reason.

## Latest release documentation

- [User's Guide](https://downloads.haskell.org/ghc/latest/docs/users_guide/)
- [Boot Libraries](https://downloads.haskell.org/ghc/latest/docs/libraries/)

Documentation for previous releases can be found at https://downloads.haskell.org/ghc/<x.y.z>/docs/html (before GHC 9.4) or at https://downloads.haskell.org/ghc/<x.y.z>/docs (for GHC 9.4 and newer).
