# Merge request conventions

See [Contributing a Patch](Contributing-a-Patch) for a description of the merge request and code review process.

## Field meanings

 * **Title:** GitLab maintains a convention where MRs whose title begins with `Draft:` are marked as work in progress. This marker is understood to mean that review is not yet required. Perhaps a contributor will offer a review regardless, and contributors of Draft patches can request reviews from individuals. But if you mark your patch as Draft and it gets no reviews, that is why.
 * **Milestone:** The first release which should contain the merge request (or a backported version of it)
 * **Labels:** This encodes a number of things including:
    * The state of the merge request (e.g. ~"backport needed")
    * The topical areas the merge request affects (e.g. ~simplifier)
    * Whether the merge request affects the user-facing libraries shipped with GHC (the ~"user facing" label)

## Merge request checklist

See the [merge request description template](https://gitlab.haskell.org/ghc/ghc/-/blob/master/.gitlab/merge_request_templates/Default.md) for the checklist of requirements that appears when creating a merge request.

## Review checklist

The [review checklist is now here](Contributing-a-Patch#5-review-checklist).