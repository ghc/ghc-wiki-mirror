This page is a working document about the JavaScript backend in GHC and is regularly updated. The JavaScript backend has been implemented (cc25d52e0f65d54c052908c7d91d5946342ab88a) and is included as a code generator as of GHC 9.6.

## Build instructions

Build instructions can be found on [javascript-backend/building](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/building).

A demo that compiles and runs `Hello World` can be found [here](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/building#compiling-hello-world).

A breakdown of build artifacts a JavaScript cross-compiling GHC produces can be found [here](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/building#compiler-output-and-build-artifacts).

## Libraries

The status of support for the JavaScript backend in various libraries can be found at [javascript-backend/ecosystem](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/javascript-backend-ecosystem).

## Roadmap

- [Open issues](https://gitlab.haskell.org/ghc/ghc/-/issues/?sort=created_date&state=opened&label_name%5B%5D=javascript&first_page_size=20)
- [Bug triage](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/bug_triage)

GHC 9.6:

* [x] (cc25d52e0f65d54c052908c7d91d5946342ab88a) Boot libraries build
* [x] (394b91ce859653231813fb9af77c26664063c1b6) gitlab CI tests the backend
* [x] FFI: support for foreign imports

GHC 9.8:

* [x] Feature: support for Template Haskell (!9779)
* [x] Feature: support for ~~Foreign exports~~ Callbacks (https://gitlab.haskell.org/ghc/ghc/-/issues/23126)

GHC 9.10:

* [ ] Update GHCJS ecosystem ([ghcjs-dom](https://hackage.haskell.org/package/ghcjs-dom), etc.). https://github.com/ghcjs/ghcjs-dom/pull/104
* [x] Perf: optimize generated JS code for size and speed https://gitlab.haskell.org/ghc/ghc/-/merge_requests/11507
* [ ] Perf: optimize JS backend (make compiler faster)
* [ ] Correctness: implement and use a typed JS EDSL in the JS backend itself (in progress, see [#22736](https://gitlab.haskell.org/ghc/ghc/-/issues/22736) for more details)
* [ ] Correctness: fix bugs found by the testsuite that have been disabled for now (in progress, see [bug triage](https://gitlab.haskell.org/ghc/ghc/-/wikis/javascript-backend/bug_triage) page)
* [x] Support for C-sources: idea is to compile via Emscripten and to automatically ~~generate glue code~~.

GHC 9.12+:
* [ ] FFI: "inlined" foreign imports (JS syntax with named argument placeholders)
* [ ] Feature: profiling (CC, eventlog, ticky-ticky, etc.)
* [ ] GHCi: GHCi support (including debugger)
* [ ] GHCi.js: Haskell code interpreter in the browser
* [ ] Feature: support for Foreign exports

## Missing features

- [ ] Eventlog: markers and events are currently discarded but can easily be printed to stderr (see rts/js/eventlog.js).
- [ ] Delimited continuations: not supporter at all for now
- [ ] Compact regions: not supported at all for now.


## Further Reading and Demos

- [Say Hello World to JS](https://engineering.iog.io/2022-12-13-ghc-js-backend-merged)
- [Run Haskell to JS code in the browser](https://engineering.iog.io/2023-01-24-javascript-browser-tutorial)

## FAQ

### How does the JS backend interact with the on-going plans to make GHC runtime retargetable? Is the current plan for JS to merge before runtime retargetability, therefore requiring the user to build a new compiler to use the backend?

There is no interaction. Currently even with GHCJS we need two compilers (one for the JS target and one for the host) so having a single multi-target compiler isn't mandatory.

In the long term, having the ability to select the backend without rebuilding the compiler would probably improve the user experience though.

### Is the expectation that the JS backend passes the entirety of the testsuite; if not, will it be a separate testsuite way?

It should be a separate testsuite way as there are some features that are not expected to pass (e.g. support for `compact`).

### Is the RTS going to support the full feature surface supported by GHC's C RTS?

Probably not (e.g. no plan for `compact` support afaik). Do we have a list of GHC's C RTS features that we could use to indicate supported features?

### How are "ways" interpreted in the context of Javascript? For instance, "dyn" makes little sense in this context. Depending upon the RTS implementation, the same might be said of `-debug`, `-threaded`, and `-eventlog` (cf #20741)

GHCJS also has different RTS "ways" (e.g. to enable some assertions). Additionally we could imagine generating different code for different targets (NodeJS, browsers, etc.).

Currently GHCJS just ignore "ways" (e.g. it returns the same code for both objects with `-dynamic-too`). Until #20741 is properly fixed, we could check command-line flags when the JS backend is enabled to disallow `-dynamic`, `-dynamic-too`, etc.

As we have workarounds, we don't plan to work on #20741 until we have a usable JS backend. But we might work on it after it's done. Any help or feedback on #20741 is welcome.

### What are we going to do about GHC's many native-code-centric flags (e.g. -fPIC, -shared, -msse2). Will these be errors if used when targeting Javascript?

There are currently ignored by backends that don't use them. We could probably detect and report their use during command-line flags validation but it is out of the scope of the JS backend implementation.

### Are there plans to support the Ticky-Ticky profiler? Cost-centre profiling?

I don't see why we wouldn't support them (cost centre profiling has been supported in GHCJS before, but a few pieces are broken, ticky-ticky profiling hasn't been supported yet)

In addition, we could also support JS specific debugging to harness browser's debugging tools.

### Is there a design for Javascript FFI? Have we considered whether this should go through the GHC Proposals Process given that it is essentially adding new syntax?

This point needs to be discussed, especially with people working on a WebAssembly backend for GHC as ideally they should both use the same FFI interface.

Perhaps worth a ghc-proposal.

TODO: document current GHCJS and Asterius interfaces

### What is the plan for handling the various C bits in base/text/bytestring/etc.? Specifically, has there been any motion on discussions with upstreams regarding upstreaming the Javascript implementations?

We plan to propose patches for these libraries. We can use CPP to condition JS specific code to `HOST_ARCH=javascript` and similarly in `.cabal` files (`arch("javascript")`).)

### Will GHC need to be aware of the system's Javascript interpreter (e.g. know the path to nodejs)? Presumably yes as we will at very least need to know how to start `iserv`.

Yes. In the longer term, it would probably be better for the external interpreter to be configured per target in a settings file. Probably something to discuss in the context of [#19877](https://gitlab.haskell.org/ghc/ghc/-/issues/19877 "Add support for several toolchains").

### What needs to happen to put proper JS backend support in Cabal? Is upstream aware of this plan? Presumably `c-sources` support will require emscripten support in both Cabal and GHC?

`js-sources` support has been fixed for GHC. `c-sources` support is still far on the roadmap.

Note that Cabal currently doesn't support cross-compilation. It would be good to make it support two toolchains: one for the host (plugins, Setup.hs), one for the target. It would also need to use the correct platform to resolve conditionals in `.cabal` files.

## References

- Haskell Symposium 2013 paper: [demo proposal](https://www.haskell.org/haskell-symposium/2013/ghcjs.pdf)
- HIW2015: [slides](https://wiki.haskell.org/wikiupload/9/95/HIW15-Stegeman-The_State_of_the_GHCJS.pdf)
- State of WebGHC 2019 (compared with GHCJS): https://webghc.github.io/2019/01/18/state-of-webghc-january-2019.html