The following tables describe to what extent GHC currently supports
various platforms.  To find out who is responsible for each platform, see [GHC Code Owners](code-owners).

For information about what distributions GHC is part of, see the [distribution packages](http://haskell.org/ghc/distribution_packages) page.


## Tier 1 platforms

Tier 1 platforms are our top priority.  We only release GHC when they all work. For a platform to qualify as a tier 1 platform it must have:

- An active GitLab CI runner, capable of doing full builds and uploading binary distributions.
- An active sponsor, willing to investigate and fix platform-specific bugs, and 
  to work with us during the release process. In many cases this sponsor is GHC HQ.

<table>
<thead>
<tr class="header">
<th>OS</th>
<th>Architecture</th>
<th>Supported</th>
<th>Test Configurations</th>
<th>Comments</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://gitlab.haskell.org/ghc/ghc/-/wikis/platforms/windows">Windows</a></td>
<td>x86-64</td>
<td>Windows 10 and later</td>
<td>Windows 2019 Server</td>
<td>Only 64-bit targets. DLL support currently quite limited due to platform limitations (see #5987)</td>
</tr>
<tr class="even">
<td>macOS</td>
<td>x86-64</td>
<td>10.10 (Yosemite) and later</td>
<td>13.4 (Ventura)</td>
<td></td>
</tr>
<tr class="odd">
<td>macOS</td>
<td>AArch64</td>
<td>11.3 (Big Sur) and later</td>
<td>13.2 (Ventura)</td>
<td></td>
</tr>
<tr class="even">
<td>Linux (Ubuntu)</td>
<td>x86-64</td>
<td>18.04 (Bionic) and later</td>
<td>18.04, 20.04</td>
<td></td>
</tr>
<tr class="odd">
<td>Linux (Debian)</td>
<td>x86 (32-bit)</td>
<td>10 (Stretch) and later</td>
<td>10</td>
<td></td>
</tr>
<tr class="even">
<td>Linux (Debian)</td>
<td>x86-64</td>
<td>9 (Stretch) and later</td>
<td>9, 10, 11</td>
<td></td>
</tr>
<tr class="odd">
<td>Linux (Debian)</td>
<td>AArch64</td>
<td>10 (Buster) and later</td>
<td>10</td>
<td></td>
</tr>
<tr class="even">
<td>Linux (Fedora)</td>
<td>x86-64</td>
<td>33 and later</td>
<td>33</td>
<td>Fedora 33, while EOL since 2021-11-30, is broadly compatible with many other distributions due to using <code>glibc-2.32</code></td>
</tr>
<tr class="odd">
<td>Linux (CentOS)</td>
<td>x86-64</td>
<td>7 and later</td>
<td>7</td>
<td>Highly compatible with RHEL</td>
</tr>
<tr class="even">
<td>Linux (Alpine)</td>
<td>x86-64</td>
<td>3.12 and later</td>
<td>3.12</td>
<td>Widely used in containers; uses musl and busybox instead of GNU userland.</td>
</tr>
</tbody>
</table>


## Tier 2 platforms

Tier 2 platforms work (to varying degrees), but we rely on community support for
developing, testing, and building distributions.  We may release GHC
with some Tier 2 platforms not working.

Platform-specific bugs on Tier 2 platforms are marked "low priority" (unless there's
a strong reason not to do so), not because they are unimportant to the users of that
platform, but to express the fact that they aren't going to hold up the release.


We'd like to promote as many
Tier 2 platforms as possible to Tier 1, as soon as they meet the Tier 1 criteria.


<table>
<tr>
  <th>Architecture</th>
  <th>OS</th>
  <th>Platform Triple</th>
  <th>GHCi</th>
  <th>NCG</th>
  <th>Dyn libs</th>
  <th>Wiki Page</th>
</tr>
<tr>
  <td>x86</td>
  <td>FreeBSD</td>
  <td>i386-portbld-freebsd</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>Yes</td>
  <td> <a href="free-bsd-ghc">FreeBSDGhc</a></td>
</tr>
<tr>
   <td>x86-64</td>
   <td>FreeBSD</td>
   <td>amd64-portbld-freebsd</td>
   <td>Yes</td>
   <td>Yes</td>
   <td>Yes</td>
   <td><a href="free-bsd-ghc">FreeBSDGhc</a> </td>
</tr>
<tr>
  <td>x86</td>
  <td>OpenBSD</td>
  <td>i386-unknown-openbsd</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>No</td>
  <td></td>
</tr>
<tr>
  <td>x86-64</td>
  <td>OpenBSD</td>
  <td>amd64-unknown-openbsd</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>No</td>
  <td><a href="building/preparation/open-bsd">Preparing and Building OpenBSD</a> </td>
</tr>
<tr>
  <td>x86-64</td>
  <td>DragonFly</td>
  <td>x86_64-portbld-dragonfly</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>Yes</td>
  <td></td>
</tr>
<tr>
  <td>PowerPC</td>
  <td>Linux</td>
  <td>powerpc-unknown-linux</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>Yes</td>
  <td></td>
</tr>
<tr>
  <td>PowerPC64</td>
  <td>Linux</td>
  <td>powerpc64-unknown-linux</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>Yes</td>
  <td></td>
</tr>
<tr>
  <td>PowerPC64le</td>
  <td>Linux</td>
  <td>powerpc64le-unknown-linux</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>Yes</td>
  <td></td>
</tr>
<tr>
  <td>S/390</td>
  <td>Linux</td>
  <td>s390-ibm-linux</td>
  <td>?</td>
  <td>No</td>
  <td>?</td>
  <td></td>
</tr>
<tr>
  <td>ARMv7</td>
  <td>Linux</td>
  <td>arm-unknown-linux</td>
  <td>Yes</td>
  <td>No</td>
  <td>Yes</td>
  <td></td>
</tr>
<tr>
  <td>ARMv7</td>
  <td>Debian armel</td>
  <td>arm-linux-gnueabi</td>
  <td>?</td>
  <td>No</td>
  <td>?</td>
  <td> <a href="building/arm-linux-gnu-eabi">Building/ARMLinuxGnuEABI</a></td>
</tr>
</table>

**S4** shared libraries are supported on Solaris 11 version 11/11 and higher

In most cases, binaries for the Tier 2 platforms can be downloaded from the [Distribution Packages](http://www.haskell.org/ghc/distribution_packages) page, e.g. you can get binaries for most of the Linux platforms from Debian. In some cases, for example the Solaris platforms, you'll need to go to the [download page](http://www.haskell.org/ghc/download) of a particular release to get a bindist.

## Tier 3 platforms

Tier 3 platforms worked in the past, but probably do not work now.

<table>
<tr>
  <th>Architecture</th>
  <th>OS</th>
  <th>Platform Triple</th>
  <th>GHCi</th>
  <th>NCG</th>
  <th>Dyn libs</th>
  <th>Wiki Page</th>
</tr>
<tr>
  <td>ARM</td>
  <td>iOS</td>
  <td>arm-apple-darwin10</td>
  <td>No</td>
  <td>Yes</td>
  <td>No</td>
  <td><a href="building/cross-compiling/i-os">Building/CrossCompiling/iOS</a></td>
</tr>
<tr>
  <td>RISC-V (rv64)</td>
  <td>Linux</td>
  <td>riscv64-unknown-linux</td>
  <td>No</td>
  <td>No</td>
  <td>No</td>
  <td></td>
</tr>
<tr>
  <td>PowerPC</td>
  <td>AIX</td>
  <td>powerpc-ibm-aix</td>
  <td>No</td>
  <td>Yes</td>
  <td>No</td>
  <td> <a href="building/aix">Building/AIX</a></td>
</tr>
<tr>
  <td>x86</td>
  <td>MacOS X</td>
  <td>i386-apple-darwin</td>
  <td>Yes</td>
  <td>Yes</td>
  <td>No</td>
  <td><a href="attic/x86-osx-ghc">Attic/X86OSXGhc</a></td>
</tr>
</table>


## Definitions

- **GHCi**

  The interactive environment, including dynamic linking of object
  code and dynamic generation of FFI calls.

- **NCG**

  Native code generator: GHC can generate assembly code directly for this platform, bypassing gcc.

- **Dynamic libraries**

  Support for generating dynamically-linked sharable libraries from
  Haskell code.

