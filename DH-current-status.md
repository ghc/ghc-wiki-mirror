## Big picture

* The overall plan: [#378 Design for Dependent Types](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0378-dependent-type-design.rst).
* [Clarifying dependent types](https://discourse.haskell.org/t/clarifying-dependent-types/1756) a useful Discourse conversation from 2022
* Weekly updates on Discourse: [#1 (2022-12-07)](https://discourse.haskell.org/t/ghc-dh-weekly-update-1-2022-12-07/5418), [#2 (2022-12-21)](https://discourse.haskell.org/t/ghc-dh-weekly-update-2-2022-12-21/5473), [#3 (2023-01-11)](https://discourse.haskell.org/t/ghc-dh-weekly-update-3-2023-01-11/5566), [#4 (2023-01-18)](https://discourse.haskell.org/t/ghc-dh-weekly-update-4-2023-01-18/5608), [#5 (2023-01-25)](https://discourse.haskell.org/t/ghc-dh-weekly-update-5-2023-01-25/5662), [#6 (2023-06-07)](https://discourse.haskell.org/t/ghc-dh-weekly-update-6-2023-06-07/6383), [#7 (2023-06-14)](https://discourse.haskell.org/t/ghc-dh-weekly-update-7-2023-06-14/6444).
* [Blog post on progress Sept 2023](https://serokell.io/blog/ghc-dependent-types-in-haskell)

## Review queue

Clear

## Work under way

The following proposals were accepted by the committee but have not been implemented in GHC yet, or were only partially implemented. The associated tickets may cover only certain parts of the proposal, so if a linked ticket turns out to be closed, it doesn't necessarily mean that the proposal was implemented.

1. [GHC Proposal #448](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0448-type-variable-scoping.rst) "Modern Scoped Type Variables"
   * Tickets: #17594 (this one is about GHC Proposal [#155](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0155-type-lambda.rst), which was subsumed by [#448](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0448-type-variable-scoping.rst))
   * Draft MR !8629 by Torsten
   * Draft MR for #23291: !10385 by Andrei
   * !11019 merge `rnHsTyPat` and `rnHsTyKi`: dormant
   * **Completed tasks** (merged MRs):
     * combine HsLam and HsLamCase: #23916 (!11238)
     * lazy skolemisation: !12019 + !11198
     * invisible binders in lambdas: `\ @a -> ...` #17594 (!11109)
2. [GHC Proposal #475](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0475-tuple-syntax.rst) "Non-punning list and tuple syntax"
   * Ticket: #21294
   * Draft MR: !8820 by Torsten
   * **Blocked** (at least partially) on #23162
3. [GHC Proposal #281](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0281-visible-forall.rst) "Visible forall in types of terms"
   * Part 1 has been implemented #22326 !9257
   * Part 2 (the term-to-type conversion): #23717 with sub-tasks
     * [x] #23740 !11134 (scoping when term variables appear in types)
     * [x] #23738 !11166 (term to type conversion in expressions)
     * [x] #23739 !11682 (pattern to type conversion in patterns)
     * [ ] #24159 (type-in-term syntax)
   * Complete "make `forall` a keyword": #23719
   * Visible `forall` in pattern synonyms: #23704
   * [Proposed amendment](https://github.com/ghc-proposals/ghc-proposals/pull/607) to [GHC Proposal #281](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0281-visible-forall.rst), to settle the question of term variables appearing in types.
   * Need to fix #24225, #24226; generally see tickets on ~"visible dependent quantification"
4. [GHC Proposal #402](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0402-gadt-syntax.rst) "Stable GADT constructor syntax"
   * Very rough draft at !9438
   * **Blocked** on !5832, which is a prerequisite to !9438 (ticket: #18389)
5. [GHC Proposal #425](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0425-decl-invis-binders.rst) "Invisible binders in type declarations"
   * Meta-ticket #23500.
   * The main feature is implemented by !9480, but there are some loose ends: !1602, #23501, #23515 ( !10672), #23502.
6. [GHC Proposal #581](https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0581-namespace-specified-imports.rst) "Namespace-specified imports"
   * Related issue: #22581
   * No one is working on it at the moment

## Proposals under discussion

* [#270](https://github.com/ghc-proposals/ghc-proposals/pull/270) "Support pun-free code" by Artem.
  * Needs an update in light of the fact that 581 was accepted.
* [#509](https://github.com/ghc-proposals/ghc-proposals/pull/509) "Type functions" by Rinat.
  * Dormant and requires an update to Core spec.
* [#592](https://github.com/ghc-proposals/ghc-proposals/pull/592) "Scoped kind variables in standalone kind signatures" by Vlad (an amendment to the original SAKS proposal)
  * Underspecified and low-priority, Vlad intends to return to it some time later.

## Bugs

The following tickets do not have an associated proposal because they are outright bugs.

1. [#12088](https://gitlab.haskell.org/ghc/ghc/-/issues/12088 "Type/data family instances in kind checking") "Type/data family instances in kind checking"
   * Draft MR: !10763 by Vlad
   * [`singleton-gadts`](https://github.com/RyanGlScott/singleton-gadts) is broken because of it
   * We can hardly start making changes to dependency analysis required for DH (referencing terms from types) before we address this major bug in existing dependency analysis
2. Improvements to renaming and type checking: #11101, #23639. They were left as future work in https://gitlab.haskell.org/ghc/ghc/-/merge_requests/10725#note_512713, Andrei intends to fix both.